(function ($) {
$(window).on('load', function () {
    $('#preloader-active').delay(450).fadeOut('slow');
    $('body').delay(450).css({
      'overflow': 'visible'
    });
  });

//   $.scrollUp({
//     scrollName: 'scrollUp', // Element ID
//     topDistance: '300', // Distance from top before showing element (px)
//     topSpeed: 300, // Speed back to top (ms)
//     animation: 'fade', // Fade, slide, none
//     animationInSpeed: 200, // Animation in speed (ms)
//     animationOutSpeed: 200, // Animation out speed (ms)
//     scrollText: '<i class="ti-arrow-up"></i>', // Text for element
//     activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
//   });

  window.onload = function() {
    setActiveLink();
  };
  
    $.fn.menumaker = function (options) {
        var cssmenu = $(this),
            settings = $.extend(
                {
                    format: "dropdown",
                    sticky: false,
                },
                options
            );
        return this.each(function () {
            $(this)
                .find(".button")
                .on("click", function () {
                    $(this).toggleClass("menu-opened");
                    var mainmenu = $(this).next("ul");
                    if (mainmenu.hasClass("open")) {
                        mainmenu.slideToggle().removeClass("open");
                    } else {
                        mainmenu.slideToggle().addClass("open");
                        if (settings.format === "dropdown") {
                            mainmenu.find("ul").show();
                        }
                    }
                });
            cssmenu.find("li ul").parent().addClass("has-sub");
            multiTg = function () {
                cssmenu
                    .find(".has-sub")
                    .prepend('<span class="submenu-button"></span>');
                cssmenu.find(".submenu-button").on("click", function () {
                    $(this).toggleClass("submenu-opened");
                    if ($(this).siblings("ul").hasClass("open")) {
                        $(this)
                            .siblings("ul")
                            .removeClass("open")
                            .slideToggle();
                    } else {
                        $(this).siblings("ul").addClass("open").slideToggle();
                    }
                });
            };
            if (settings.format === "multitoggle") multiTg();
            else cssmenu.addClass("dropdown");
            if (settings.sticky === true) cssmenu.css("position", "fixed");
            resizeFix = function () {
                var mediasize = 1199;
                if ($(window).width() > mediasize) {
                    cssmenu.find("ul").show();
                }
                if ($(window).width() <= mediasize) {
                    cssmenu.find("ul").hide().removeClass("open");
                }
            };
            resizeFix();
            return $(window).on("resize", resizeFix);
        });
    };
})(jQuery);

(function ($) {
    $(document).ready(function () {
        $("#cssmenu").menumaker({
            format: "multitoggle",
        });
    });

})(jQuery);


function setActiveLink() {
    // Get the current URL path
    var path = window.location.pathname;
  
    // Remove the leading slash
    path = path.substring(1);
  
    // Remove any query parameters
    path = path.split('?')[0];
  
    // Remove any trailing slashes
    path = path.replace(/\/$/, "");
  
    // Remove the file extension if present
    path = path.replace(/\.[^/.]+$/, "");
  
    // Add the active class to the corresponding navigation link
    document.getElementById(path).classList.add('active');
  }

  
