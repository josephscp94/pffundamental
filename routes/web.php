<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    //ruta del admin

    Route::resource('/resumen', App\Http\Controllers\Admin\ResumenController::class);
    Route::resource('/colaborador', App\Http\Controllers\Admin\ColaboradorController::class);
    Route::resource('/rol', App\Http\Controllers\Admin\RolController::class);
    Route::resource('/socio_comunitario', App\Http\Controllers\Admin\Socio_comunitarioController::class);
    Route::resource('/facultad', App\Http\Controllers\Admin\FacultadController::class);
    Route::resource('/carrera', App\Http\Controllers\Admin\CarreraController::class);
    Route::resource('/tag', App\Http\Controllers\Admin\TagController::class);
    Route::resource('/competencia', App\Http\Controllers\Admin\CompetenciaController::class);
    Route::resource('/proyecto', App\Http\Controllers\Admin\ProyectoController::class);
    Route::resource('/home_noticias', App\Http\Controllers\Admin\Home_noticiasController::class);
    Route::resource('/testimonios', App\Http\Controllers\Admin\TestimoniosController::class);
    Route::resource('/que_hacemos', App\Http\Controllers\Admin\Que_hacemosController::class);
    Route::resource('/quienes_somos', App\Http\Controllers\Admin\Quienes_somosController::class);
    Route::resource('/home_carrusel', App\Http\Controllers\Admin\Home_carruselController::class);
    Route::resource('/ods', App\Http\Controllers\Admin\OdsController::class);
    Route::resource('/user', App\Http\Controllers\Admin\UserController::class);
    Route::resource('/periodo', App\Http\Controllers\Admin\PeriodoController::class);
    Route::resource('/solicitudes', App\Http\Controllers\Admin\SolicitudesController::class);
    Route::resource('/contacto', App\Http\Controllers\Admin\ContactoController::class);
    Route::resource('/area', App\Http\Controllers\Admin\AreaController::class);
    Route::resource('/grafico_carreras', App\Http\Controllers\Admin\GraficoController::class);
    Route::resource('/telefono_area', App\Http\Controllers\Admin\Telefono_areaController::class);
    Route::get('/grafico_carreras', [App\Http\Controllers\Admin\GraficoController::class, 'grafico_carreras'])->name('grafico_carreras');
    Route::get('/grafico_socios', [App\Http\Controllers\Admin\GraficoController::class, 'grafico_socios'])->name('grafico_socios');
    Route::get('/grafico_periodos', [App\Http\Controllers\Admin\GraficoController::class, 'grafico_periodos'])->name('grafico_periodos');
    Route::get('/grafico_tags', [App\Http\Controllers\Admin\GraficoController::class, 'grafico_tags'])->name('grafico_tags');
    Route::get('/grafico_ods', [App\Http\Controllers\Admin\GraficoController::class, 'grafico_ods'])->name('grafico_ods');
});

//ruta publica


Route::get('/', [App\Http\Controllers\FrontController::class, 'home'])->name('/');
Route::get('inicio', [App\Http\Controllers\FrontController::class, 'home'])->name('inicio');
Route::get('/noticias/{slug}', [App\Http\Controllers\FrontController::class, 'noticias'])->name('noticias');
Route::get('/blog-noticias', [App\Http\Controllers\FrontController::class, 'blognoticias'])->name('blog-noticias');
Route::get('/quienes-somos', [App\Http\Controllers\FrontController::class, 'quienes_somos'])->name('quienes-somos');
Route::get('/competencias-genericas', [App\Http\Controllers\FrontController::class, 'competencias'])->name('competencias-genericas');
Route::get('/contacto', [App\Http\Controllers\FrontController::class, 'contacto'])->name('contacto');
Route::get('/proyectos/{id}', [App\Http\Controllers\FrontController::class, 'proyectos'])->name('proyectos');
Route::get('/que-hacemos', [App\Http\Controllers\FrontController::class, 'que_hacemos'])->name('que-hacemos');
Route::get('/carreras', [App\Http\Controllers\FrontController::class, 'carreras'])->name('carreras');
Route::get('/proyecto-detalle/{slug}', [App\Http\Controllers\FrontController::class, 'proyecto_detalle'])->name('proyecto-detalle');
Route::get('/socios-comunitarios', [App\Http\Controllers\FrontController::class, 'socio_comunitarios'])->name('socios-comunitarios');
Route::post('/socios-comunitarios', [App\Http\Controllers\FrontController::class, 'formulario'])->name('socios-comunitarios');
Route::get('/tags/{slug}', [App\Http\Controllers\FrontController::class, 'tags'])->name('tags');
