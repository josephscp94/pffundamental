<?php

namespace Database\Seeders;

use App\Models\Carrera;
use Illuminate\Database\Seeder;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Economia y negocios id=1

        $carrera = new Carrera();
        $carrera->nombre = "Ingenieria en informatica empresarial";
        $carrera->slug = "ingenieria_en_informatica_empresarial";
        $carrera->imagen = "ingenieria_en_informatica_empresarial.jpg";
        $carrera->facultad_id = "1";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Auditoría e Ingeniería en Control de Gestión";
        $carrera->slug = "auditoría-e-ingeniería-en-control-de-gestión";
        $carrera->imagen = "auditoría-e-ingeniería-en-control-de-gestión.jpg";
        $carrera->facultad_id = "1";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Contador Público y Auditor";
        $carrera->slug = "contador-público-y-auditor";
        $carrera->imagen = "contador-público-y-auditor.jpg";
        $carrera->facultad_id = "1";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Comercial";
        $carrera->slug = "ingeniería-comercial";
        $carrera->imagen = "ingeniería-comercial.jpg";
        $carrera->facultad_id = "1";
        $carrera->save();

        // Ciencias de la Salud id=2

        $carrera = new Carrera();
        $carrera->nombre = "Enfermería";
        $carrera->slug = "enfermería";
        $carrera->imagen = "enfermería.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Fonoaudiología";
        $carrera->slug = "fonoaudiología";
        $carrera->imagen = "fonoaudiología.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Kinesiología";
        $carrera->slug = "kinesiología";
        $carrera->imagen = "kinesiología.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Medicina";
        $carrera->slug = "medicina";
        $carrera->imagen = "medicina.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Nutrición y Dietética";
        $carrera->slug = "nutrición-y-dietética";
        $carrera->imagen = "nutrición-y-dietética.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Obstetricia y Puericultura";
        $carrera->slug = "obstetricia-y-puericultura";
        $carrera->imagen = "obstetricia-y-puericultura.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Odontología";
        $carrera->slug = "odontología";
        $carrera->imagen = "odontología.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Tecnología Médica";
        $carrera->slug = "tecnología-médica";
        $carrera->imagen = "tecnología-médica.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Terapia Ocupacional";
        $carrera->slug = "terapia-ocupacional";
        $carrera->imagen = "terapia-ocupacional.jpg";
        $carrera->facultad_id = "2";
        $carrera->save();

        // Ciencias Jurídicas y Sociales id=3

        $carrera = new Carrera();
        $carrera->nombre = "Administración Pública";
        $carrera->slug = "administración-pública";
        $carrera->imagen = "administración-pública.jpg";
        $carrera->facultad_id = "3";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Derecho";
        $carrera->slug = "derecho";
        $carrera->imagen = "derecho.jpg";
        $carrera->facultad_id = "3";
        $carrera->save();

        // Ingeniería id=4

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil de Minas";
        $carrera->slug = "ingeniería-civil-de-minas";
        $carrera->imagen = "ingeniería-civil-de-minas.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil Eléctrica";
        $carrera->slug = "ingeniería-civil-eléctrica";
        $carrera->imagen = "ingeniería-civil-eléctrica.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil en Bioinformática";
        $carrera->slug = "ingeniería-civil-en-bioinformática";
        $carrera->imagen = "ingeniería-civil-en-bioinformática.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil en Computación";
        $carrera->slug = "ingeniería-civil-en-computación";
        $carrera->imagen = "ingeniería-civil-en-computación.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil en Obras Civiles";
        $carrera->slug = "ingeniería-civil-en-obras-civiles";
        $carrera->imagen = "ingeniería-civil-en-obras-civiles.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil Industrial";
        $carrera->slug = "ingeniería-civil-industrial";
        $carrera->imagen = "ingeniería-civil-industrial.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil Mecánica";
        $carrera->slug = "ingeniería-civil-mecánica";
        $carrera->imagen = "ingeniería-civil-mecánica.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería Civil Mecatrónica";
        $carrera->slug = "ingeniería-civil-mecatrónica";
        $carrera->imagen = "ingeniería-civil-mecatrónica.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Ingeniería en Desarrollo de Videojuegos y Realidad Virtual";
        $carrera->slug = "ingeniería-en-desarrollo-de-videojuegos-y-realidad-virtual";
        $carrera->imagen = "ingeniería-en-desarrollo-de-videojuegos-y-realidad-virtual.jpg";
        $carrera->facultad_id = "4";
        $carrera->save();

        // Psicología id=5

        $carrera = new Carrera();
        $carrera->nombre = "Psicología";
        $carrera->slug = "psicología";
        $carrera->imagen = "psicología.jpg";
        $carrera->facultad_id = "5";
        $carrera->save();

        // Arquitectura, Música y Diseño id=6

        $carrera = new Carrera();
        $carrera->nombre = "Arquitectura";
        $carrera->slug = "arquitectura";
        $carrera->imagen = "arquitectura.jpg";
        $carrera->facultad_id = "6";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Diseño";
        $carrera->slug = "diseño";
        $carrera->imagen = "diseño.jpg";
        $carrera->facultad_id = "6";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Licenciatura en Interpretación y Formación Musical Especializada";
        $carrera->slug = "licenciatura-en-interpretación-y-formación-musical-especializada";
        $carrera->imagen = "licenciatura-en-interpretación-y-formación-musical-especializada.jpg";
        $carrera->facultad_id = "6";
        $carrera->save();

        // Ciencias de la Educación id=7

        $carrera = new Carrera();
        $carrera->nombre = "Pedagogía en Educación General Básica Mención en Inglés";
        $carrera->slug = "pedagogía-en-educación-general-básica-mención-en-inglés";
        $carrera->imagen = "pedagogía-en-educación-general-básica-mención-en-inglés.jpg";
        $carrera->facultad_id = "7";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Pedagogía en Educación Media en Biología y Química";
        $carrera->slug = "pedagogía-en-educación-media-en-biología-y-química";
        $carrera->imagen = "pedagogía-en-educación-media-en-biología-y-química.jpg";
        $carrera->facultad_id = "7";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Pedagogía en Educación Media en Inglés";
        $carrera->slug = "pedagogía-en-educación-media-en-inglés";
        $carrera->imagen = "pedagogía-en-educación-media-en-inglés.jpg";
        $carrera->facultad_id = "7";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Pedagogía en Educación Media en Matemática y Física";
        $carrera->slug = "pedagogía-en-educación-media-en-matemática-y-física";
        $carrera->imagen = "pedagogía-en-educación-media-en-matemática-y-física.jpg";
        $carrera->facultad_id = "7";
        $carrera->save();

        $carrera = new Carrera();
        $carrera->nombre = "Pedagogía en Educación Parvularia Mención en Inglés";
        $carrera->slug = "pedagogía-en-educación-parvularia-mención-en-inglés";
        $carrera->imagen = "pedagogía-en-educación-parvularia-mención-en-inglés.jpg";
        $carrera->facultad_id = "7";
        $carrera->save();

        // Ciencias Agrarias id=8

        $carrera = new Carrera();
        $carrera->nombre = "Agronomía";
        $carrera->slug = "agronomía";
        $carrera->imagen = "Agronomía.jpg";
        $carrera->facultad_id = "8";
        $carrera->save();
    }
}
