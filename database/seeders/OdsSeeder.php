<?php

namespace Database\Seeders;

use App\Models\Ods; 
use Illuminate\Database\Seeder;

class OdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fin_de_la_pobreza = new Ods();
        $fin_de_la_pobreza ->nombre = "Fin de la pobreza";
        $fin_de_la_pobreza ->slug = "fin_de_la_pobreza";
        $fin_de_la_pobreza ->save();

        $hambre_cero = new Ods();
        $hambre_cero ->nombre = "Hambre cero";
        $hambre_cero ->slug = "hambre_cero";
        $hambre_cero ->save();

        $Salud_y_bienestar = new Ods();
        $Salud_y_bienestar ->nombre = "Salud y bienestar";
        $Salud_y_bienestar ->slug = "Salud_y_bienestar";
        $Salud_y_bienestar ->save();

        $educacion_de_calidad = new Ods();
        $educacion_de_calidad ->nombre = "Educación de calidad";
        $educacion_de_calidad ->slug = "educacion_de_calidad";
        $educacion_de_calidad ->save();

        $igualdad_de_genero = new Ods();
        $igualdad_de_genero ->nombre = "Igualdad de género";
        $igualdad_de_genero ->slug = "igualdad_de_genero";
        $igualdad_de_genero ->save();

        $agua_limpia_y_saneamiento = new Ods();
        $agua_limpia_y_saneamiento ->nombre = "Agua limpia y saneamiento";
        $agua_limpia_y_saneamiento ->slug = "agua_limpia_y_saneamiento";
        $agua_limpia_y_saneamiento ->save();

        $energia = new Ods();
        $energia ->nombre = "Energía asequible y no contaminante";
        $energia ->slug = "energia";
        $energia ->save();

        $trabajo_decente = new Ods();
        $trabajo_decente ->nombre = "Trabajo decente y crecimiento económico";
        $trabajo_decente ->slug = "trabajo_decente";
        $trabajo_decente ->save();

        $Industria = new Ods();
        $Industria ->nombre = "Industria, innovación e infraestructura";
        $Industria ->slug = "Industria";
        $Industria ->save();

        $Reduccion_de_las_desigualdes = new Ods();
        $Reduccion_de_las_desigualdes ->nombre = "Reducción de las desigualdades";
        $Reduccion_de_las_desigualdes ->slug = "Reduccion_de_las_desigualdes";
        $Reduccion_de_las_desigualdes ->save();

        $cuidades_y_comunidades_sostenibles = new Ods();
        $cuidades_y_comunidades_sostenibles ->nombre = "Ciudades y comunidades sostenibles";
        $cuidades_y_comunidades_sostenibles ->slug = "cuidades_y_comunidades_sostenibles";
        $cuidades_y_comunidades_sostenibles ->save();

        $produccion_y_consumo_responsables = new Ods();
        $produccion_y_consumo_responsables ->nombre = "Producción y consumo responsables";
        $produccion_y_consumo_responsables ->slug = "produccion_y_consumo_responsables";
        $produccion_y_consumo_responsables ->save();

        $accion_por_el_clima = new Ods();
        $accion_por_el_clima ->nombre = "Acción por el clima";
        $accion_por_el_clima ->slug = "accion_por_el_clima";
        $accion_por_el_clima ->save();

        $vida_submarina = new Ods();
        $vida_submarina ->nombre = "Vida submarina";
        $vida_submarina ->slug = "vida_submarina";
        $vida_submarina ->save();

        $vida_de_ecosistemas_terrestres = new Ods();
        $vida_de_ecosistemas_terrestres ->nombre = "Vida de ecosistemas terrestres";
        $vida_de_ecosistemas_terrestres ->slug = "vida_de_ecosistemas_terrestres";
        $vida_de_ecosistemas_terrestres ->save();

        $paz_justicia_e_instituciones_solidas = new Ods();
        $paz_justicia_e_instituciones_solidas ->nombre = "Paz, justicia e instituciones solidas";
        $paz_justicia_e_instituciones_solidas ->slug = "paz_justicia_e_instituciones_solidas";
        $paz_justicia_e_instituciones_solidas ->save();

        $alianzas_para_lograr_objetivos = new Ods();
        $alianzas_para_lograr_objetivos ->nombre = "Alianzas para lograr objetivos";
        $alianzas_para_lograr_objetivos ->slug = "alianzas_para_lograr_objetivos";
        $alianzas_para_lograr_objetivos ->save();

    }
}
