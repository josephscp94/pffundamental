<?php

namespace Database\Seeders;

use App\Models\Socio_comunitario;
use Illuminate\Database\Seeder;

class Socio_comunitarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Socio_comunitario();
        $var->nombre = "Fondo Esperanza";
        $var->descripcion = "Fondo Esperanza es pionero en crear e implementar en Chile un servicio microfinanciero integral enfocado en emprendedores de sectores vulnerables, principalmente mujeres, con el objetivo de entregarles las herramientas necesarias para el desarrollo de sus negocios. Esto lo hacemos base a tres ejes.";
        $var->imagen = "fondo_esperanza_talca.png";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Centro penintenciario";
        $var->descripcion = "Los centros de cumplimiento penitenciarios que participaron de las intervenciones en el periodo 2021, acogieron a 10 equipos de estudiantes. Las ciudades involucradas fueron las de: Curicó, Molina, Talca y Cauquenes.";
        $var->imagen = "centro_penintenciario.png";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Emprendimientos - Empresas";
        $var->descripcion = "Los y las emprendedoras y empresas que acogieron a los equipos de estudiantes fueron 35, de las más diversas áreas de la comercialización, producción y servicios. Las comunas a la que pertenecen son: Curicó, Talca, Teno, San Clemente, Pichilemu, Linares, entre otras.";
        $var->imagen = "Emprendimient-Empresas.jpg";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Establecimientos Educacionales";
        $var->descripcion = "Los establecimientos educacionales incorporaron a 93 equipos de estudiantes, correspondientes a los niveles de: prebásica, básica, media y universitaria. Asimismo se incorporan en esta tipología a los establecimientos que atienden necesidades educativas especiales. Sobre las comunas a la que pertenecen, estas son: Santiago, Talca, Curicó, Teno, Linares, Sagrada Familia, Curacautín.";
        $var->imagen = "Establecimientos Educacionales.webp";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Organizaciones de la Sociedad Civil";
        $var->descripcion = "Las organizaciones no gubernamentales y de la sociedad civil, acogieron a 6 equipos de estudiantes, atendiendo a necesidades de: personas con discapacidad, sustentabilidad, educación y voluntariado; de las comunas de Talca y Curicó";
        $var->imagen = "Organizaciones de la Sociedad Civil.jpg";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Fundación Envejecimiento Funcional";
        $var->descripcion = "La fundación tiene como objetivo lograr que los adultos mayores participen de actividades que promuevan el envejecer activamente, para lo cual se requiere fomentar y equilibrar la responsabilidad  personal, el encuentro, la solidaridad intergeneracional y la creación de entornos favorables, que aporten a la calidad de vida y retrasen los niveles de dependencia.";
        $var->imagen = "Fundación Envejecimiento Funcional.jpg";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Seremi de desarrollo social";
        $var->descripcion = "La Seremi de desarrollo social es la encargada de caracterizar a la población nacional y regional, detectar las necesidades sociales de dicha población y elaborar reportes en los distintos niveles territoriales y sistematizar los datos y estadísticas que describan la realidad social nacional.";
        $var->imagen = "Seremi de desarrollo social.jpg";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Ilustre Municipalidad de Linares";
        $var->descripcion = "La Ilustre Municipalidad de Linares forma parte de las 345 corporaciones edilicias existentes a lo largo y ancho del país, es una corporación autónoma de derecho público con personalidad jurídica y patrimonio propio. Su finalidad es satisfacer las necesidades de la comunidad y asegurar su participación en el progreso económico, social y cultural de la comuna";
        $var->imagen = "Ilustre Municipalidad de Linares.jpg";
        $var->save();

        $var = new Socio_comunitario();
        $var->nombre = "Taller laboral Unpade";
        $var->descripcion = "El Taller Laboral UNPADE es una escuela especial que busca ser inclusiva e innovadora desde la diversificación e integralidad de teorías y prácticas pedagógicas, otorgando distintas respuestas a las problemáticas que presentan las personas con discapacidad intelectual,";
        $var->imagen = "Taller laboral Unpade.jpg";
        $var->save();
    }
}
