<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new Area();
        $rol->nombre = "Equipo de Dirección";
        $rol->save();

        $rol = new Area();
        $rol->nombre = "Equipo de Docencia";
        $rol->save();

        $rol = new Area();
        $rol->nombre = "Profesionales de vinculación con el medio";
        $rol->save();
    }
}
