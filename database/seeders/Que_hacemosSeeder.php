<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Que_hacemos;

class Que_hacemosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Que_hacemos();
        $var->compromiso_institucional = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non. Morbi pellentesque lacus purus, in dignissim justo condimentum eget. Nulla facilisi. Ut elementum vehicula lacus. Cras enim risus, lacinia a cursus at, pulvinar eu turpis. Etiam ut pharetra est. Sed a neque non nibh feugiat vestibulum in ut massa. Vestibulum quis ligula imperdiet, mollis magna vitae, iaculis ante. Nam id dui nec mi posuere rutrum. Sed urna risus, cursus sit amet iaculis non, porta et tortor. Fusce fermentum elementum lectus ut bibendum. Integer at est vitae felis aliquam viverra. Maecenas eu sodales quam.";
        $var->imagen_ci = "imagen_ci.jpg";
        $var->docencia = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non. Morbi pellentesque lacus purus, in dignissim justo condimentum eget. Nulla facilisi. Ut elementum vehicula lacus. Cras enim risus, lacinia a cursus at, pulvinar eu turpis. Etiam ut pharetra est. Sed a neque non nibh feugiat vestibulum in ut massa. Vestibulum quis ligula imperdiet, mollis magna vitae, iaculis ante. Nam id dui nec mi posuere rutrum. Sed urna risus, cursus sit amet iaculis non, porta et tortor. Fusce fermentum elementum lectus ut bibendum. Integer at est vitae felis aliquam viverra. Maecenas eu sodales quam.";
        $var->imagen_dc = "imagen_dc.jpg";
        $var->vinculacion_con_el_medio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non. Morbi pellentesque lacus purus, in dignissim justo condimentum eget. Nulla facilisi. Ut elementum vehicula lacus. Cras enim risus, lacinia a cursus at, pulvinar eu turpis. Etiam ut pharetra est. Sed a neque non nibh feugiat vestibulum in ut massa. Vestibulum quis ligula imperdiet, mollis magna vitae, iaculis ante. Nam id dui nec mi posuere rutrum. Sed urna risus, cursus sit amet iaculis non, porta et tortor. Fusce fermentum elementum lectus ut bibendum. Integer at est vitae felis aliquam viverra. Maecenas eu sodales quam.";
        $var->imagen_vcm = "imagen_vcm.jpg";
        $var->user_id = "1";
        $var->save();
    }
}
