<?php

namespace Database\Seeders;

use App\Models\Home_carrusel;
use Illuminate\Database\Seeder;

class Home_carruselSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Home_carrusel();
        $var->descripcion = "Conoce nuestros proyectos de Responsabilidad Social";
        $var->imagen = "carrusel1.jpg";
        $var->estado = "1";
        $var->link = "https://formacionfundamental.utalca.cl/carreras";
        $var->user_id = "1";
        $var->save();

        $var = new Home_carrusel();
        $var->descripcion = "Conoce a nuestros Socios Comunitarios y se parte de nuestra alianza";
        $var->imagen = "carrusel2.jpg";
        $var->estado = "1";
        $var->link = "https://formacionfundamental.utalca.cl/socios-comunitarios";
        $var->user_id = "1";
        $var->save();
    }
}
