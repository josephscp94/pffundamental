<?php

namespace Database\Seeders;

use App\Models\Testimonios;
use Illuminate\Database\Seeder;

class TestimoniosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Testimonios();
        $var->nombre = "Cristóbal Toledo Acevedo";
        $var->Slug = "Cristóbal Toledo Acevedo";
        $var->cargo = "Estudiante de Ing Civil Industrial";
        $var->imagen = "testimonio1.jpg";
        $var->parrafo = "Durante el año pasado fui el representante del equipo que obtuvo el primer lugar del Reconocimiento impulsa 2021, con nuestro proyecto del módulo de Responsabilidad Social, con clases de nivelación a estudiantes privados de libertad del Centro penitenciario de Cauquenes, obteniendo resultados significativos. Es por esta razón de que el compromiso del estudiante de la Utalca es poner al servicio de otros nuestra experiencia, conocimiento y aprendizaje.";
        $var->estado = "1";
        $var->user_id = "1";
        $var->save();

        $var = new Testimonios();
        $var->nombre = "Nombre Apellido 2";
        $var->Slug = "nombre-apellido-2";
        $var->cargo = "Cargo";
        $var->imagen = "testimonio2.jpg";
        $var->parrafo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non. Morbi pellentesque lacus purus, in dignissim justo condimentum eget. Nulla facilisi. Ut elementum vehicula lacus. Cras enim risus, lacinia a cursus at, pulvinar eu turpis. Etiam ut pharetra est. Sed a neque non nibh feugiat vestibulum in ut massa. Vestibulum quis ligula imperdiet, mollis magna vitae, iaculis ante. Nam id dui nec mi posuere rutrum. Sed urna risus, cursus sit amet iaculis non, porta et tortor. Fusce fermentum elementum lectus ut bibendum. Integer at est vitae felis aliquam viverra. Maecenas eu sodales quam.";
        $var->estado = "1";
        $var->user_id = "1";
        $var->save();

        $var = new Testimonios();
        $var->nombre = "Nombre Apellido 3";
        $var->Slug = "nombre-apellido-3";
        $var->cargo = "Cargo";
        $var->imagen = "testimonio3.jpg";
        $var->parrafo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non. Morbi pellentesque lacus purus, in dignissim justo condimentum eget. Nulla facilisi. Ut elementum vehicula lacus. Cras enim risus, lacinia a cursus at, pulvinar eu turpis. Etiam ut pharetra est. Sed a neque non nibh feugiat vestibulum in ut massa. Vestibulum quis ligula imperdiet, mollis magna vitae, iaculis ante. Nam id dui nec mi posuere rutrum. Sed urna risus, cursus sit amet iaculis non, porta et tortor. Fusce fermentum elementum lectus ut bibendum. Integer at est vitae felis aliquam viverra. Maecenas eu sodales quam.";
        $var->estado = "1";
        $var->user_id = "1";
        $var->save();

        $var = new Testimonios();
        $var->nombre = "Nombre Apellido 4";
        $var->Slug = "nombre-apellido-4";
        $var->cargo = "Cargo";
        $var->imagen = "testimonio4.jpg";
        $var->parrafo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non. Morbi pellentesque lacus purus, in dignissim justo condimentum eget. Nulla facilisi. Ut elementum vehicula lacus. Cras enim risus, lacinia a cursus at, pulvinar eu turpis. Etiam ut pharetra est. Sed a neque non nibh feugiat vestibulum in ut massa. Vestibulum quis ligula imperdiet, mollis magna vitae, iaculis ante. Nam id dui nec mi posuere rutrum. Sed urna risus, cursus sit amet iaculis non, porta et tortor. Fusce fermentum elementum lectus ut bibendum. Integer at est vitae felis aliquam viverra. Maecenas eu sodales quam.";
        $var->estado = "1";
        $var->user_id = "1";
        $var->save();
    }
}
