<?php

namespace Database\Seeders;

use App\Models\Facultad;
use Illuminate\Database\Seeder;

class FacultadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $economia = new Facultad();
        $economia->nombre = "Economía y negocios";
        $economia->slug = "economía-y-negocios";
        $economia->save();

        $facultad = new Facultad();
        $facultad->nombre = "Ciencias de la Salud";
        $facultad->slug = "ciencias-de-la-salud";
        $facultad->save();

        $facultad = new Facultad();
        $facultad->nombre = "Ciencias Jurídicas y Sociales";
        $facultad->slug = "ciencias-jurídicas-y-sociales";
        $facultad->save();

        $facultad = new Facultad();
        $facultad->nombre = "Ingeniería";
        $facultad->slug = "ingeniería";
        $facultad->save();

        $facultad = new Facultad();
        $facultad->nombre = "Psicología";
        $facultad->slug = "psicología";
        $facultad->save();

        $facultad = new Facultad();
        $facultad->nombre = "Arquitectura, Música y Diseño";
        $facultad->slug = "arquitectura-música-y-diseño";
        $facultad->save();

        $facultad = new Facultad();
        $facultad->nombre = "Ciencias de la Educación";
        $facultad->slug = "ciencias-de-la-educación";
        $facultad->save();

        $facultad = new Facultad();
        $facultad->nombre = "Ciencias Agrarias";
        $facultad->slug = "ciencias-agrarias";
        $facultad->save();
    }
}
