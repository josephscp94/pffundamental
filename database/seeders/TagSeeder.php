<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Tag();
        $var->nombre = "Desarrollo local";
        $var->slug = "desarrollo-local";
        $var->save();

        $var = new Tag();
        $var->nombre = "Gestión de empresas";
        $var->slug = "gestión-de-empresas";
        $var->save();

        $var = new Tag();
        $var->nombre = "Alfabetización digital";
        $var->slug = "alfabetización-digital";
        $var->save();

        $var = new Tag();
        $var->nombre = "Emprendimiento";
        $var->slug = "emprendimiento";
        $var->save();

        $var = new Tag();
        $var->nombre = "Fundaciónes";
        $var->slug = "Fundaciónes";
        $var->save();

        $var = new Tag();
        $var->nombre = "Corporaciones";
        $var->slug = "corporaciones";
        $var->save();

        $var = new Tag();
        $var->nombre = "Educación";
        $var->slug = "educación";
        $var->save();

        $var = new Tag();
        $var->nombre = "Municipios";
        $var->slug = "municipios";
        $var->save();

        $var = new Tag();
        $var->nombre = "ONG";
        $var->slug = "ong";
        $var->save();

        $var = new Tag();
        $var->nombre = "Gestión territorial";
        $var->slug = "gestion-territorial";
        $var->save();

        $var = new Tag();
        $var->nombre = "Trabajo comunitario";
        $var->slug = "trabajo-comunitario";
        $var->save();

        $var = new Tag();
        $var->nombre = "Modelo de gestión";
        $var->slug = "modelo-de-gestion";
        $var->save();

        $var = new Tag();
        $var->nombre = "Reciclaje";
        $var->slug = "reciclaje";
        $var->save();

        $var = new Tag();
        $var->nombre = "Huerto sostenible";
        $var->slug = "huerto-sostenible";
        $var->save();
    }
}
