<?php

namespace Database\Seeders;

use App\Models\Home_noticias;
use Illuminate\Database\Seeder;

class Home_noticiasSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $var = new Home_noticias();
    $var->titular = "Informe Ejecutivo de Proyectos de Responsabilidad Social 2021. ";
    $var->slug = "informe-ejecutivo-de-Proyectos-de-Responsabilidad-Social-2021";
    $var->bajada = "El presente informe describe el modelo de vinculación desarrollado por la línea académica de Responsabilidad, además detalla los proyectos de intervención ejecutados por las distintas carreras de la universidad en el año 2021.";
    $var->cuerpo = "Conece en extención el documento aquí";
    $var->imagen = "noticia1.jpg";
    $var->estado = "1";
    $var->link = "https://drive.google.com/file/d/1Mgj6RWtMfntO3_aBg_zielxpOIDCCNXq/view";
    $var->user_id = "1";
    $var->save();

    $var = new Home_noticias();
    $var->titular = "Congreso Internacional de Vinculación con el Medio - UNAB 2022.";
    $var->slug = "congreso-internacional-de-vinculación-con-el-medio-UNAB-2022.";
    $var->bajada = "El objetivo central de este congreso fue compartir los principales desafíos de las instituciones de educación superior en el desarrollo de buenas prácticas de proyectos de Vinculación con el Medio. ";
    $var->cuerpo = "El evento conto con expositores de México, Brasil, Ecuador, Perú, Argentina, Colombia y España se congregaron a presentar saberes prácticos, metodológicos y teóricos de las distintas iniciativas de vinculación con el medio. En el caso de la Universidad de Talca, la docente Natalia Martinez Mondaca presentó la ponencia: Diseño instruccional como base para la implementación en la Metodología de A+S en modalidad on - line.";
    $var->imagen = "noticia2.jpg";
    $var->estado = "1";
    $var->user_id = "1";
    $var->save();

    $var = new Home_noticias();
    $var->titular = "Reconocimiento Impulsa - 2021.";
    $var->slug = "reconocimiento-impulsa-2021.";
    $var->bajada = "En su primera versión, el Reconocimiento a los Proyectos de Responsabilidad Social - IMPULSA 2021, estuvo dirigido a equipos de estudiantes que cursaron el módulo en el año académico 2021.";
    $var->cuerpo = "La invitación se realizó a los más de 340 proyectos de todas las carreras de la universidad que cursaron el módulo, para que los y las estudiantes visibilizaran ante un jurado académico el desarrollo de la intervención implementada con el socio comunitario y/o destinatarios de la comunidad. En esta oportunidad se analizaron 15 proyectos, quedando como finalistas 3 de ellos, obteniendo el 1er lugar el proyecto Apoyo para la nivelación de estudios a Población Juvenil C.C.P Cauquenes desarrollado por estudiantes de I.C. Industrial; en 2do lugar el proyecto Estimulación Cognitiva a adultos mayores del Programa Vínculos Curicó año 2021 de la carrera de Fonoaudiología; y en 3er lugar el proyecto Taller de Alfabetización Digital para madres- cuidadoras de Espacio Down implementado por estudiantes de las carreras de I.C. Mecatrónica e I.C. Mecánica. Felicitaciones a cada uno de los proyectos que se postularon, pues evidencian el potencial de las competencias genéricas y disciplinares, así como los valores institucionales, posibilitando el interés por ser agentes de cambio social. ";
    $var->imagen = "noticia3.jpg";
    $var->estado = "1";
    $var->user_id = "1";
    $var->save();

    $var = new Home_noticias();
    $var->titular = "Congreso de las Américas sobre Educación Internacional - 2021.";
    $var->slug = "congreso-de-las-américas-sobre-educación-internacional-2021.";
    $var->bajada = "El Congreso de las Américas sobre Educación International (CAEI) ";
    $var->cuerpo = "Es el foro continental de excelencia que reúne a los principales actores y tomadores de decisiones vinculados con la internacionalización de la educación superior en las Américas para fortalecer contactos, intercambiar experiencias y trazar el futuro de la cooperación académica en la región.
                        Este año, las docentes Carolina Fuentealba Cordero, Paula Lizana Ortiz presentaron los principales compromisos de trabajo con la comunidad evidenciando el modelo pedagógico de Responsabilidad Social, por otro lado, el docente y Joaquin Núñez Saez presentó poster virtual Innovando en la Formación Ciudadana en contexto de Pandemia. ";
    $var->imagen = "noticia4.jpg";
    $var->estado = "1";
    $var->user_id = "1";
    $var->save();

    $var = new Home_noticias();
    $var->titular = "Congreso de Vinculación con el Medio 2021 - UNAB.";
    $var->slug = "congreso-de-vinculación-con-el-medio-2021-UNAB.";
    $var->bajada = "Congreso de Vinculación con el Medio de la Universidad Andrés Bello . ";
    $var->cuerpo = " El congreso tuvo el objetivo de fortalecer la red de contactos entre académicos y funcionarios para el desarrollo de más y mejores iniciativas de VcM en Chile
        En esta ocasión, el docente Joaquín Núñez Saez presentó la experiencia de la Universidad de Talca en la formación de profesionales socialmente responsables.";
    $var->imagen = "noticia5.jpg";
    $var->estado = "1";
    $var->user_id = "1";
    $var->save();

    $var = new Home_noticias();
    $var->titular = "Premio MEIN - Premio Interamericano en Modelos Educativos Innovadores en Educación 2020.";
    $var->slug = "Premio-MEIN-2020.";
    $var->bajada = "El premio Mein 2020 Recogió un total de 39 experiencias en concurso, provenientes de 28 IES de 10 países.";
    $var->cuerpo = "Esta instancia permitió conocer iniciativas de todo el continente, que promueven prácticas y acciones innovadoras en procesos de enseñanza aprendizaje, investigación, transferencia de conocimiento y vinculación con Instituciones de Educación Superior. El galardón de esta actividad fue otorgado a la Universidad de Talca con la experiencia: Responsabilidad Social Universitaria, un modelo de formación aplicada a pregrado, la docente expositora en esta oportunidad fue Paula Andrea Lizana Ortiz.";
    $var->imagen = "noticia6.jpg";
    $var->estado = "1";
    $var->user_id = "1";
    $var->save();

    $var = new Home_noticias();
    $var->titular = "Concurso: Utalca y 100 palabras";
    $var->slug = "Utalca-y-100-palabras";
    $var->bajada = "Se inicia la recepción de microcuentos para la novena versión del concurso literario utalca y 100 palabras.";
    $var->cuerpo = "Si eres estudiante de pregrado o carreras técnicas, participa enviando tu microcuento de hasta 100 palabras 

        A los siguientes correos 
        Español: microcuentos@utalca.cl 
        Inglés francés o alemán: microcuentosidiomas@utalca.cl 
        Recuerda que el plazo es hasta el 29 de septiembre
        Revisa las bases en el siguiente link: ";
    $var->imagen = "noticia7.jpg";
    $var->estado = "1";
    $var->link = "https://udetalca-my.sharepoint.com/personal/mgrandon_utalca_cl/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fmgrandon%5Futalca%5Fcl%2FDocuments%2F2023%2FUTalca%20y%20100%20palabras%2FBases%20UTalca%20y%20100%20palabras%202023%2Epdf&parent=%2Fpersonal%2Fmgrandon%5Futalca%5Fcl%2FDocuments%2F2023%2FUTalca%20y%20100%20palabras&ga=1";
    $var->user_id = "1";
    $var->save();
  }
}
