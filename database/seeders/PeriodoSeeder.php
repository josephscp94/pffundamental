<?php

namespace Database\Seeders;

use App\Models\Periodo;
use Illuminate\Database\Seeder;

class PeriodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Periodo();
        $var->año = "2021";
        $var->save();

        $var = new Periodo();
        $var->año = "2022";
        $var->save();

        $var = new Periodo();
        $var->año = "2023";
        $var->save();
    }
}
