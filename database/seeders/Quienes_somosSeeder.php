<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Quienes_somos;

class Quienes_somosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Quienes_somos();
        $var->proposito = "El Programa de Formación Fundamental es pionero en la educación chilena. Creado e implementado por la Universidad de Talca a partir de 2006, busca desarrollar en los y las estudiantes habilidades comunicacionales, personales, interpersonales, de autogestión, liderazgo y manejo de conflictos, para integrar equipos de trabajo colaborativos.
         Semestralmente, el Programa atiende a más de 5000 estudiantes pertenecientes a las 38 carreras de pregrado y 5 carreras técnicas, desplegados en los 6 campus de la Universidad de Talca. Para ello, cuenta con un equipo de más de 30 docentes y profesionales.
          Como parte de la formación impartida por el Programa, el alumnado realiza proyectos de responsabilidad social desde sus áreas de conocimiento que los vinculan directamente con el territorio. Incluye un taller práctico de comunicación oral bajo la metodología de teatro aplicado en educación. 
          El Programa pretende que quienes estudian en la Universidad de Talca se formen de manera integral y no solo se centren en los conocimientos propios de sus carreras, pues el mundo laboral de hoy exige competencias interpersonales, ciudadanos con una visión global y socialmente responsables.";
        $var->url = "https://www.youtube.com/embed/MIYOp7SNb-k?si=FtwIATuZRy5s_n2a";
        $var->user_id = "1";
        $var->save();
    }
}
