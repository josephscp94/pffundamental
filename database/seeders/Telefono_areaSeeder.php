<?php

namespace Database\Seeders;

use App\Models\Telefono_area;
use Illuminate\Database\Seeder;

class Telefono_areaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Telefono_area();
        $var->nombre = "Asistente";
        $var->telefono = "XXXXXXXXX";
        $var->user_id = "1";
        $var->save();

        $var = new Telefono_area();
        $var->nombre = "Cordinación de Gestión";
        $var->telefono = "XXXXXXXXX";
        $var->user_id = "1";
        $var->save();

        $var = new Telefono_area();
        $var->nombre = "Área de Comunicación Oral y Escrita";
        $var->telefono = "XXXXXXXXX";
        $var->user_id = "1";
        $var->save();

        $var = new Telefono_area();
        $var->nombre = "Área Interpersonal";
        $var->telefono = "XXXXXXXXX";
        $var->user_id = "1";
        $var->save();

        $var = new Telefono_area();
        $var->nombre = "Área de Responsabilidad Social";
        $var->telefono = "XXXXXXXXX";
        $var->user_id = "1";
        $var->save();
    }
}
