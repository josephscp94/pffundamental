<?php

namespace Database\Seeders;

use App\Models\Telefono_area;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(AreaSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(FacultadSeeder::class);
        $this->call(OdsSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CarreraSeeder::class);
        $this->call(CompetenciaSeeder::class);
        $this->call(Quienes_somosSeeder::class);
        $this->call(Que_hacemosSeeder::class);
        $this->call(ColaboradorSeeder::class);
        $this->call(Home_carruselSeeder::class);
        $this->call(TestimoniosSeeder::class);
        $this->call(Home_noticiasSeeder::class);
        $this->call(Socio_comunitarioSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(PeriodoSeeder::class);
        $this->call(ProyectoSeeder::class);
        $this->call(ContactoSeeder::class);
        $this->call(Telefono_areaSeeder::class);
    }
}
