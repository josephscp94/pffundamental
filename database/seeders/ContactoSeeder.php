<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contacto;

class ContactoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Contacto();
        $var->correo = "formacionfundamental@utalca.cl";
        $var->telefono = "712200731";
        $var->instagram = "https://instagram.com/pffutalca?igshid=MmU2YjMzNjRlOQ==";
        $var->url_universidad = "https://www.utalca.cl";
        $var->url_vicerrectoria = "http://www.pregrado.utalca.cl";
        $var->descripcion = "El edificio del Programa de Formación Fundamental se encuentra ubicado en el lado sur de la Universidad de Talca, al costado derecho de las salas Arrayán y al frente de la Escuela de Ciencias Políticas y Administración Pública.";
        $var->user_id = "1";
        $var->save();
    }
}
