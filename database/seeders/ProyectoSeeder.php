<?php

namespace Database\Seeders;

use App\Models\Proyecto;
use App\Models\Alumno;
use Illuminate\Database\Seeder;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // proyecto 1
        $var = new Proyecto();
        $var->nombre = "Fondo Esperanza, Curicó";
        $var->slug = "fondo-esperanza-curicó";
        $var->descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vehicula lacus ipsum, ac tincidunt ante dapibus non";
        $var->imagen = "proyecto1.jpg";
        $var->carrera_id = "1";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "1";
        $var->save();
        $var->Tag()->sync([3, 7]);
        $var->Ods()->sync([1, 7]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Daniel Corvalan";
        $alumno1->proyecto_id = "1";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "Arlette Lobos";
        $alumno2->proyecto_id = "1";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Alejandra Cancino";
        $alumno3->proyecto_id = "1";
        $alumnos[] = $alumno3;
        $alumno4 = new Alumno();
        $alumno4->nombre = "Jose Rojas";
        $alumno4->proyecto_id = "1";
        $alumnos[] = $alumno4;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }

        // proyecto 2
        $var = new Proyecto();
        $var->nombre = "Programa Familias - Fosis Metropolitano";
        $var->slug = "programa-familias-fosis-metropolitano";
        $var->descripcion = "Estas temáticas son relevantes tanto para AFIS como para los/las usuarias del programa. Por una parte, constituye información relevante que el AFI debería manejar para compartir en las sesiones familiares, por ejemplo, la educación financiera. Por otro lado, son temáticas indispensables para las personas que desean tener o, ya tienen un microemprendimiento funcionando. Asimismo, esto vendría a complementar y a fortalecer al acompañamiento sociolaboral, el cual debido a la reducción metodológica se ha visto más afectado en su desarrollo.";
        $var->imagen = "proyecto2.jpg";
        $var->carrera_id = "1";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "5";
        $var->save();
        $var->Tag()->sync([4, 5]);
        $var->Ods()->sync([1, 8, 10]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Luis Ahumada";
        $alumno1->proyecto_id = "2";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "José Osses";
        $alumno2->proyecto_id = "2";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Ignacio Quezada";
        $alumno3->proyecto_id = "2";
        $alumnos[] = $alumno3;
        $alumno4 = new Alumno();
        $alumno4->nombre = "Isaac Vega";
        $alumno4->proyecto_id = "2";
        $alumnos[] = $alumno4;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }

        // proyecto 3
        $var = new Proyecto();
        $var->nombre = "Programa Mujer Jefas de Hogar, Municipalidad de Colbún";
        $var->slug = "programa-mujer-jefas-de-hogar-municipalidad-de-colbún";
        $var->descripcion = "Producto de la pandemia nos hemos adelantado en varios años a los cambios tecnológico, por lo que las emprendedoras requieren manejar las herramientas tecnológicas para su emprendimiento";
        $var->imagen = "proyecto3.jpg";
        $var->carrera_id = "1";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "5";
        $var->save();
        $var->Tag()->sync([3, 5, 7]);
        $var->Ods()->sync([5, 8, 10]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Cristóbal Agurto";
        $alumno1->proyecto_id = "3";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "Pablo Rojas";
        $alumno2->proyecto_id = "3";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Joaquín Vergara";
        $alumno3->proyecto_id = "3";
        $alumnos[] = $alumno3;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }

        // proyecto 4
        $var = new Proyecto();
        $var->nombre = "Fundación Envejecimiento Funcional";
        $var->slug = "Fundación-Envejecimiento-Funcional";
        $var->descripcion = "Parte del trabajo del Centro Diurno Referencial es trabajar directamente con la comunidad en beneficio de las personas mayores de la comuna.
        Para ello se han tenido avances significativos, pero que pudieran potenciarse a partir de un análisis específico de qué otros actores se pueden contactar.";
        $var->imagen = "proyecto4.jpg";
        $var->carrera_id = "14";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "6";
        $var->save();
        $var->Tag()->sync([10, 11]);
        $var->Ods()->sync([10, 16]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Ahylin Contreras Morales";
        $alumno1->proyecto_id = "4";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "Javiera Alvarado Martínez";
        $alumno2->proyecto_id = "4";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Pía Peralta Casanova";
        $alumno3->proyecto_id = "4";
        $alumnos[] = $alumno3;
        $alumno4 = new Alumno();
        $alumno4->nombre = "Francisca Vásquez Contreras";
        $alumno4->proyecto_id = "4";
        $alumnos[] = $alumno4;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }

        // proyecto 5
        $var = new Proyecto();
        $var->nombre = "Seremi Desarrollo Social Maule";
        $var->slug = "Seremi-Desarrollo-Social-Maule";
        $var->descripcion = "La SEREMI de desarrollo social mantiene en sus procesos duplicidad de trámites que inciden en el aumento innecesario de cargas de trabajo y el incremento de los tiempos de respuesta.";
        $var->imagen = "proyecto5.jpg";
        $var->carrera_id = "14";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "7";
        $var->save();
        $var->Tag()->sync([12]);
        $var->Ods()->sync([8]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Javiera Parra Olave";
        $alumno1->proyecto_id = "5";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "Yessenia Silva Romero";
        $alumno2->proyecto_id = "5";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Dylan Catalán";
        $alumno3->proyecto_id = "5";
        $alumnos[] = $alumno3;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }

        // proyecto 6
        $var = new Proyecto();
        $var->nombre = "Centro de Día para Adultos Mayores Junto a Ti Mejor y Feliz";
        $var->slug = "centro-de-Día-para-adultos-mayores-junto-a-ti-mejor-y-feliz";
        $var->descripcion = "Dar continuidad a proyecto iniciado en el año 2021 de implementación y habilitación de huerto comunitario en dependencias del Centro de Día";
        $var->imagen = "proyecto6.jpg";
        $var->carrera_id = "34";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "8";
        $var->save();
        $var->Tag()->sync([8, 13, 14]);
        $var->Ods()->sync([3, 12, 15]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Dilan Jara";
        $alumno1->proyecto_id = "6";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "Sebastian Bustamante";
        $alumno2->proyecto_id = "6";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Patrick Garrido";
        $alumno3->proyecto_id = "6";
        $alumnos[] = $alumno3;
        $alumno4 = new Alumno();
        $alumno4->nombre = "Cristian Vasquez";
        $alumno4->proyecto_id = "6";
        $alumnos[] = $alumno4;
        $alumno5 = new Alumno();
        $alumno5->nombre = "Maria Jose Muñoz";
        $alumno5->proyecto_id = "6";
        $alumnos[] = $alumno5;
        $alumno6 = new Alumno();
        $alumno6->nombre = "Diego Espinoza";
        $alumno6->proyecto_id = "6";
        $alumnos[] = $alumno6;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }

        // proyecto 7
        $var = new Proyecto();
        $var->nombre = "Taller Laboral Unpade";
        $var->slug = "taller-laboral-unpade";
        $var->descripcion = "La escuela Taller Laboral Unpade tiene invernaderos y huertos pero debido al cuidado del agua, es necesario reinventar";
        $var->imagen = "proyecto7.jpg";
        $var->carrera_id = "34";
        $var->periodo_id = "2";
        $var->socio_comunitario_id = "9";
        $var->save();
        $var->Tag()->sync([14]);
        $var->Ods()->sync([10, 11, 12]);
        $var->Users()->sync([1]);

        $alumnos = [];
        $alumno1 = new Alumno();
        $alumno1->nombre = "Cintia Andrade Cordero";
        $alumno1->proyecto_id = "7";
        $alumnos[] = $alumno1;
        $alumno2 = new Alumno();
        $alumno2->nombre = "Rodrigo Calderón Ahumada";
        $alumno2->proyecto_id = "7";
        $alumnos[] = $alumno2;
        $alumno3 = new Alumno();
        $alumno3->nombre = "Josué Carroza Fuentes";
        $alumno3->proyecto_id = "7";
        $alumnos[] = $alumno3;
        $alumno4 = new Alumno();
        $alumno4->nombre = "Bárbara Farías Gaete";
        $alumno4->proyecto_id = "7";
        $alumnos[] = $alumno4;
        $alumno5 = new Alumno();
        $alumno5->nombre = "Dante Valdivia Sánchez";
        $alumno5->proyecto_id = "7";
        $alumnos[] = $alumno5;
        foreach ($alumnos as $alumno) {
            $alumno->save();
        }
    }
}
