<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Competencia;

class CompetenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comp = new Competencia();
        $comp->nombre = "Comunicación Oral y Escrita";
        $comp->descripcion = "Comunicar discursos en forma oral y escrita, basándose en los recursos lingüísticos académicos para desempeñarse en situaciones del ámbito profesional.";
        $comp->modulo = "El desarrollo de habilidades comunicacionales orales y escritas -que se da en primer año- busca que los y las estudiantes sean capaces de comprender lo que leen, además de comunicarse en forma escrita y oral, usando sus recursos corporales en el taller de teatro aplicado.";
        $comp->imagen = "coe.jpg";
        $comp->save();
        $comp->Users()->sync([1]);

        $comp = new Competencia();
        $comp->nombre = "Habilidades Interpersonales";
        $comp->descripcion = "Integrar equipos de trabajo desarrollando habilidades sociales y de autogestión, para potenciar la capacidad de crear valor desde su profesión.";
        $comp->modulo = "Implica fortalecer el desarrollo del autoconocimiento para que los y las estudiantes enfrenten de mejor forma los desafíos de los distintos contextos. Además, se potencian habilidades sociales para construir relaciones basadas en la inclusión, el respeto, la responsabilidad, el liderazgo y la empatía, y así desempeñarse exitosamente a nivel individual y como parte de un equipo.";
        $comp->imagen = "habilidades_interpersonales.jpg";
        $comp->save();
        $comp->Users()->sync([1]);

        $comp = new Competencia();
        $comp->nombre = "Responsabilidad Social";
        $comp->descripcion = "Actuar con sentido ético y responsabilidad social en el ejercicio profesional con criterios ciudadanos para el desarrollo sustentable del entorno.";
        $comp->modulo = "Implica desarrollar en los y las estudiantes la capacidad de responder ante sí mismo y la sociedad por acciones u omisiones vinculadas al ejercicio de su profesión en los distintos ámbitos en los que se desempeñan para satisfacer necesidades que aporten al desarrollo sustentable, considerando aspectos sociales, culturales, éticos y ambientales. A su vez, incluye orientar y evaluar actividades individuales y colectivas desde su disciplina hacia el desarrollo y el bien de la sociedad.";
        $comp->imagen = "responsabilidad_social.jpg";
        $comp->save();
        $comp->Users()->sync([1]);
    }
}
