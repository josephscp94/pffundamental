<?php

namespace Database\Seeders;

use App\Models\Rol;
use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new Rol();
        $rol->nombre = "Directora";
        $rol->area_id = "1";
        $rol->save();

        $rol = new Rol();
        $rol->nombre = "Asistente";
        $rol->area_id = "1";
        $rol->save();

        $rol = new Rol();
        $rol->nombre = "Coordinador de los módulos de 4to año";
        $rol->area_id = "1";
        $rol->save();

        $rol = new Rol();
        $rol->nombre = "Docente Cátedra";
        $rol->area_id = "2";
        $rol->save();

        $rol = new Rol();
        $rol->nombre = "Profesionales de Vinculación con el Medio";
        $rol->area_id = "3";
        $rol->save();
    }
}
