<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Colaborador;

class ColaboradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new Colaborador();
        $var->nombre = "Natalia Martínez Mondaca";
        $var->correo = "nmartinez@utalca.cl";
        $var->imagen = "Natalia Martínez Mondaca.jpeg";
        $var->save();
        $var->Rol()->sync([4]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Paula Lizana Ortiz";
        $var->correo = "plizana@utalca.cl";
        $var->imagen = "Paula Lizana Ortiz.jpeg";
        $var->save();
        $var->Rol()->sync([4]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Maderline Grandón Avendaño";
        $var->correo = "mgrandon@utalca.cl";
        $var->imagen = "Maderline Grandón Avendaño.jpeg";
        $var->save();
        $var->Rol()->sync([4]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Mariapaz Rojas Reyes";
        $var->correo = "maria.rojas@utalca.cl";
        $var->imagen = "Mariapaz Rojas Reyes.jpeg";
        $var->save();
        $var->Rol()->sync([5]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Valentina Zúñiga Ahumada";
        $var->correo = "valentina.zuniga@utalca.cl";
        $var->imagen = "Valentina Zúñiga Ahumada.jpeg";
        $var->save();
        $var->Rol()->sync([5]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Mónica Águila Díaz";
        $var->correo = "monica.aguila@utalca.cl";
        $var->imagen = "Mónica Águila Díaz.jpeg";
        $var->save();
        $var->Rol()->sync([5]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Macarena Pérez Bravo";
        $var->correo = "macarena.perez@utalca.cl";
        $var->imagen = "Macarena Pérez Bravo.jpeg";
        $var->save();
        $var->Rol()->sync([4]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Joaquín Núñez Sáez";
        $var->correo = "jonunez@utalca.cl";
        $var->imagen = "Joaquín Núñez Sáez.jpeg";
        $var->save();
        $var->Rol()->sync([3, 4]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Angie Riquelme Escobar";
        $var->correo = "angie.riquelme@utalca.cl ";
        $var->imagen = "Angie Riquelme Escobar.jpeg";
        $var->save();
        $var->Rol()->sync([5]);
        $var->Users()->sync([1]);

        $var = new Colaborador();
        $var->nombre = "Perla Olivera Castillo";
        $var->correo = "perla.olivera@utalca.cl";
        $var->imagen = "Perla Olivera Castillo.jpeg";
        $var->save();
        $var->Rol()->sync([5]);
        $var->Users()->sync([1]);
    }
}
