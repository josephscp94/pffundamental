<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQueHacemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('que_hacemos', function (Blueprint $table) {
            $table->id();
            $table->string('compromiso_institucional', 3000);
            $table->string('imagen_ci', 250);
            $table->string('docencia', 3000);
            $table->string('imagen_dc', 250);
            $table->string('vinculacion_con_el_medio', 3000);
            $table->string('imagen_vcm', 255);
            $table->foreignId('user_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('que_hacemos');
    }
}
