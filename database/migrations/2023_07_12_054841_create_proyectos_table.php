<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->string('slug', 100);
            $table->string('descripcion', 3000);
            $table->string('imagen', 120);
            $table->foreignId('carrera_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('socio_comunitario_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('periodo_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
