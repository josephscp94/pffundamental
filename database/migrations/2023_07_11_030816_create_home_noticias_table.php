<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_noticias', function (Blueprint $table) {
            $table->id();
            $table->string('titular', 250);
            $table->string('slug', 250);
            $table->string('bajada', 250);
            $table->string('cuerpo', 10000);
            $table->string('imagen', 120);
            $table->boolean('estado')->default(1); //0:Desabilitado 1: Habilitado
            $table->string('link', 1000)->nullable();
            $table->timestamps();
            $table->foreignId('user_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_noticias');
    }
}
