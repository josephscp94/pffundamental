<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeCarruselsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_carrusels', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion', 100);
            $table->string('imagen', 200);
            $table->boolean('estado')->default(1); //0:Desabilitado 1: Habilitado
            $table->string('link', 1000)->nullable();
            $table->foreignId('user_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_carrusels');
    }
}
