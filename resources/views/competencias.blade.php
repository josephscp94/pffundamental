<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Competencias genéricas</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <link href="{{ asset('css/competencias.css') }}" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

</head>
<section>
  @yield('scrollup')
</section>

<body>
  <header>
    @include('layouts.head')
  </header>

  <body>
    <main>
      <div class="container-fluid page-header mb-5 p-0" style=" background-image: url('/img/heading.png')">
        <div class="container-fluid page-header-inner py-5" >
          <div class="container text-center">
            <h1 class="display-3 text-white mb-3 animated slideInDown">Competencias genéricas</h1>

          </div>
        </div>
      </div>
    </main>

    <div class="container" id="container-competencias">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb justify-content-left ">
          <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
          <li class="breadcrumb-item text-black active" aria-current="page"><strong>Competencias genéricas</strong></li>
        </ol>
      </nav>
      <div class="row g-4 " data-wow-delay="0.3s">
        <div class="col-lg-4">
          <div class="nav w-100 nav-pills me-4">
            <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4 active" data-bs-toggle="pill"
              data-bs-target="#tab-pane-1" type="button">
              <img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
              <h4 class="m-0">{{ $competencias[0]->nombre }}</h4>
            </button>
            <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill"
              data-bs-target="#tab-pane-2" type="button">
              <img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">

              <h4 class="m-0">{{ $competencias[1]->nombre }}</h4>
            </button>
            <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill"
              data-bs-target="#tab-pane-3" type="button">
              <img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
              <h4 class="m-0">{{ $competencias[2]->nombre }}</h4>
            </button>

          </div>
        </div>
        <div class="col-lg-8">
          <div class="tab-content w-100">

            <div class="tab-pane fade show active" id="tab-pane-1">
              <div class="row g-4">
                <menu>
                  <h1 class="titulos"> Módulos de {{ $competencias[0]->nombre }}</h1>
                  <div id="accordion">
                    <br>
                    <div class="card-body">
                      <?php $parrafos = explode("\n",  $competencias[0]->modulo ) ?>
                      @foreach($parrafos as $parrafo)
                      @if(trim($parrafo) != "")
                      <h5 class="parrafo">{{$parrafo}}</h5>
                      @endif
                      @endforeach
                    </div>
                  </div>
                </menu>
              </div>
              <div class="row g-4">
                <div class="col-md-12">
                  <h1 class="titulos">Descripción de la competencia</h1>
                  <?php $parrafos = explode("\n",$competencias[0]->descripcion ) ?>
                  @foreach($parrafos as $parrafo)
                  @if(trim($parrafo) != "")
                  <h5 class="parrafo">{{$parrafo}}</h5>
                  @endif
                  @endforeach

                </div>
                <div class="col-md-12" style="min-height: 350px;">
                  <div class="position-relative h-100">
                    <img class="position-absolute img-fluid w-100 h-100"
                      src="/img/admin/competencia_transversal/{{ $competencias[0]->imagen }}" style="object-fit: cover;"
                      alt="">
                  </div>
                </div>

              </div>
            </div>

            <div class="tab-pane fade" id="tab-pane-2">
              <div class="row g-4">
                <menu>
                  <h1 class="titulos">Módulos de {{ $competencias[1]->nombre }}</h1>
                  <div id="accordion">
                    <br>
                    <div class="card-body">
                      <?php $parrafos = explode("\n",  $competencias[1]->modulo ) ?>
                      @foreach($parrafos as $parrafo)
                      @if(trim($parrafo) != "")
                      <h5 class="parrafo">{{$parrafo}}</h5>
                      @endif
                      @endforeach
                    </div>
                  </div>
                </menu>
              </div>
              <div class="row g-4">
                <div class="col-md-12">
                  <h1 class="titulos">Descripción de la competencia</h1>
                  <?php $parrafos = explode("\n",$competencias[1]->descripcion ) ?>
                  @foreach($parrafos as $parrafo)
                  @if(trim($parrafo) != "")
                  <h5 class="parrafo">{{$parrafo}}</h5>
                  @endif
                  @endforeach

                </div>
                <div class="col-md-12" style="min-height: 350px;">
                  <div class="position-relative h-100">
                    <img class="position-absolute img-fluid w-100 h-100"
                      src="/img/admin/competencia_transversal/{{ $competencias[1]->imagen }}" style="object-fit: cover;"
                      alt="">
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="tab-pane-3">
              <div class="row g-4">
                <menu>
                  <h1 class="titulos">Módulos de {{ $competencias[2]->nombre }}</h1>
                  <div id="accordion">
                    <br>
                    <div class="card-body">
                      <?php $parrafos = explode("\n",  $competencias[2]->modulo ) ?>
                      @foreach($parrafos as $parrafo)
                      @if(trim($parrafo) != "")
                      <h5 class="parrafo">{{$parrafo}}</h5>
                      @endif
                      @endforeach
                    </div>
                    <br>
                  </div>
                  <div class="col-lg-12">
                    <div class="col-md-6 " id="carreras">
                      <div id="azul"
                        style=" border-radius: 10px;height: 50px;background-color: #dc6b6b;text-align: center; padding-left:5px;">
                        <p style="padding-top: 10px;">
                          <a id="btn-competencia" style=" color: #fff; font-weight: bold; text-decoration: none;"
                            href="{{ route('carreras') }}">Proyectos</a>
                        </p>
                      </div>
                    </div>

                    <div class="col-md-6" id="carreras">
                      <div id="azul"
                        style=" border-radius: 10px;height: 50px;background-color: #dc6b6b;text-align: center; padding-left:5px">
                        <p style="padding-top: 10px;">
                          <a id="btn-competencia" href="{{ route('socios-comunitarios') }}"
                            style=" color: #fff; font-weight: bold; text-decoration: none;">Socios
                            comunitarios</a>
                        </p>
                      </div>
                    </div>
                  </div>
                </menu>
              </div>

              <br>
              <div class="row g-4">
                <div class="col-md-12">
                  <h1 class="titulos">Descripción de la competencia</h1>
                  <?php $parrafos = explode("\n",$competencias[2]->descripcion ) ?>
                  @foreach($parrafos as $parrafo)
                  @if(trim($parrafo) != "")
                  <h5 class="parrafo">{{$parrafo}}</h5>
                  @endif
                  @endforeach
                </div>
                <div class="col-md-12" style="min-height: 350px;">
                  <div class="position-relative h-100">
                    <img class="position-absolute img-fluid w-100 h-100"
                      src="/img/admin/competencia_transversal/{{ $competencias[2]->imagen }}" style="object-fit: cover;"
                      alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>

</body>
<script type="text/javascript" src="{{ asset('./js/competencias.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

<footer>
  @include('layouts.footer')
</footer>

</html>