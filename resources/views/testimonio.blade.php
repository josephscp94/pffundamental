<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <!-- Libraries Stylesheet -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap1.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
</head>

<!-- Testimonial Start -->
<div class="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
    <div class="container">
        <div class="text-center">
            {{-- <h6 class="text-primary text-uppercase">Testimonios</h6> --}}
            <h1 class="mb-5">Testimonios</h1>
        </div>
        <br>
        <div class="owl-carousel testimonial-carousel position-relative">
            @forelse ($testimonios as $testimonio)
            <div class="testimonial-item text-center-1">
                <h5 class="mb-0">{{ $testimonio->nombre }}</h5>
                <p class="testimonio">{{ $testimonio->cargo }}</p>
                <p class="text-center-1">{{ $testimonio->created_at->format('d M, y') }}</p>
                <br>
                <img class="p-2 mx-auto mb-3" src="/img/admin/testimonios/{{ $testimonio->imagen }}"
                    style="aspect-ratio: 1/1;">
                <br>
                <div class="testimonial-text text-center-1 p-4" height="390" width="540">
                    <p class="mb-0" height="540" width="540" overflow:auto>{{$testimonio->parrafo}} </p>
                </div>


            </div>
            @empty
            @endforelse
        </div>
    </div>
</div>
<!-- Testimonial End -->


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="lib/wow/wow.min.js"></script>
<script src="lib/counterup/counterup.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/tempusdominus/js/moment.min.js"></script>
<script src="lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

<!-- Template Javascript -->
<script src="js/testimonio.js"></script>
</body>

</html>