<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Formación Fundamental | Utalca</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap1.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/home_noticias.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
</head>

<!-- Trending Area Start -->
<div class="trending-area fix pt-25 gray-bg">
    <div class="container">

        <div class="trending-main">
            <div class="row">
                <div class="text-center">
                    {{-- <h6 class="text-primary text-uppercase">Testimonios</h6> --}}
                    <h1 class="mb-5">Nuestras Noticias</h1>
                    <br>
                </div>
                <div class="col-lg-4">
                    <!-- Trending Top -->
                    <div class="slider-active">
                        <!-- Single -->

                        @forelse ($home_noticias as $key => $noticia)
                        @if ($key >= 0)
                        <div class="single-slider" style="background-size: cover;width: 100%;display: inline-block;">
                            <div class="trending-top mb-30" id="noticia2">
                                <div width="480" height="380" class="trend-top-img image-container" id="noticia2">
                                    <a href="{{route('noticias',$noticia->slug)}}"> <img id="noticia2" width="480"
                                            height="380" src="/img/admin/home_noticias/{{ $noticia->imagen }}"
                                            alt=""></a>
                                    <div class="trend-top-cap">
                                        <h2 id="titular-h4"><a id="titular-h4"
                                                href="{{route('noticias',$noticia->slug)}}" data-animation="fadeInUp"
                                                data-delay=".4s" data-duration="400ms">{{
                                                $noticia->titular }}</a></h2>
                                        <p data-animation="fadeInUp" data-delay=".4s" data-duration="400ms">{{
                                            $noticia->created_at->format('d M, y') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @empty
                        @endforelse
                    </div>
                </div>
                <!-- Right content -->
                <div class="col-lg-4">
                    <!-- Trending Top -->
                    <div class="slider-active">
                        <!-- Single -->

                        @forelse ($home_noticias as $key => $noticia)
                        @if ($key >= 1)
                        <div class="single-slider" style="background-size: cover;width: 100%;display: inline-block;">
                            <div class="trending-top mb-30" id="noticia2">
                                <div width="480" height="380" class="trend-top-img image-container" id="noticia2">
                                    <a href="{{route('noticias',$noticia->slug)}}"> <img id="noticia2" width="480"
                                            height="380" src="/img/admin/home_noticias/{{ $noticia->imagen }}"
                                            alt=""></a>
                                    <div class="trend-top-cap">
                                        <h2 id="titular-h4"><a id="titular-h4"
                                                href="{{route('noticias',$noticia->slug)}}" data-animation="fadeInUp"
                                                data-delay=".4s" data-duration="400ms">{{
                                                $noticia->titular }}</a></h2>
                                        <p data-animation="fadeInUp" data-delay=".4s" data-duration="400ms">{{
                                            $noticia->created_at->format('d M, y') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @empty
                        @endforelse
                    </div>
                </div>

                <div class="col-lg-4">
                    <!-- Trending Top -->
                    <div class="slider-active">
                        <!-- Single -->

                        @forelse ($home_noticias as $key => $noticia)
                        @if ($key >= 2)
                        <div class="single-slider" style="background-size: cover;width: 100%;display: inline-block;">
                            <div class="trending-top mb-30" id="noticia2">
                                <div width="480" height="380" class="trend-top-img image-container" id="noticia2">
                                    <a href="{{route('noticias',$noticia->slug)}}"> <img id="noticia2" width="480"
                                            height="380" src="/img/admin/home_noticias/{{ $noticia->imagen }}"
                                            alt=""></a>
                                    <div class="trend-top-cap">
                                        <h2 id="titular-h4"><a id="titular-h4"
                                                href="{{route('noticias',$noticia->slug)}}" data-animation="fadeInUp"
                                                data-delay=".4s" data-duration="400ms">{{
                                                $noticia->titular }}</a></h2>
                                        <p data-animation="fadeInUp" data-delay=".4s" data-duration="400ms">{{
                                            $noticia->created_at->format('d M, y') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Jquery, Popper, Bootstrap -->
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="js/owl.carousel.min.js"></script>
<script src="js/slick.min.js"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="js/home_noticias1.js"></script>

</body>

</html>