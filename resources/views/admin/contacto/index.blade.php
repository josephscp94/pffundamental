@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Contacto</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/contacto">Contacto</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>

            {{-- <a href="{{ route('quienes_somos.create') }}" class="btn btn-success">Nuevo</a> --}}
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Correo</th>
                            <th scope="col">Teléfono de PFF</th>
                            <th scope="col">Enlace Instagram</th>
                            <th scope="col">URL Universidad</th>
                            <th scope="col">URL Vicerrectoría</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($contacto as $item)
                        <tr>
                            <td>{{$item->correo}}</td>
                            <td>{{$item->telefono}}</td>
                            <td>{{$item->instagram}}</td>
                            <td>{{$item->url_universidad}}</td>
                            <td>{{$item->url_vicerrectoria}}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$item->descripcion}}</td>
                        </tr>
                        <td><a href="{{ route('contacto.edit', $item->id) }}" class="btn btn-success"
                                style="margin-bottom: 10px;">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>
                        </td>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Teléfonos de las áreas</h1>
                </div><!-- /.col -->
            </div>
            <a href="{{ route('telefono_area.create') }}" class="btn btn-success">Nuevo</a>
            <table class="table table-striped">
                <thead>
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($telefonos as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->telefono }}</td>
                        <td>
                            <a href="{{ route('telefono_area.edit', $item->id) }}" class="btn btn-success">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>
                            <form method="POST" action="{{ route('telefono_area.destroy', $item->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection