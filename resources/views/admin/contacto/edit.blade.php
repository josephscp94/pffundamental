@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Contacto</h1>
                </div><!-- /.col -->
            </div>
            <br>
            <form action="{{ route('contacto.update',$contacto) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="correo">Correo de PFF</label>
                        <input type="text" name="correo" class="form-control" maxlength="50" required
                            value="{{ $contacto->correo }}">
                    </div>

                    <div class="form-group">
                        <label for="telefono">Telefono del footer</label>
                        <input type="text" name="telefono" class="form-control" maxlength="20" required
                            value="{{ $contacto->telefono }}">
                    </div>

                    <div class="form-group">
                        <label for="Instagram">Instagram de PFF</label>
                        <input type="text" name="Instagram" class="form-control" maxlength="100" required
                            value="{{ $contacto->instagram }}">
                    </div>

                    <div class="form-group">
                        <label for="url_universidad">Url de la universidad</label>
                        <input type="text" name="url_universidad" class="form-control" maxlength="100" required
                            value="{{ $contacto->url_universidad }}">
                    </div>

                    <div class="form-group">
                        <label for="url_vicerrectoria">Url de Vicerrectoria</label>
                        <input type="text" name="url_vicerrectoria" class="form-control" maxlength="100" required
                            value="{{ $contacto->url_vicerrectoria }}">
                    </div>

                    <div class=" form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="1000" required
                            value="{{ $contacto->descripcion }}">{{ $contacto->descripcion }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-success">Editar</button>
            </form>

        </div>
    </div>
</div>
@endsection