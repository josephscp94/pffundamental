@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Colaboradores</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/colaborador">Colaboradores</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <hr>
            <div class="col-sm-12">
                <form class="form-inline ml-3 float-right">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Buscar"
                            aria-label="Search">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la búsqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
                </h6>
            </div>
            <a href="{{ route('colaborador.create') }}" class="btn btn-success">Nuevo</a>
            <a href="{{ route('rol.index') }}" class="btn btn-success">Roles</a>
            <table class="table table-striped">
                <thead>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Rol</th>
                    <th>Correo</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($colaboradores as $colaborador)
                    <tr>
                        <td>{{ $colaborador->nombre }}</td>
                        <td><img src="/img/admin/colaborador/{{ $colaborador->imagen }}" alt="" width="50" height="50">
                        </td>
                        <td>
                            @forelse ($colaborador->Rol as $rol)
                            {{ $rol->nombre; }}
                            <br>
                            @empty
                            @endforelse
                        </td>
                        <td>{{ $colaborador->correo }}</td>
                        <td>
                            <a href="{{ route('colaborador.edit', $colaborador->id) }}" class="btn btn-success">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>

                            <form method="POST" action="{{ route('colaborador.destroy', $colaborador->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
            {{$colaboradores->links() }}
        </div>
    </div>
</div>
@endsection