@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Colaborador</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/colaborador">Colaborador</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$colaborador->nombre}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('colaborador.update',$colaborador) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required
                            value="{{ $colaborador->nombre }}">
                    </div>

                    <div class="form-group">
                        <label for="roles">Rol</label>
                        <select name="roles[]" id="roles" class="form-control selectpicker" data-style="btn-primary"
                            title="Seleccionar rol" multiple required>
                            @foreach ($roles as $rol)
                            <option value="{{ $rol->id }}" {{ $colaborador->rol->contains($rol->id) ? 'selected' : ''
                                }}>
                                {{ $rol->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="correo">Ingresar correo</label>
                        <input type="text" name="correo" class="form-control" maxlength="50" required
                            value="{{ $colaborador->correo }}">
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $colaborador->imagen }}</p>
                        <img src="/img/admin/colaborador/{{ $colaborador->imagen }}" style="max-width: 300px;">
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Editar</button>
            </form>

        </div>
    </div>
</div>
@endsection