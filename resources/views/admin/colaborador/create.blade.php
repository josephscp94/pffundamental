@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Colaboradores</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/colaborador">Colaboradores</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'Colaboradores'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('colaborador.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required>
                    </div>

                    <div class="form-group">
                        <label for="roles">Ingresar roles</label>
                        <select name="roles[]" id="roles" class="form-control selectpicker" data-style="btn-primary"
                            title="Seleccionar roles" multiple required>
                            @foreach ($roles as $rol)
                            <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="correo">Ingresar correo</label>
                        <input type="text" name="correo" class="form-control" maxlength="50" required>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <img src="/img/admin/colaborador/colaborador.jpg" alt="">
                        <input type="file" name="imagen" required>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Crear</button>
            </form>

        </div>
    </div>
</div>
@endsection