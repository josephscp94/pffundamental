@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> Crear un nuevo rol</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/rol">Rol</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'rol'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('rol.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar rol</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="area_id">Área Académica</label>
                        <select name="area_id" class="form-control selectpicker" data-style="btn-primary"
                            title="Elija una área académica" required>
                            @foreach ($areas as $area)
                            <option value="{{ $area->id }}">{{ $area->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Crear</button>
            </form>
        </div>
    </div>
</div>
@endsection