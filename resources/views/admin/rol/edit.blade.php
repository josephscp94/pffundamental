@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> Editar un rol</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/rol">Rol</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$rol->nombre}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <form action="{{ route('rol.update',$rol) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')
                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar rol</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100"
                            value="{{ $rol->nombre }}">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="area_id">Área Académica</label>
                        <select name="area_id" class="form-control selectpicker" data-style="btn-primary"
                            title="{{ $rol->area->año }}">
                            @foreach ($areas as $area)
                            <option value="{{ $area->id }}" {{ $rol->area_id == $area->id ? 'selected' :
                                '' }}>
                                {{ $area->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection