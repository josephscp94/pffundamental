@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Competencia genéricas</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/competecia">Competencia
                                genéricas</a></a></li>

                    </ol>
                </div><!-- /.col -->
            </div>
            {{-- <a href="{{ route('competencia.create') }}" class="btn btn-success">Nuevo</a> --}}
            <table class="table table-striped">
                <thead>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Descripción de módulos</th>
                    <th>Imagen</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($competencia as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{$item->descripcion}}</td>
                        <td>{{$item->modulo}}</td>
                        <td><img src="/img/admin/competencia_transversal/{{ $item->imagen }}" alt="" width="50"
                                height="50"></td>

                        <td>
                            <a href="{{ route('competencia.edit', $item->id) }}" class="btn btn-success"
                                style="margin-bottom: 10px;">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>

                            <form method="POST" action="{{ route('competencia.destroy', $item->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection