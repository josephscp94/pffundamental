@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Listado de periodos </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin/periodo">Periodos</a></li>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div><!-- /.col -->
                </div>
                <a href="{{ route('periodo.create') }}" class="btn btn-success">Nuevo</a>
                <table class="table table-striped">
                    <thead>
                        <th>Año</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                        @forelse ($periodos as $item)
                        <tr>
                            <td>{{ $item->año }}</td>
                            <td>
                                <a href="{{ route('periodo.edit', $item->id) }}" class="btn btn-success"
                                    style="margin-bottom: 10px;">
                                    <img src="/svg/edit.svg" alt="home-image" width="20">
                                </a>
                            </td>
                        </tr>
                        @empty
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection