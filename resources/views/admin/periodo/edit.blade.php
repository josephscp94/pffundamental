@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar: Periodos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/periodo">Periodos</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$periodos->nombre}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            {!! Form::open(['route'=>['periodo.update',$periodos],'method'=>'PUT','files'=>true]) !!}

            <div class="jumbotron">
                <div class="form-group">
                    <label for="año">Ingresar periodo</label>
                    {!! Form::text('año',$periodos->año,['class'=>'form-control','maxlength'=>'4']) !!}
                </div>
            </div>

            {!! Form::submit('Editar',['class'=>'btn btn-success']) !!}
            {!! Form::close()!!}

            <form action="{{ route('periodo.update',$periodos) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="año">Ingresar año</label>
                        <input type="text" name="año" class="form-control" maxlength="4" value="{{ $periodos->año }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection