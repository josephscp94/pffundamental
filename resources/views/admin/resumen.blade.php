@extends('layouts.menu')

@section('content')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{$cons_proyectos}}</h3>

                        <p>Proyectos</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-cash-register"></i>
                    </div>
                    <a href="{{url('/admin/proyecto')}}" class="small-box-footer">Tabla <i
                            class="fas fa-arrow-circle-right"></i></a>
                    <a href="{{url('/admin/grafico_carreras')}}" class="small-box-footer">Grafico <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{$cons_sc}}</h3>

                        <p>Socios Comunitarios</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="{{url('/admin/socio_comunitario')}}" class="small-box-footer">Tabla <i
                            class="fas fa-arrow-circle-right"></i></a>
                    <a href="{{url('/admin/grafico_socios')}}" class="small-box-footer">Grafico <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$cons_tags}}</h3>
                        <p>Tags</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="{{url('/admin/tag')}}" class="small-box-footer">Tabla <i
                            class="fas fa-arrow-circle-right"></i></a>
                    <a href="{{url('/admin/grafico_tags')}}" class="small-box-footer">Grafico <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-dark">
                    <div class="inner">
                        <h3>{{$cons_ods}}</h3>
                        <p>Ods</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="{{url('/admin/ods')}}" class="small-box-footer">Tabla <i
                            class="fas fa-arrow-circle-right"></i></a>
                    <a href="{{url('/admin/grafico_ods')}}" class="small-box-footer">Grafico <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <p>Periodos</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="{{url('/admin/periodo')}}" class="small-box-footer">Tabla <i
                            class="fas fa-arrow-circle-right"></i></a>
                    <a href="{{url('/admin/grafico_periodos')}}" class="small-box-footer">Grafico <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>{{$cons_solicitudes}}</h3>
                        <p>Solicitudes</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="{{url('/admin/solicitudes')}}" class="small-box-footer">Ver <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-light ">
                    <div class="inner">
                        <h3>Google Analytics</h3>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="https://analytics.google.com/analytics/web/#/p409819608/reports/intelligenthome?params=_u..nav%3Dmaui"
                        class="small-box-footer" target="_blank">Link <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

        </div>
    </div>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    {{-- <div class="card-header">{{ __('Dashboard') }}</div> --}}

                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif

                        Hola: {{ Auth::user()->name }} has Iniciado Sesion de manera correcta
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection