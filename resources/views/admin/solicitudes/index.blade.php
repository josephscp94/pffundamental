@extends('layouts.menu')
@section('content')

<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.0.37/jspdf.plugin.autotable.js">
    </script>

</head>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Solicitudes</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/solicitudes">Solicitudes</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <hr>
            <div class="col-sm-12">
                <form class="form-inline ml-3 float-right">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Buscar"
                            aria-label="Search">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la búsqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
                </h6>
            </div>

            <body>
                <button onclick="generatePDF()">EXPORTAR PDF</button>
                <br />
                <br />

                <table class="table table-striped" id="table_with_data" border="1" cellspacing="0" bordercolor="gray">
        
                    <thead>
                        <th>Nombre de la institución</th>
                        <th>Descripción</th>
                        <th>Nombre de la persona</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Fecha</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                        @forelse ($solicitudes as $solicitud)
                        <tr>
                            <td>{{ $solicitud->nombre_institucion }}</td>
                            <td>{{ $solicitud->descripcion }}</td>
                            <td>{{ $solicitud->nombre_persona }}</td>
                            <td>{{ $solicitud->correo }}</td>
                            <td>{{ $solicitud->telefono }}</td>
                            <td>{{ $solicitud->created_at->format('d \d\e F, Y H:i') }}</td>
                            <td>
                                <form method="POST" action="{{ route('solicitudes.destroy', $solicitud->id) }}"
                                    style="display:inline">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger"
                                        onclick="return confirm('¿Desea eliminar este registro?')">
                                        <img src="/svg/delete.svg" alt="home-image" width="20">
                                    </button>

                                </form>
                            </td>
                        </tr>
                        @empty
                        @endforelse
                    </tbody>

                </table>
            </body>
            {{$solicitudes->links() }}
        </div>
    </div>
</div>
@endsection

<script>
    function generatePDF() {
 var doc = new jsPDF('l', 'pt');

 var elem = document.getElementById('table_with_data');
 var data = doc.autoTableHtmlToJson(elem);

 // Excluir la columna 'Acción'
 data.columns = data.columns.filter(column => column.name !== 'Acción');
 data.rows = data.rows.map(row => {
 delete row['Acción'];
 return row;
 });

 // Agregar título
 doc.setFontSize(16);
 doc.text('Solicitudes entrantes de socios comunitarios', 50, 40);

 // Agregar tabla con margen superior
 var startY = 60; // Ajusta este valor según el tamaño de tu título
 doc.autoTable(data.columns, data.rows, {
 startY: startY,
 margin: {left: 35},
 theme: 'grid',
 tableWidth: 'auto',
 fontSize: 8,
 overflow: 'linebreak',
 });

 doc.save('Solicitudes de socios comunitarios.pdf');
}


</script>