<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/svg/Logo_blanco-2.svg">
    <title>Administrador| Grafico</title>
    <link href="{{ asset('css/grafico.css') }}" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Kanit:wght@600&family=Oswald:wght@200;400&family=Roboto:wght@700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <a href="/admin/resumen"><button class="bn632-hover bn26">Volver</button></a>
    <div class="titulo">
        <h1>Periodos por proyectos</h1>
    </div>
    <br>
    <canvas id="chart">

        <canvas id="chart"></canvas>

        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script>
            var labels = @json($labels);
            var data = @json($data);
        
            var ctx = document.getElementById('chart').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Cantidad de Proyectos',
                        data: data,
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1
                    }]
                },
                // options: {
                //     responsive: true, // Hace que el gráfico se ajuste al tamaño del contenedor
                //     maintainAspectRatio: false, // Permite cambiar el tamaño del gráfico sin mantener la relación de aspecto
                //     scales: {
                //         y: {
                //             beginAtZero: true,
                //             precision: 0
                //         }
                //     }
                // }
            });
        </script>
</body>

</html>