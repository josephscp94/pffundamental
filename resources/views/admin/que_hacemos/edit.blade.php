@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Qué hacemos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/que_hacemos">Qué hacemos</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{'que_hacemos'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('que_hacemos.update',$quehacemos) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">

                    <div class=" form-group">
                        <label for="compromiso_institucional">Compromiso institucional</label>
                        <textarea name="compromiso_institucional" class="form-control" maxlength="3000"
                            value="{{ $quehacemos->compromiso_institucional }}">{{ $quehacemos->compromiso_institucional }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen_ci">Editar imagen del compromiso institucional</label>
                        <br>
                        <input type="file" name="imagen_ci">
                        <p>Nombre de la imagen cargada: {{ $quehacemos->imagen_ci }}</p>
                        <img src="/img/admin/que_hacemos/{{ $quehacemos->imagen_ci }}" style="max-width: 300px;">
                    </div>

                    <div class=" form-group">
                        <label for="docencia">Docencia académica</label>
                        <textarea name="docencia" class="form-control" maxlength="3000"
                            value="{{ $quehacemos->docencia }}">{{ $quehacemos->docencia }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen_dc">Editar imagen de docencia académica</label>
                        <br>
                        <input type="file" name="imagen_dc">
                        <p>Nombre de la imagen cargada: {{ $quehacemos->imagen_dc }}</p>
                        <img src="/img/admin/que_hacemos/{{ $quehacemos->imagen_dc }}" style="max-width: 300px;">
                    </div>

                    <div class=" form-group">
                        <label for="vinculacion_con_el_medio">Vinculación con el medio</label>
                        <textarea name="vinculacion_con_el_medio" class="form-control" maxlength="3000"
                            value="{{ $quehacemos->vinculacion_con_el_medio }}">{{ $quehacemos->vinculacion_con_el_medio }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen_vcm">Editar imagen de vinculación con el medio</label>
                        <br>
                        <input type="file" name="imagen_vcm">
                        <p>Nombre de la imagen cargada: {{ $quehacemos->imagen_vcm }}</p>
                        <img src="/img/admin/que_hacemos/{{ $quehacemos->imagen_vcm }}" style="max-width: 300px;">
                    </div>

                </div>

                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection