@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Qué hacemos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/que_hacemos">Qué hacemos</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            {{-- <a href="{{ route('que_hacemos.create') }}" class="btn btn-success">Nuevo</a> --}}
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Compromiso institucional</th>
                        <th scope="col">Imagen Compromiso</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quehacemos as $item)

                    <tr>
                        <td>{{$item->compromiso_institucional}}</td>
                        <td>
                            <img src="/img/admin/que_hacemos/{{ $item->imagen_ci }}" alt="" width="100" height="100">
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Docencia</th>
                        <th scope="col">Imagen Docencia</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quehacemos as $item)

                    <tr>
                        <td>{{$item->docencia}}</td>
                        <td>
                            <img src="/img/admin/que_hacemos/{{ $item->imagen_dc }}" alt="" width="100" height="100">
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Vinculación con el medio</th>
                        <th scope="col">Imagen Vinculación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quehacemos as $item)

                    <tr>
                        <td>{{$item->vinculacion_con_el_medio}}</td>
                        <td>
                            <img src="/img/admin/que_hacemos/{{ $item->imagen_vcm }}" alt="" width="100" height="100">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="{{ route('que_hacemos.edit', $item->id) }}" class="btn btn-success"
                                style="margin-bottom: 10px;">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>
</div>
@endsection