@extends('layouts.menu')
@section('content')
<div class="container">
   
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Proyectos</h1>
                 
                </div><!-- /.col -->
              
                <div class="col-sm-6">
               
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/proyecto">Proyectos</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Hola {{ Auth::user()->name }}</h4>
                <p>Si va a agragar un proyecto, por favor siga esta recomedación:<br>
                    1-Asegurese de que el socio comunitario y los tags  esten registrado en la sección <strong>"Socios Comunitario"y "Tags"</strong>  perteneciente al proyecto <br>
                    2-Que la imagen tenga un formato <strong>".jpg" o ".png"</strong>.<br>
                    3-Respete los caracteres de los campos solicitados.
                </p>
               
            </div>
            <hr>
            <div class="col-sm-12">
                <form class="form-inline ml-3 float-right">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Buscar"
                            aria-label="Search">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text">Buscar</button>

                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la búsqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
                </h6>
            </div>
            <br>
            <a href="{{ route('proyecto.create') }}" class="btn btn-success" style='display:inline'>Nuevo</a>
            <a href="{{ route('carrera.index') }}" class="btn btn-success" style='display:inline'>Carreras</a>
            <a href="{{ route('tag.index') }}" class="btn btn-success" style='display:inline'>Tags</a>
            <a href="{{ route('socio_comunitario.index') }}" class="btn btn-success" style='display:inline'>Socios
                comunitario</a>
            <a href="{{ route('ods.index') }}" class="btn btn-success" style='display:inline'>Ods</a>
            <a href="{{ route('periodo.index') }}" class="btn btn-success" style='display:inline'>Periodo</a>
            <table class="table table-striped">
                <thead>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Carrera</th>
                    <th>Socio Comunitarios</th>
                    <th>Ods</th>
                    <th>Tags</th>
                    <th>Alumnos</th>
                    <th>Periodo</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($proyectos as $proyecto)
                    <tr>
                        <td>{{ $proyecto->nombre }}</td>
                        <td><img src="/img/admin/proyecto/{{ $proyecto->imagen }}" alt="" width="50" height="50"></td>
                        <td>{{ $proyecto->Carrera->nombre }}</td>
                        <td>{{ $proyecto->Socio_comunitario->nombre }}</td>

                        <td>
                            @forelse ($proyecto->Ods as $ods)
                            {{ $ods->nombre; }}
                            <br>
                            @empty
                            @endforelse
                        </td>

                        <td>
                            @forelse ($proyecto->Tag as $tag)
                            {{ $tag->nombre; }}
                            <br>
                            @empty
                            @endforelse
                        </td>

                        <td>
                            @forelse ($proyecto->Alumno as $alumno)
                            {{ $alumno->nombre; }}
                            <br>
                            @empty
                            @endforelse
                        </td>
                        <td>{{ $proyecto->Periodo->año }}</td>

                        <td>
                            <a href="{{ route('proyecto.edit', $proyecto->id) }}" class="btn btn-success"
                                style="margin-bottom: 10px;">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>

                            <form method="POST" action="{{ route('proyecto.destroy', $proyecto->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
            {{ $proyectos->links() }}
        </div>
    </div>
  
</div>
@endsection