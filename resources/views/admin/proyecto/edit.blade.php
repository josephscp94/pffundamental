@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Proyecto</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/carrera">Proyecto</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$proyecto->nombre}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <form action="{{ route('proyecto.update',$proyecto) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100"
                            value="{{ $proyecto->nombre }}">
                    </div>

                    <div class=" form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="3000"
                            value="{{ $proyecto->descripcion }}">{{ $proyecto->descripcion }}</textarea>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="carrera_id">Carrera</label>
                        <select name="carrera_id" class="form-control selectpicker" data-style="btn-primary"
                            title="{{ $proyecto->carrera->año }}">
                            @foreach ($carreras as $carrera)
                            <option value="{{ $carrera->id }}" {{ $proyecto->carrera_id == $carrera->id ? 'selected' :
                                '' }}>
                                {{ $carrera->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="periodo_id">Periodo</label>
                        <select name="periodo_id" class="form-control selectpicker" data-style="btn-primary"
                            title="{{ $proyecto->Periodo->año }}">
                            @foreach ($periodos as $periodo)
                            <option value="{{ $periodo->id }}" {{ $proyecto->periodo_id == $periodo->id ? 'selected' :
                                '' }}>
                                {{ $periodo->año }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="socio_comunitario_id">Socio comunitario</label>
                        <select name="socio_comunitario_id" class="form-control selectpicker" data-style="btn-primary"
                            title="{{ $proyecto->Socio_comunitario->nombre }}">
                            @foreach ($socio_comunitarios as $socio_comunitario)
                            <option value="{{ $socio_comunitario->id }}" {{ $proyecto->socio_comunitario_id ==
                                $socio_comunitario->id ? 'selected' :
                                '' }}>{{ $socio_comunitario->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="odss">ODS</label>
                        <select name="odss[]" id="odss" class="form-control selectpicker" data-style="btn-primary"
                            title="Seleccionar ODS" multiple required>
                            @foreach ($odss as $ods)
                            <option value="{{ $ods->id }}" {{ $proyecto->ods->contains($ods->id) ? 'selected' : '' }}>
                                {{ $ods->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <select name="tags[]" id="tags" class="form-control selectpicker" data-style="btn-primary"
                            title="Seleccionar Tags" multiple required>
                            @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}" {{ $proyecto->tag->contains($tag->id) ? 'selected' : '' }}>
                                {{ $tag->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="alumnos1">Ingresar Alumno 1</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[0]->nombre) ? $alumnos[0]->nombre : '' }}">
                            </div>

                            <div class="col">
                                <label for="alumnos2">Ingresar Alumno 2</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[1]->nombre) ? $alumnos[1]->nombre : '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos3">Ingresar Alumno 3</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[2]->nombre) ? $alumnos[2]->nombre : '' }}">
                            </div>

                            <div class="col">
                                <label for="alumnos4">Ingresar Alumno 4</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[3]->nombre) ? $alumnos[3]->nombre : '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos5">Ingresar Alumno 5</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[4]->nombre) ? $alumnos[4]->nombre : '' }}">
                            </div>

                            <div class="col">
                                <label for="alumnos6">Ingresar Alumno 6</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[5]->nombre) ? $alumnos[5]->nombre : '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos7">Ingresar Alumno 7</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[6]->nombre) ? $alumnos[6]->nombre : '' }}">
                            </div>

                            <div class="col">
                                <label for="alumnos8">Ingresar Alumno 8</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[7]->nombre) ? $alumnos[7]->nombre : '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos9">Ingresar Alumno 9</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[8]->nombre) ? $alumnos[8]->nombre : '' }}">
                            </div>

                            <div class="col">
                                <label for="alumnos10">Ingresar Alumno 10</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100"
                                    value="{{ isset($alumnos[9]->nombre) ? $alumnos[9]->nombre : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $proyecto->imagen }}</p>
                        <img src="/img/admin/proyecto/{{ $proyecto->imagen }}" style="max-width: 300px;">
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection