@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Crear un nuevo proyecto</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/proyecto">Proyecto</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'proyecto'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('proyecto.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required>
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="3000" required></textarea>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="carrera_id">Carrera</label>
                        <select name="carrera_id" class="form-control selectpicker" data-style="btn-primary"
                            title="Elija una carrera" required>
                            @foreach ($carreras as $carrera)
                            <option value="{{ $carrera->id }}">{{ $carrera->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="periodo_id">Periodo</label>
                        <select name="periodo_id" class="form-control selectpicker" data-style="btn-primary"
                            title="Elija un periodo" required>
                            @foreach ($periodos as $periodo)
                            <option value="{{ $periodo->id }}">{{ $periodo->año }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="socio_comunitario_id">Socio comunitario</label>
                        <select name="socio_comunitario_id" class="form-control selectpicker" data-style="btn-primary"
                            title="Elija un socio comunitario" required>
                            @foreach ($socio_comunitarios as $socio_comunitario)
                            <option value="{{ $socio_comunitario->id }}">{{ $socio_comunitario->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="odss">Ods</label>
                        <select name="odss[]" id="odss" class="form-control selectpicker" data-style="btn-primary"
                            title="Seleccionar objetivos de desarrollo sostenible" multiple required>
                            @foreach ($odss as $ods)
                            <option value="{{ $ods->id }}">{{ $ods->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <select name="tags[]" id="tags" class="form-control selectpicker" data-style="btn-primary"
                            title="Seleccionar tags" multiple required>
                            @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="alumnos1">Ingresar Alumno 1</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>

                            <div class="col">
                                <label for="alumnos2">Ingresar Alumno 2</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos3">Ingresar Alumno 3</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>

                            <div class="col">
                                <label for="alumnos4">Ingresar Alumno 4</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos5">Ingresar Alumno 5</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>

                            <div class="col">
                                <label for="alumnos6">Ingresar Alumno 6</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos7">Ingresar Alumno 7</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>

                            <div class="col">
                                <label for="alumnos8">Ingresar Alumno 8</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="alumnos9">Ingresar Alumno 9</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>

                            <div class="col">
                                <label for="alumnos10">Ingresar Alumno 10</label>
                                <input type="text" name="alumnos[]" class="form-control" maxlength="100">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <img src="/img/admin/proyecto/proyecto.jpg" alt="">
                        <input type="file" name="imagen" required>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Crear</button>
            </form>

        </div>
    </div>
</div>
@endsection