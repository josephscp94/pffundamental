@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Testimonios</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/testimonios">Testimonios</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$testimonios->nombre}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('testimonios.update',$testimonios) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="250"
                            value="{{ $testimonios->nombre }}">
                    </div>

                    <div class=" form-group">
                        <label for="cargo">Ingresar cargo</label>
                        <textarea name="cargo" class="form-control" maxlength="250"
                            value="{{ $testimonios->cargo }}">{{ $testimonios->cargo }}</textarea>
                    </div>

                    <div class=" form-group">
                        <label for="parrafo">Ingresar parrafo</label>
                        <textarea name="parrafo" class="form-control" maxlength="700"
                            value="{{ $testimonios->parrafo }}">{{ $testimonios->parrafo }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $testimonios->imagen }}</p>
                        <img src="/img/admin/testimonios/{{ $testimonios->imagen }}" style="max-width: 300px;">
                    </div>

                    <div class="card-header">
                        <label for="estado">Ingresar estado</label>
                        <input type="checkbox" name="estado" {{ $testimonios->estado == 1 ? "checked='checked'" : ''}}>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>

@endsection