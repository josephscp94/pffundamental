@extends('layouts.menu')
@section('content')
@section ('styles')

!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

@endsection
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Testimonios</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/testimonios">Testimonios</a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'testimonios'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('testimonios.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="250" required>
                    </div>

                    <div class="form-group">
                        <label for="cargo">Ingresar cargo</label>
                        <textarea name="cargo" class="form-control" maxlength="250" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="parrafo">Ingresar parrafo</label>
                        <textarea name="parrafo" class="form-control" maxlength="700" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <img src="/img/admin/testimonios" alt="">
                        <input type="file" name="imagen" required>
                    </div>

                    <div class="form-group">
                        <label for="estado">Ingresar estado</label>
                        <input type="checkbox" name="estado" class="form-check-labell" checked>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@endsection