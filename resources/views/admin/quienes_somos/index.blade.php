@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Quiénes somos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/quienes_somos">Quiénes somos</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la búsqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
            </div>

            {{-- <a href="{{ route('quienes_somos.create') }}" class="btn btn-success">Nuevo</a> --}}
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Propósito</th>
                        <th scope="col">Video URL</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quienessomos as $item)

                    <tr>
                        <td>{{$item->proposito}}</td>
                        <td>{{$item->url}}</td>
                        {{-- <td>{{ $item->User->name }}</td> --}}
                        <td>
                            <a href="{{ route('quienes_somos.edit', $item->id) }}" class="btn btn-success"
                                style="margin-bottom: 10px;">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection