@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Quiénes somos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/quienes_somos">Quiénes somos</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{'quienes_somos'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('quienes_somos.update',$quienessomos) }}" method="POST"
                enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">

                    <div class=" form-group">
                        <label for="proposito">Ingresar proposito</label>
                        <textarea name="proposito" class="form-control" maxlength="1500"
                            value="{{ $quienessomos->proposito }}">{{ $quienessomos->proposito }}</textarea>
                    </div>

                    <div class=" form-group">
                        <label for="url">Ingresar url</label>
                        <textarea name="url" class="form-control" maxlength="255"
                            value="{{ $quienessomos->url }}">{{ $quienessomos->url }}</textarea>
                    </div>

                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection