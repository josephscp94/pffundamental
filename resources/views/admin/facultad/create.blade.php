@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Crear una nueva facultad</h1>
                </div><!-- /.col -->

            </div>

            <form action="{{ route('facultad.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="50" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
            </form>
        </div>
    </div>
</div>
@endsection