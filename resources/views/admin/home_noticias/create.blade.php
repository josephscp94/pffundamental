@extends('layouts.menu')
@section('content')
@section ('styles')

!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

@endsection
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home noticias</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/home_noticias">Noticias</a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'noticia'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('home_noticias.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="titular">Ingresar el titular de la noticia</label>
                        <input type="text" name="titular" class="form-control" maxlength="250" required>
                    </div>

                    <div class="form-group">
                        <label for="bajada">Ingresar la bajada de la noticia</label>
                        <textarea name="bajada" class="form-control" maxlength="250" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="cuerpo">Ingresa el cuerpo de la noticia</label>
                        <textarea class="form-control" name="cuerpo" id="'#tast-textarea'" maxlength="10000" required
                            @required(true) rows="5">{{old('cuerpo')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="link">Ingresar el link de la noticia (opcional)</label>
                        <input type="text" name="link" class="form-control" maxlength="1000">
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <img src="/img/admin/home_noticias/home_noticias.jpg" alt="">
                        <input type="file" name="imagen" required>
                    </div>

                    <div class="form-group">
                        <label for="estado">Ingresar estado</label>
                        <input type="checkbox" name="estado" class="form-check-labell" checked>
                    </div>

                    <button type="submit" class="btn btn-success">Crear</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@section('scripts')

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/39.0.2/classic/ckeditor.js"></script>
@endsection