@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home - noticias</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/home_noticias">noticias</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$noticias->titular}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('home_noticias.update',$noticias) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="titular">Ingresar titular</label>
                        <input type="text" name="titular" class="form-control" maxlength="250" required
                            value="{{ $noticias->titular }}">
                    </div>

                    <div class=" form-group">
                        <label for="bajada">Ingresar bajada</label>
                        <textarea name="bajada" class="form-control" maxlength="250" required
                            value="{{ $noticias->bajada }}">{{ $noticias->bajada }}</textarea>
                    </div>

                    <div class=" form-group">
                        <label for="cuerpo">Ingresar cuerpo</label>
                        <textarea name="cuerpo" class="form-control" maxlength="10000" required
                            value="{{ $noticias->cuerpo }}">{{ $noticias->cuerpo }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="link">Ingresar link</label>
                        <input type="text" name="link" class="form-control" maxlength="1000"
                            value="{{ $noticias->link }}">
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $noticias->imagen }}</p>
                        <img src="/img/admin/home_noticias/{{ $noticias->imagen }}" style="max-width: 300px;">
                    </div>

                    <div class="card-header">
                        <label for="estado">Ingresar estado</label>
                        <input type="checkbox" name="estado" {{ $noticias->estado == 1 ? "checked='checked'" : ''}}>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection