@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Crear un nuevo tags</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/tag">Tags</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'tag'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('tag.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar tag</label>
                        <input type="text" name="nombre" class="form-control" maxlength="50" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
            </form>
        </div>
    </div>
</div>
@endsection