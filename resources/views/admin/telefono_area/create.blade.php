@extends('layouts.menu')
@section('content')
@section ('styles')

!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

@endsection
<div class="container-fluid">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Teléfonos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/contacto">Contactos PFF</a></li>
                        <li class="breadcrumb-item active"><a href="#">Crear</a></li>
                    </ol>
                </div>
            </div>

            <form action="{{ route('telefono_area.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required>
                    </div>

                    <div class="form-group">
                        <label for="telefono">Ingresar teléfono</label>
                        <input type="text" name="telefono" class="form-control" maxlength="20" required>
                    </div>

                    <button type="submit" class="btn btn-success">Crear</button>
                </div>
            </form>
        </div>

    </div>

</div>

@endsection