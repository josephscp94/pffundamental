@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar teléfono</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/contacto">Contactos PFF</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{'carrusel'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('telefono_area.update',$telefono) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class=" form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <textarea name="nombre" class="form-control" maxlength="100" required
                            value="{{ $telefono->nombre }}">{{ $telefono->nombre }}</textarea>
                    </div>

                    <div class=" form-group">
                        <label for="telefono">Ingresar teléfono</label>
                        <textarea name="telefono" class="form-control" maxlength="20" required
                            value="{{ $telefono->telefono }}">{{ $telefono->telefono }}</textarea>
                    </div>

                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>

        </div>
    </div>
</div>
@endsection