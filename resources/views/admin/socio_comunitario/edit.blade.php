@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <form action="{{ route('socio_comunitario.update',$socio_comunitario) }}" method="POST"
                enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100"
                            value="{{ $socio_comunitario->nombre }}">
                    </div>

                    <div class=" form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="750"
                            value="{{ $socio_comunitario->descripcion }}">{{ $socio_comunitario->descripcion }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $socio_comunitario->imagen }}</p>
                        <img src="/img/admin/socio_comunitario/{{ $socio_comunitario->imagen }}"
                            style="max-width: 300px;">
                    </div>

                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection