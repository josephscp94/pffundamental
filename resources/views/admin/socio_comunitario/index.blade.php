@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Listado de Socios comunitarios</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/socio_comunitario"> Socios comunitarios</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <hr>
            <div class="col-sm-12">
                <form class="form-inline ml-3 float-right">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Buscar"
                            aria-label="Search">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text">Buscar</button>

                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la búsqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
                </h6>
            </div>

            <a href="{{ route('socio_comunitario.create') }}" class="btn btn-success" style='display:inline'>Nuevo</a>
            <table class="table table-striped">
                <thead>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Descripción</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($socios_comunitarios as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td><img src="/img/admin/socio_comunitario/{{ $item->imagen }}" alt="" width="50" height="50">
                        </td>
                        <td>{{ $item->descripcion }}</td>
                        <td>
                            <a href="{{ route('socio_comunitario.edit', $item->id) }}" class="btn btn-success"
                                style="margin-bottom: 10px;">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>

                            <form method="POST" action="{{ route('socio_comunitario.destroy', $item->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
            {{$socios_comunitarios->links() }}
        </div>
    </div>
</div>
@endsection