@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Crear un nuevo Socio comunitario</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/socio_comunitario"> Socios comunitarios</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{' Socios comunitarios'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('socio_comunitario.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required>
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="750" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <img src="/img/admin/socio_comunitario" alt="">
                        <input type="file" name="imagen" required>
                    </div>

                </div>
                <button type="submit" class="btn btn-success">Crear</button>
            </form>
        </div>
    </div>
</div>
@endsection