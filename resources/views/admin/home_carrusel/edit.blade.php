@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home carrusel</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/home_carrusel">Home carrusel</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{'carrusel'}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('home_carrusel.update',$carrusel) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class=" form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="100" required
                            value="{{ $carrusel->descripcion }}">{{ $carrusel->descripcion }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="link">Ingresar link (opcional)</label>
                        <input type="text" name="link" class="form-control" maxlength="1000"
                            value="{{ $carrusel->link }}">
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $carrusel->imagen }}</p>
                        <img src="/img/admin/home_carrusel/{{ $carrusel->imagen }}" style="max-width: 300px;">
                    </div>

                    <div class="card-header">
                        <label for="estado">Ingresar estado</label>
                        <input type="checkbox" name="estado" {{ $carrusel->estado == 1 ? "checked='checked'" : ''}}>
                    </div>

                </div>
                <button type="submit" class="btn btn-success">Editar</button>
            </form>

        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    CKEDITOR.replace( 'descripcion' );
</script>