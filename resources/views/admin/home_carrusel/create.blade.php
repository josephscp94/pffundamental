@extends('layouts.menu')
@section('content')
@section ('styles')

!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

@endsection
<div class="container-fluid">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home carrusel</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/home_carrusel">Home carrusel</a></li>
                        <li class="breadcrumb-item"><a href="#">Crear</a></li>
                        <li class="breadcrumb-item active">{{'carrusel'}}</li>
                    </ol>
                </div>
            </div>

            <form action="{{ route('home_carrusel.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="descripcion">Ingresar descripción</label>
                        <textarea name="descripcion" class="form-control" maxlength="100" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="link">Ingresar el link de la noticia (opcional)</label>
                        <input type="text" name="link" class="form-control" maxlength="1000">
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <img src="/img/admin/home_carrusel/home_carrusel.jpg" alt="">
                        <input type="file" name="imagen" required>
                    </div>

                    <div class="form-group">
                        <label for="estado">Ingresar estado</label>
                        <input type="checkbox" name="estado" class="form-check-labell" checked>
                    </div>

                    <button type="submit" class="btn btn-success">Crear</button>
                </div>
            </form>
        </div>

    </div>

</div>
</div>

</div>
@endsection

@section('scripts')
<script>
    CKEDITOR.replace( 'descripcion' );
</script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

@endsection