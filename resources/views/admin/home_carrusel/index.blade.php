@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home carrusel</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/home_carrusel">Home carrusel</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <hr>
            <div class="col-sm-12">
                <form class="form-inline ml-3 float-right">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Buscar"
                            aria-label="Search">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text">Buscar</button>

                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la busqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
                </h6>
            </div>

            <a href="{{ route('home_carrusel.create') }}" class="btn btn-success">Nuevo</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Descripción</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($carrusel as $item)

                    <tr>
                        <td>{{$item->descripcion}}</td>
                        <td>
                            <img src="/img/admin/home_carrusel/{{ $item->imagen }}" alt="" width="100" height="100">
                        </td>
                        <td>
                            <a class="btn btn-sm btn-{{$item->estado ? 'success  ': 'danger'}}">
                                {{$item->estado ? 'Activado':'Desactivado' }}
                            </a>
                        </td>

                        <td>
                            <a href="{{ route('home_carrusel.edit', $item->id) }}" class="btn btn-success">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>

                            <form method="POST" action="{{ route('home_carrusel.destroy', $item->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>

                    </tr>


                    @endforeach
                </tbody>
            </table>
            {{ $carrusel->links() }}
        </div>
    </div>
</div>
@endsection