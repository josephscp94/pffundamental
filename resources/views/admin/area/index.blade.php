@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Listado de area academicas </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin/area">Areas</a></li>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div><!-- /.col -->
                </div>
                {{-- <a href="{{ route('area.create') }}" class="btn btn-success">Nuevo</a> --}}
                <table class="table table-striped">
                    <thead>
                        <th>Nombre</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                        @forelse ($areas as $item)
                        <tr>
                            <td>{{ $item->nombre }}</td>
                            <td>
                                <a href="{{ route('area.edit', $item->id) }}" class="btn btn-success">
                                    <img src=" /svg/edit.svg" alt="home-image" width="20">
                                </a>
                                {{-- <form method="POST" action="{{ route('area.destroy', $item->id) }}"
                                    style="display:inline">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger"
                                        onclick="return confirm('¿Desea eliminar este registro?')">
                                        <img src="/svg/delete.svg" alt="home-image" width="20">
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                        @empty
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection