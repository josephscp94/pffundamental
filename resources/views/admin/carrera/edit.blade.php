@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Carreras</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/carrera">Carreras</a></a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$carrera->nombre}}</li>
                    </ol>
                </div><!-- /.col -->
            </div>

            <form action="{{ route('carrera.update',$carrera) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar nombre</label>
                        <input type="text" name="nombre" class="form-control" maxlength="100" required
                            value="{{ $carrera->nombre }}">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="facultad_id">Carrera</label>
                        <select name="facultad_id" class="form-control selectpicker" data-style="btn-primary"
                            title="{{ $carrera->facultad->nombre }}" required>
                            @foreach ($facultades as $facultad)
                            <option value="{{ $facultad->id }}" {{ $carrera->facultad_id == $facultad->id ? 'selected' :
                                '' }}>
                                {{ $facultad->nombre }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="imagen">Ingresar imagen</label>
                        <br>
                        <input type="file" name="imagen">
                        <p>Nombre de la imagen cargada: {{ $carrera->imagen }}</p>
                        <img src="/img/admin/carrera/{{ $carrera->imagen }}" style="max-width: 300px;">
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
@endsection