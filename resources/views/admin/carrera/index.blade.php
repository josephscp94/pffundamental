@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Carreras</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/carrera">Carreras</a></a></li>

                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <hr>
            <div class="col-sm-12">
                <form class="form-inline ml-3 float-right">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Buscar"
                            aria-label="Search">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text">Buscar</button>

                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                @if($search)
                <div class="alert alert-success" role="alert">
                    El resultado de la búsqueda de <strong>'{{$search}}'</strong> son:.
                </div>
                @endif
                </h6>
            </div>
            <br>
            <a href="{{ route('carrera.create') }}" class="btn btn-success" style='display:inline'>Nuevo</a>
            <a href="{{ route('facultad.index') }}" class="btn btn-success">Facultades</a>
            <table class="table table-striped">
                <thead>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($carreras as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td><img src="/img/admin/carrera/{{ $item->imagen }}" alt="" width="50" height="50"></td>

                        <td>
                            <a href="{{ route('carrera.edit', $item->id) }}" class="btn btn-success">
                                <img src="/svg/edit.svg" alt="home-image" width="20">
                            </a>

                            <form method="POST" action="{{ route('carrera.destroy', $item->id) }}"
                                style="display:inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <img src="/svg/delete.svg" alt="home-image" width="20">
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
            {{ $carreras->links() }}
        </div>
    </div>
</div>
</div>
@endsection