@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edita un Objetivos de Desarrollo Sostenible</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/ods">Ods</a></li>
                        <li class="breadcrumb-item"><a href="#">Editar</a></li>
                        <li class="breadcrumb-item active">{{$ods->nombre}}</li>
                    </ol>
                </div>
            </div>

            <form action="{{ route('ods.update',$ods) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="jumbotron">
                    <div class="form-group">
                        <label for="nombre">Ingresar Objetivos de Desarrollo Sostenible</label>
                        <input type="text" name="nombre" class="form-control" maxlength="50" value="{{ $ods->nombre }}">
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Editar</button>
            </form>
        </div>
    </div>
</div>
@endsection