<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Socios comunitarios</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <link href="{{ asset('css/socio_comunitarios.css') }}" rel="stylesheet">

</head>
{{-- <section>
  @yield('scrollup')
</section> --}}
<header>
  @include('layouts.head')
</header>

<body>
  <main>

    <div class="container-fluid page-header mb-5 p-0" style=" background-image: url('/img/heading.png')  ">
      <div class="container-fluid page-header-inner py-5" >
        <div class="container text-center">
          <h1 class="display-3 text-white mb-3 animated slideInDown">Socios comunitarios</h1>

        </div>
      </div>
    </div>
  </main>

  <article id="socios">
    <div class="container-xxl service py-5">
      <div class="container" id="socios">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb justify-content-left ">
            <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
            <li class="breadcrumb-item text-black active" aria-current="page"><a
                href="{{route('competencias-genericas')}}"><strong>Competencias genéricas</strong></a>
            </li>
            <li class="breadcrumb-item text-black active" aria-current="page"><strong>Socios comunitarios</strong>
            </li>

          </ol>
        </nav>
        <h2>Conoce nuestros socios comunitarios</h2>
        <div class="row">

          @forelse ($socios_comunitarios as $socio)
          <div class="col-md-4 col-xs-6">
            <div class="cards">
              <div class="cards-img">
                <div class="position-relative inner-box">
                  <div class="image position-relative ">
                    <img src="/img/admin/socio_comunitario/{{ $socio->imagen }}" alt="" alt="portfolio-image"
                      class="img-fluid w-100 d-block">
                    <div class="overlay-box">
                      <div class="overlay-inner">
                        <div class="overlay-content">
                          <h3>{{ $socio->nombre }}</h3>
                          {{-- <p>Design</p> --}}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="cards-body">

                    <?php $parrafos = explode("\n", $socio->descripcion) ?>
                    @foreach($parrafos as $parrafo)
                    @if(trim($parrafo) != "")
                    <h6 id="parrafo">{{$parrafo}}</h6>
                    @endif
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
          @empty
          @endforelse
        </div>
        <div style="display: flex; justify-content: center;">
          <style>
            .pagination a {
              color: #dc6b6b;
            }

            .page-item.active .page-link {
              background-color: #dc6b6b;
              border-color: #dc6b6b;
            }
          </style>
          {{ $socios_comunitarios->links() }}
        </div>
        <br>
      </div>
      <div class="clearfix visible-sm visible-xs"></div>
    </div>
  </article>

  {{-- socios-comunitarios --}}
  <article id="solicitudes">
    <div class="container-xxl service py-5" id="formulario">
      <div class="container" id="form">
        <div class="col-md-12" id="solicitud-tarjeta">
          <h1>¿Quieres ser uno de nuestros Socios Comunitarios?, completa el siguiente formulario... ¡te
            contactaremos!
          </h1>
        </div>
        <form action="{{ route('socios-comunitarios') }}" method="Post">
          @csrf
          <div class="form-group row">
            <label for="nombre_institucion" class="col-sm-2 col-form-label">Nombre de la institución</label>
            <div class="col-sm-10">
              <input type="text" name="nombre_institucion" class="form-control"
                placeholder="Ingresa el nombre de la institución " maxlength="100" required>
            </div>
          </div>
          <br>
          <div class="form-group row">
            <label for="nombre_persona" class="col-sm-2 col-form-label">Nombre del solicitante</label>
            <div class="col-sm-10">
              <input type="text" name="nombre_persona" class="form-control"
                placeholder="Ingresa el nombre del solicitatante" maxlength="100" required>
            </div>
          </div>
          <br>
          <div class="form-group row">
            <label for="correo" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
              <input type="text" name="correo" class="form-control" placeholder="Ingresa un correo para contactarte"
                maxlength="100" required>
            </div>
          </div>
          <br>
          <div class="form-group row">
            <label for="telefono" class="col-sm-2 col-form-label">Teléfono</label>
            <div class="col-sm-10">
              <input type="text" name="telefono" class="form-control" placeholder="Ingresa un número de teléfono"
                maxlength="20" required>
            </div>
          </div>
          <br>
          <div class="form-group row">
            <label for="descripcion" class="col-sm-2 col-form-label">¿Cuál es tú necesidad?</label>
            <div class="col-sm-10">
              <textarea name="descripcion" class="form-control" placeholder="Ingresa tu respuesta" id="message"
                style="height: 100px" maxlength="500" required></textarea>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </article>
</body>


<footer>
  @include('layouts.footer')
</footer>



</html>