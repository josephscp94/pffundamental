<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Programa de Formación Fundamental | Noticias</title>
    <link rel="icon" href="/svg/Logo_blanco-2.svg">
  
   
    <link href="{{ asset('css/noticias.css') }}" rel="stylesheet">

</head>
@include('layouts.head')
<div class="about-area2 gray-bg pt-60 pb-60">
    <div class="container">
        <div class="row gx-lg-5">
            <div class="col-lg-7 col-xl-8">
                <div class="blog-post">
                    <figure>
                        <img id="noticia" width="1280px" height="1024px"
                            src="/img/admin/home_noticias/{{ $home_noticias->imagen }}" class="img_home_carrusel">

                    </figure>

                    <h1 class="titulos"> {{ $home_noticias->titular }} </h1>

                    <div class="d-lg-flex justify-content-between share-div">
                        <ul class="list-unstyled d-flex">
                            <li> <i class="far fa-user"></i><span> Por: {{ $home_noticias->User->name }} </span> </li>
                            <li> <i class="far fa-calendar-alt"></i> {{ $home_noticias->created_at->format('d-m-Y') }}
                            </li>
                           

                        </ul>
                       
                     
                        
                    </div>

                    <div class="mt-4 flex flex-col gap-4">
                        <?php $parrafos = explode("\n", $home_noticias->bajada) ?>
                        @foreach($parrafos as $parrafo)
                        @if(trim($parrafo) != "")
                        <h5 class="parrafo">{{$parrafo}}</h5>
                        @endif
                        @endforeach
                    </div>
                    <div class="mt-4 flex flex-col gap-4">
                        <?php $parrafos = explode("\n", $home_noticias->cuerpo) ?>
                        @foreach($parrafos as $parrafo)
                        @if(trim($parrafo) != "")
                        <h5 class="parrafo">{{$parrafo}}</h5>
                        @endif
                        @endforeach

                    </div>
                    @if(!empty($home_noticias->link))
                    <a href="{{ $home_noticias->link }}" class="btn btn-primary " id="btn-blog">Mas información</a>
                    @endif


                </div>
            </div>
            <div class="col-lg-5 col-xl-4">

                <div class="recent-post-div mt-5">
                    <div class="col-md-12">
                        <h1>Noticias- En pauta</h1>
                    </div>
                    <div class="recent-post-div-insiide">
                        @forelse ($otras_noticias as $noticia)
                        <a href={{route('noticias',$noticia->slug)}} class="d-flex w-100 justify-content-between
                            align-items-center">
                            <figure class="img-wrap">
                                <img src="/img/admin/home_noticias/{{ $noticia->imagen }}" class="img_home_carrusel">
                            </figure>
                            <h5> {{ $noticia->titular }} </h5>
                        </a>
                        @empty
                        @endforelse
                    </div>
                </div>
              
                    <!-- Resto del código de la noticia -->
                
                   
                
                <div class="recent-post-div mt-5">
                    <div class="card-header" id="carreras">

                        <a href="{{ route('blog-noticias') }}" class="myButton" id="btn-blog">Ver todas las noticias</a>

                    </div>

                </div>
            </div>



        </div>

    </div>
</div>
</div>

</div>
@include('layouts.footer')
</div>