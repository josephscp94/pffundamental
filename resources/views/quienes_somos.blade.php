<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Quiénes Somos</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <link href="{{ asset('css/quienes_somos.css') }}" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons.css">

</head>

<header>
  @include('layouts.head')
</header>

<body>
  <main>

    <div class="container-fluid page-header mb-5 p-0" id="mb-5"
      style=" background-image: url('/img/heading.png') ">
      <div class="container-fluid page-header-inner py-5" >
        <div class="container text-center">
          <h1 class="display-3 text-white mb-3 animated slideInDown">Quiénes somos</h1>
         
        </div>
      </div>
    </div>

  </main>

  <body>
    <article>
      <!-- Video Start -->
      <div class="container-xxl service py-5">
        <div class="container" id="container-quienessomos ">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-left ">
              <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
              <li class="breadcrumb-item text-black active" aria-current="page"><strong>Quiénes somos</strong></li>
            </ol>
          </nav>
          <div class="row g-0">
            <div class="col-lg-6 py-5 wow fadeIn" id="py-5qs" data-wow-delay="0.3s">
              <div class="py-5" id="py-5qs">
                <h1 class="titulos">Propósito del Programa de Formación
                  Fundamental
                </h1>
                <div class="mt-4 flex flex-col gap-4">
                  <h5 class="fw-normal lh-base fst-italic text-white mb-5">
                    <?php $parrafos = explode("\n", $quienes_somos[0]->proposito) ?>
                  </h5>
                  @foreach($parrafos as $parrafo)
                  @if(trim($parrafo) != "")
                  <h5 class="parrafo">{{$parrafo}}</h5>
                  @endif
                  @endforeach
                </div>
                {{-- <h5 class="fw-normal lh-base fst-italic text-white mb-5">{{ $quienes_somos[0]->proposito }}</h5>
                --}}
              </div>
            </div>
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
              <div class="h-100 d-flex align-items-center justify-content-center" style="min-height: 200px;">
                <button type="button" class="btn-play" data-bs-toggle="modal" data-src="{{ $quienes_somos[0]->url }}"
                  data-bs-target="#videoModal">
                  <span></span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>



      <!-- Video End -->
      <!-- The Modal -->
      <div id="myModal" class="modal">

        <!-- The Close Button -->
        <span class="close">&times;</span>

        <!-- Modal Content (The Image) -->
        <img class="modal-content" id="img01">

        <!-- Modal Caption (Image Text) -->
        <div id="caption"></div>
      </div>

      <!-- Video Modal Start -->
      <div class="modal modal-video fade" id="videoModal" tabindex="-1" aria-labelledby="exampleModalLabel" ;
        aria-hidden=" true">
        <div class="modal-dialog">
          <div class="modal-content rounded-0">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLabel"> Video del programa de Formación Fundamental</h3>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <!-- 16:9 aspect ratio -->
              <div class="ratio ratio-16x9">
                <iframe class="embed-responsive-item" src="" id="video" allowfullscreen allowscriptaccess="always"
                  allow="autoplay"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
    <!-- Video Modal fin -->
    <article>
      <div class="container-xxl service py-5" id="py-5qs">
        <div class="container" id="container-quienessomos">
          <div class="col-md-12 text-center" id="titulos">
            <br>
            <h1 class="titulos">Conoce nuestro equipo </h1>

            <br>
          </div>
          <div class="row g-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="col-lg-4">
              <div class="nav w-100 nav-pills me-4">
                <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4 active"
                  data-bs-toggle="pill" data-bs-target="#tab-pane-1" type="button">
                  <img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
                  <h4 class="m-0">{{ $areas[0]->nombre }}</h4>
                </button>
                <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill"
                  data-bs-target="#tab-pane-2" type="button">
                  <img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">

                  <h4 class="m-0">{{ $areas[1]->nombre }}</h4>
                </button>
                <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill"
                  data-bs-target="#tab-pane-3" type="button">
                  <img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
                  <h4 class="m-0">{{ $areas[2]->nombre }}
                  </h4>
                </button>

              </div>
            </div>

            <div class="col-lg-8">
              <div class="tab-content w-100">

                <div class="tab-pane fade show active" id="tab-pane-1">
                  <div class="container" id="colaboradores-container1">
                    @include('quienes_somos1')
                  </div>
                </div>

                <div class="tab-pane fade" id="tab-pane-2">
                  <div class="row g-4">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                      <div class="container" id="colaboradores-container2">
                        @include('quienes_somos2')
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade" id="tab-pane-3">
                  <div class="row g-4">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                      <div class="container" id="colaboradores-container3">
                        @include('quienes_somos3')
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>

    </article>



    {{-- <article>
      <section>

      </section>
    </article> --}}
  </body>
  @include('layouts.footer')
  <!-- JavaScript Libraries -->
  <script type="text/javascript" src="{{ asset('./js/competencias.js') }}"></script>
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script> --}}
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

  <!-- Template Javascript -->
  <script src="js/quienes_somos.js"></script>


</html>