<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Proyectos</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <link href="{{ asset('css/proyectos.css') }}" rel="stylesheet">

</head>
{{-- <section>
  @yield('scrollup')
</section> --}}


<header>
  @include('layouts.head')
</header>

<body>

  <main>

    <div class="container-fluid page-header mb-5 p-0"
      style=" background-image: url(/img/admin/carrera/{{ $carrera->imagen }})">
      <div class="container-fluid page-header-inner py-5" >
        <div class=" container text-center">
          <h1 class="display-3 text-white mb-3 animated slideInDown">Proyectos</h1>
         
        </div>
      </div>
    </div>
  </main>
  <!-- Main -->


  <article>
    <div class="container" id="container-proyectos">

      <br>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb justify-content-left ">
          <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
          <li class="breadcrumb-item"><a href="{{route('competencias-genericas')}}"><strong>Competencias
                genéricas</strong></a></li>
          <li class="breadcrumb-item"><a href="{{route('carreras')}}"><strong>Proyectos</strong></a></li>
          <li class="breadcrumb-item text-black active" aria-current="page"><strong>{{ $carrera->nombre }}</strong>
          </li>
        </ol>
      </nav>
      <div class="row">
        <div class="col-md-12 ">
          <form class="card card-sm">
            <div class="card-body row no-gutters align-items-center">
              <div class="col-auto">
                <i class="fas fa-search h4 text-body"></i>
              </div>
              <!--end of col-->

              <div class="col">
                <input type="text" name="buscador" id="buscador" placeholder="Buscar por nombre del proyecto"
                  class="w-full lg:w-auto xl:w-full p-1 font-ui text-xl bg-blanco border-[3px] border-gris ">
              </div>
              <!--end of col-->
              <div class="col-auto">
                <button class="btn btn-lg btn-buscador" type="submit">Buscar</button>
              </div>
              <!--end of col-->
            </div>
          </form>
        </div>

        <div class="col-md-4" id="filtros">
          <br>
          <h3>Filtros</h3>
          <menu>
            <!-- SEARCH BAR -->
            <form action="{{ route('proyectos',['id' => $carrera->id]) }}" method="GET">

              <div class="form-group mb-3 col-xs-3">
                <label for="periodo_id"> Periodo</label>
                <select name="periodo_id" class="form-control selectpicker" data-style="btn-primary"
                  placeholder="Todos">
                  <option value="">Todos</option>
                  @forelse($periodos as $periodo)
                  <option value="{{ $periodo->id }}">{{ $periodo->año }}</option>
                  @empty
                  @endforelse
                </select>
              </div>

              <div class="form-group mb-3 col-xs-3">
                <label for="socio_comunitario_id"> Socio comunitario</label>
                <select name="socio_comunitario_id" class="form-control selectpicker" data-style="btn-primary"
                  placeholder="Todos">
                  <option value="">Todos</option>
                  @forelse($socios as $socio)
                  <option value="{{ $socio->id }}">{{ $socio->nombre }}</option>
                  @empty
                  @endforelse
                </select>
              </div>

              <div class="form-group mb-3 col-xs-3">
                <label for="ods_id"> Objetivo de Desarrollo Sostenible</label>
                <select name="ods_id" class="form-control selectpicker" data-style="btn-primary" placeholder="Todos">
                  <option value="">Todos</option>
                  @forelse($odss as $ods)
                  <option value="{{ $ods->id }}">{{ $ods->nombre }}</option>
                  @empty
                  @endforelse
                </select>
              </div>

              <button type="submit" class="btn btn-filtros">Filtrar</button>
            </form>
        </div>
        </menu>
        <div class="col-md-8">
          <div class="row" id=row-col>
            @forelse ($proyectos as $proyecto)
            <!-- proyectos -->
            <div class="col-md-4 col-xs-6">
              <div class="proyecto">
                <div class="proyecto-img">
                  <div class="position-relative inner-box">
                    <div class="image position-relative ">
                      <img src="/img/admin/proyecto/{{ $proyecto->imagen }}" alt="portfolio-image"
                        class="img-fluid w-100 d-block">
                      <div class="overlay-box">

                        <div class="overlay-inner">
                          <div class="overlay-content">
                            <h3 class="mb-0">Socio comunitario</h3>
                            <p>{{ $proyecto->Socio_comunitario->nombre }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="proyecto-body">
                  {{-- <p class="proyecto-category">Category</p> --}}
                  <h3 class="proyecto-name"><a href="{{route('proyecto-detalle',$proyecto->slug)}}">{{
                      $proyecto->nombre
                      }}
                    </a></h3>
                </div>
              </div>
            </div>
            @empty
            <h3>No hay proyectos para mostrar</h3>
            @endforelse

            <!-- /proyectos -->
          </div>
          <div style="display: flex; justify-content: center;">
            <style>
              .pagination a {
                color: #dc6b6b;
              }

              .page-item.active .page-link {
                background-color: #dc6b6b;
                border-color: #dc6b6b;
              }
            </style>
            {{ $proyectos->links() }}
          </div>
          <br>
        </div>
      </div>
    </div>

  </article>
</body>


<script type="text/javascript" src="{{ asset('./js/proyectos.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
  integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
  integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
--}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
  integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>

{{-- <script src="lib/magnific-popup/jquery.magnific-popup.min.js"></script> --}}
<footer>
  @include('layouts.footer')
</footer>

</html>