<div class="row">
  @forelse ($boton2 as $colaborador2)
  <div id="foto-colaborador" class="col-md-4 col-xs-6">
    <div class="cards">
      <div class="cards-img">
        <div class="position-relative inner-box">
          <div class="image position-relative ">
            <img src="/img/admin/colaborador/{{ $colaborador2->imagen }}" alt="">
            <div class="overlay-box">
              <div class="overlay-inner">
                <div class="overlay-content">
                  <h3>{{ $colaborador2->nombre }}</h3>
                  @forelse ($colaborador2->Rol as $rol)
                  <p> {{ $rol->nombre; }}</p>
                  @empty
                  @endforelse
                </div>
              </div>
            </div>
          </div>
          <div class="cards-body">
         

            <li><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<a href="mailto: {{$colaborador2->correo}}">{{
                $colaborador2->correo }}</a></li>
          </div>
        </div>
      </div>
    </div>
  </div>
  @empty
  @endforelse
</div>
<div style="display: flex; justify-content: center;">
  <style>
    .pagination a {
      color: #dc6b6b;
    }

    .page-item.active .page-link {
      background-color: #dc6b6b;
      border-color: #dc6b6b;
    }
  </style>
  {{ $boton2->links() }}
</div>