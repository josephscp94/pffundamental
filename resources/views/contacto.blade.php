<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Programa de Formación Fundamental | Contacto</title>
	<link rel="icon" href="/svg/Logo_blanco-2.svg">
	<link href="{{ asset('css/contacto.css') }}" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
</head>

<body>
	<section>
		@yield('scrollup')
	</section>
	<header>
		@include('layouts.head')

	</header>



	<main>
		<div class="container-fluid page-header mb-5 p-0"
			style=" background-image: url('/img/heading.png')  ">
			<div class="container-fluid page-header-inner py-5" >
				<div class="container text-center">
					<h1 class="display-3 text-white mb-3 animated slideInDown">Contacto</h1>

				</div>
			</div>
		</div>
	</main>

	<body>
		<article>

		</article>
		<div class="container-xxl py-5">
			<div class="container">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb justify-content-left ">
						<li class="breadcrumb-item"><a href="{{ route('/') }}"><strong>Inicio</strong></a></li>
						<li class="breadcrumb-item text-black active" aria-current="page"><strong>Contacto</strong>
						</li>
					</ol>
				</nav>
				<div class="row g-4">
					<div class="col-12">
						<h1 class="titulo">Programa de Formación Fundamental</h1>
						<br>

					</div>
					<div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
						<iframe class="position-relative rounded w-100 h-100"
							src="https://www.google.com/maps/embed?pb=!4v1697846730609!6m8!1m7!1sCAoSLEFGMVFpcE5ZSTZZZjBodll1dDVtQlNETURBUldmZlU2Y0JuTEZsRzBaRFNv!2m2!1d-35.4064138!2d-71.637357!3f194.24500944999545!4f20.026981007543156!5f0.7820865974627469"
							width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
							referrerpolicy="no-referrer-when-downgrade"></iframe>
					</div>

					<div class="col-md-6">
						<div class="text-center wow fadeInUp">
							<br>
							<h6 class="descripcion">{{$footer[0]->descripcion}}</h6>

							<div class="contactos">
								<br>
								<h2 class="titulo"> Nuestros contactos</h2>
								<br>
								@forelse ($telefonos as $telefono)
								<div class="contacto-icono">
									<img class="iconos" src="/svg/arrow_color.svg" alt="home-image" width="30">
									<p class="info-contacto">{{$telefono->nombre}}: {{ $telefono->telefono }}</p>
								</div>
								@empty

								@endforelse
							</div>



						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Service End -->

		<footer>
			@include('layouts.footer')
		</footer>

		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
		<script src="js/jquery.scrollUp.min.js"></script>

	</body>

</html>