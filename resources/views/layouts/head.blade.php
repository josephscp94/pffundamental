<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <link rel="icon" href="/svg/Logo_blanco-2.svg">

  <link rel="stylesheet" href="css/themify-icons.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <link href="{{ asset('css/head.css') }}" rel="stylesheet">
</head>

<div id="preloader-active">
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="preloader-inner position-relative">
      {{-- <div class="preloader-circle"></div> --}}
      <div class="preloader-img pere-text">
        <img src="/svg/utalca.svg" alt="">
      </div>
    </div>
  </div>
</div>

<!-- header -->
<div class="header-main">
  <div class="container">
    <nav id='cssmenu'>
      <div class="logo">
        <a href="{{route('/')}}"><img src="/img/logopff3.png"></a>
      </div>

      <div id="head-mobile"></div>
      <div class="button"></div>
      <ul>
        <li id="inicio"><a href="{{route('inicio')}}"><strong>Inicio</strong></a></li>
        <li id="quienes-somos"><a href="{{route('quienes-somos')}}"><strong>Quiénes somos</strong></a></li>
        <li id="que-hacemos"><a href="{{route('que-hacemos')}}"><strong>Qué hacemos</strong></a></li>
        <li id="competencias-genericas"><a href="{{route('competencias-genericas')}}"><strong>Competencias
              genéricas</strong></a>
        </li>
        <li id="contacto"><a href="{{route('contacto')}}"><strong>Contacto</strong></a>
        </li>
      </ul>

    </nav>
  </div>

  {{-- <a href="javascript:void(0)" onclick="scrollToBottom()"></a> --}}


  {{-- a href="javascript:void(0)" onclick="scrollToBottom()" --}}


</div>

<script src='https://code.jquery.com/jquery-2.1.0.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>
{{-- <script src="./script.js"></script> --}}
<script type="text/javascript" src="{{ asset('./js/head.js') }}"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="js/vendor/jquery-1.12.4.min.js"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="js/slick.min.js"></script>

<!-- Scrollup-->
{{-- <script src="js/jquery.scrollUp.min.js"></script> --}}

<!-- Jquery Plugins, main Jquery -->
<script src="js/plugins.js"></script>
{{-- <script src="js/blog_noticias.js"></script> --}}
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BE83VHFZD8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-BE83VHFZD8');
</script>