<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  {{--
  <link rel="icon" href="/img/logo-pff.png"> --}}
  <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
</head>

<body>
  <footer class="footer">
    <div class="container" id="footer-col">
      <div class="footer-left col-md-6 ">


        {{-- <p class="menu">
          <span class="footer-span"> Nosotros</span><br>
          <a href="{{ route('quienes-somos') }}"> Quiénes somos</a> |
          <a href="{{ route('que-hacemos') }}"> Qué hacemos</a> |
          <a href="{{ route('carreras') }}"> Proyectos de responsabilidad social</a> |

        </p> --}}

        {{-- <a href="{{$footer[0]->url_universidad}}"><img src="/img/logo-u.png" class="logo-footer-1"></a> --}}
        <a href="{{ $footer[0]->url_vicerrectoria }}"><img src="/img/logo-vicerrectoria-.png" class="logo-footer"></a>
      </div>
      <div class="footer-center col-md-6 ">
        <div class="footer-logos">

          <span class="footer-span"> Nuestros contactos</span>
          <br>
          <div class="iconos">
            <img src="/svg/instagram.png" alt="home-image" width="30">
            <a href="{{$footer[0]->instagram}}">
              <p>Programa de Formación Fundamental</p>
            </a>

            <br>

            <img src="/svg/phone.png" alt="home-image" width="35">
            <p> {{$footer[0]->telefono}}</p>

            <br>

            <img src="/svg/email.svg" alt="home-image" width="30">
            <p><a href="mailto:{{$footer[0]->correo}}">{{$footer[0]->correo}}</a></p>

          </div>

        </div>
      </div>


      {{-- <div class="footer-right col-md-6 col-sm-6">

        <p class="about">
          <span class="footer-span"> Sobre nosotros</span> <br>{{$footer[0]->texto_contacto}}
        </p>


      </div> --}}


    </div>



  </footer>
  <footer>
    <div class="footer-hr col-md-12 ">
      <br>
      <br>

      <p class="name"> &copy; Copyright 2023 | Universidad de Talca</p>
    </div>
  </footer>


</body>

</html>