<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PFF | Panel administrativo</title>
    <link rel="icon" href="/svg/Logo_blanco-2.svg">
    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="{{ asset('/css/js/adminlte.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
    {{--
    <link rel="stylesheet" href="/css/styles_app.css"> --}}
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- Styles -->
    <link href="{{ asset('/css/adminlte.min.css') }}" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div id="app">
        <div class="wrapper">

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">

                        <a class="nav-link" data-widget="pushmenu" href="#"> <img src="/svg/menu.svg" alt="menu"></a>
                    </li>
                </ul>

            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar bg-light elevation-4">
                <!-- Brand Logo -->
                <a href="{{ url('/') }}" class="brand-link">
                    <img src="/img/logopff2.png" alt="PPF logo" class="brand-image img-elipce">
                    <span class="brand-text font-weight-light">PFF</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Panel de usuario lateral (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        {{-- <div class="image">
                            <img src="/svg/user.svg" class="img-circle elevation-2" alt="User Image">
                        </div> --}}
                        <div class="info">
                            <a href="{{ route('user.edit',Auth::user()->id) }}" class="d-block">
                                @guest
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a>
                                @else
                                {{ Auth::user()->name }}
                                <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    <button type="button" class="btn btn-outline-danger">Cerrar Sesión</button>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                                @endguest
                            </a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">

                            <li class="nav-item">
                                <a href="{{url('/admin/resumen')}}"
                                    class="{{ Request::path() === '/admin/resumen' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/resumen.svg" alt="home-image" width="20">
                                    <p>Inicio</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/proyecto')}}"
                                    class="{{ Request::path() === '/admin/proyecto' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/project.svg" alt="home-image" width="20">
                                    <p>Proyectos</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/quienes_somos')}}"
                                    class="{{ Request::path() === '/admin/quienes_somos' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/quienes_somos.svg" alt="home-image" width="20">
                                    <p>Quiénes somos</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/que_hacemos')}}"
                                    class="{{ Request::path() === '/admin/que_hacemos' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/que_hacemos.svg" width="20" alt="home-image">
                                    <p>Qué hacemos</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/competencia')}}"
                                    class="{{ Request::path() === '/admin/competencia' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/competencia.svg" alt="home-image" width="20">
                                    <p>Competencias</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/colaborador')}}"
                                    class="{{ Request::path() === '/admin/colaborador' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/colaborador.svg" width="20" alt="home-image">
                                    <p>Colaborador</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/socio_comunitario')}}"
                                    class="{{ Request::path() === '/admin/socio_comunitario' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/socios_comunitarios.svg" alt="home-image" width="20">
                                    <p>Socios comunitario</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{url('/admin/home_carrusel')}}"
                                    class="{{ Request::path() === '/admin/home_carrusel' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/home_carrusel.svg" width="20" alt="home-image">
                                    <p>Home carrusel</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/home_noticias')}}"
                                    class="{{ Request::path() === '/admin/home_noticias' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/home_noticias.svg" width="20" alt="home-image">
                                    <p>Home noticias</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/testimonios')}}"
                                    class="{{ Request::path() === '/admin/testimonios' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/testimonio.svg" width="20" alt="home-image">
                                    <p>Testimonios</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/solicitudes')}}"
                                    class="{{ Request::path() === '/admin/solicitudes' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/solicitudes.svg" width="20" alt="home-image">
                                    <p>Solicitudes</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/admin/contacto')}}"
                                    class="{{ Request::path() === '/admin/contacto' ? 'nav-link active' : 'nav-link' }}">
                                    <img src="/svg/contacto.svg" width="20" alt="home-image">
                                    <p>Contactos PFF</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">
                                    <img src="/svg/register.svg" width="20" alt="home-image">
                                    <p>Registrar Administrador</p>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </nav>
                    <!-- /.navbar -->
                    <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper mt-6" style="background: white">
                <section class="content">
                    @yield('content')
                </section>
                <!-- Main content -->
                <section class="content">
                    @yield('contentUsu')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <!-- NO QUITAR -->
                <strong>
                    <div class="float-right d-none d-sm-inline-block">
                        <b></b>
                    </div>
            </footer>

            <!-- Control Sidebar -->
            {{-- <aside class="control-sidebar control-sidebar-dark">
                <h1>Hola que hace</h1>
            </aside> --}}
            <!-- /.control-sidebar -->
        </div>
    </div>
    <script src="{{asset('js/responsive.js') }}"></script>
    <script>
        function countChars(obj){
        document.getElementById("charNum").innerHTML = obj.value.length+' caracteres';}
    </script>
</body>

</html>