<div class="row">
  @forelse ($boton3 as $colaborador3)
  <div id="foto-colaborador" class="col-md-4 col-xs-6">
    <div class="cards">
      <div class="cards-img">
        <div class="position-relative inner-box">
          <div class="image position-relative ">
            <img src="/img/admin/colaborador/{{ $colaborador3->imagen }}" alt="">
            <div class="overlay-box">
              <div class="overlay-inner">
                <div class="overlay-content">
                  <h3>{{ $colaborador3->nombre }}</h3>
                  @forelse ($colaborador3->Rol as $rol)
                  <p> {{ $rol->nombre; }}</p>
                  @empty
                  @endforelse
                </div>
              </div>
            </div>
          </div>
          <div class="cards-body">
        

            <li><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<a href="mailto: {{$colaborador3->correo}}">{{
                $colaborador3->correo }}</a></li>
          </div>
        </div>
      </div>
    </div>
  </div>
  @empty
  @endforelse
</div>
<div style="display: flex; justify-content: center;">
  <style>
    .pagination a {
      color: #dc6b6b;
    }

    .page-item.active .page-link {
      background-color: #dc6b6b;
      border-color: #dc6b6b;
    }
  </style>
  {{ $boton3->links() }}
</div>