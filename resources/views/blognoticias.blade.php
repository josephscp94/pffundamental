<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Blog Noticias</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <link href="{{ asset('lib/themify/css/themify-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('lib//magnific-popup/magnific-popup.css') }}" rel="stylesheet">
  <link href="{{ asset('css/blog_noticias.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>

<body>
  <!-- Preloader Start -->
  <header>
    @include('layouts.head')
  </header>
  <!-- Preloader Start -->

  <div class="container-fluid page-header mb-5 p-0" style="background-image: url('/img/heading.png')  ">
    <div class="container-fluid page-header-inner py-5">
      <div class="container text-center">
        <h1 class="display-3 text-white mb-3 animated slideInDown">Blog de noticias</h1>
       
      </div>
    </div>
  </div>
  </main>
  <main>
    <!-- About US Start -->
    <div class="about-area2 gray-bg pt-60 pb-60">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb justify-content-left ">
            <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
            <li class="breadcrumb-item text-black active" aria-current="page"><strong>Blog de noticias</strong></li>
          </ol>
        </nav>
        <div class="row">
          <div class="col-lg-12">
            <div class="whats-news-wrapper">
              <!-- Heading & Nav Button -->
              <div class="row justify-content-between align-items-end mb-15">
                <div class="col-xl-12">
                  <div class="section-tittle mb-30">
                    <h3>Todas las noticias</h3>
                  </div>
                </div>
              </div>
              <!-- Tab content -->
              <div class="row">
                <div class="col-12">
                  <!-- Nav Card -->
                  <div class="tab-content" id="nav-tabContent">
                    <!-- card one -->
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                      <div class="row">
                        @forelse ($home_noticias as $noticia)
                        <div class="col-xl-6 col-lg-6 col-md-6">
                          <div class="whats-news-single mb-40 mb-40">
                            <div class="whates-img">
                              <img src="/img/admin/home_noticias/{{ $noticia->imagen }}" alt="">
                            </div>
                            <div class="whates-caption whates-caption2">
                              <h4><a href="{{route('noticias',$noticia->slug)}}">{{
                                  $noticia->titular }}</a></h4>
                              <p>{{
                                $noticia->bajada }}</p>
                              <span>{{$noticia->created_at->format('d M, y') }}</span>
                            </div>
                          </div>
                        </div>
                        @empty
                        <h4>No hay noticias para mostrar</h4>
                        @endforelse
                      </div>
                      <div style="display: flex; justify-content: center;">
                        <style>
                          .pagination a {
                            color: #dc6b6b;
                          }

                          .page-item.active .page-link {
                            background-color: #dc6b6b;
                            border-color: #dc6b6b;
                          }
                        </style>
                        {{ $home_noticias->links() }}
                      </div>
                    </div>
                  </div>
                  <!-- End Nav Card -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </main>
  </div>


  <!-- JS here -->

  <!-- Jquery, Popper, Bootstrap -->
  <script src="js/vendor/jquery-1.12.4.min.js"></script>

  <!-- Jquery Slick , Owl-Carousel Plugins -->
  <script src="js/slick.min.js"></script>

  <!-- Jquery Plugins, main Jquery -->
  <script src="js/plugins.js"></script>
  <script src="js/blog_noticias.js"></script>

</body>
@include('layouts.footer')

</html>