<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Carreras</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <!--Bootstrap ================================================== -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

  <!-- Style Sheet ================================================== -->
  <link href="{{ asset('css/home.css') }}" rel="stylesheet">
  <link href="{{ asset('css/carrera.css') }}" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
</head>

<body>
  <header>
    @include('layouts.head')
  </header>

  <body data-bs-spy="scroll" data-bs-target="#header-nav" tabindex="0">

    <main>

      <div class="container-fluid page-header mb-5 p-0" id="mb-5"
        style=" background-image: url('/img/heading.png')  ">
        <div class="container-fluid page-header-inner py-5">
          <div class="container text-center">
            <h1 class="display-3 text-white mb-3 animated slideInDown">Carreras</h1>

          </div>
        </div>
      </div>

    </main>
    <section id="projects">
      <div class="container-xxl service py-5">
        <div class="container">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-left ">
              <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
              <li class="breadcrumb-item"><a href="{{route('competencias-genericas')}}"><strong>Competencias
                    genéricas</strong></a>
              </li>
              <li class="breadcrumb-item text-black active" aria-current="page"><strong>Carreras</strong></li>
            </ol>
          </nav>
          <div class="container-fluid my-5 pt-5 p-0 " id="mb-5">
            <h2 class="text-center fw-bold display-4 mb-5">Facultades</h2>
            <h5 class="text-center mt-5">Seleccione una carrera para ver los proyectos</h5>
            <br>
            <div class="mb-4">
              <p class="text-center">
                <button class="filter-button px-3 me-2 mb-3 active" data-filter="*">Todas</button>
                @forelse ($facultades as $facultad)
                <button class="filter-button px-3 me-2 mb-3" data-filter=".{{ $facultad->id }}">{{ $facultad->nombre
                  }}</button>
                @empty
                @endforelse
              </p>
            </div>

            <div class="isotope-container">
              @forelse ($carreras as $carrera)
              <div class="col-md-4 item {{$carrera->facultad_id }}">
                <a href="{{route('proyectos',['id'=> $carrera->id])}}"><img
                    src="/img/admin/carrera/{{ $carrera->imagen }}" alt="image" class="img-fluid"></a>
                {{-- <div class="description text-center px-3 py-5"> --}}
                  <a href="{{route('proyectos',['id'=> $carrera->id])}}">
                    <h3 class="project-heading">{{
                      $carrera->nombre}}</h3>
                  </a>
                </div>
                {{--
              </div> --}}
              @empty
              @endforelse
            </div>
          </div>
        </div>

      </div>


    </section>

    <!-- script ================================================== -->
    <script src="js/carrera-js/jquery-1.11.0.min.js"></script>
    <script src="js/carrera-js/plugins.js"></script>
    <script src="js/carrera-js/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>

  </body>

  <footer>
    @include('layouts.footer')
  </footer>

</html>