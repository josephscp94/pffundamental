<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Programa de Formación Fundamental | Proyecto-Detalle</title>
    <link rel="icon" href="/svg/Logo_blanco-2.svg">
    <link href="{{ asset('css/proyecto-detalle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/head.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    {{--
    <link href="css/bootstrap1.min.css" rel="stylesheet"> --}}

</head>
<header>
    @include('layouts.head')
</header>


<body>

    <main>

        <div class="container-fluid page-header mb-5 p-0"
            style=" background-image: url(/img/admin/carrera/{{ $proyecto->carrera->imagen }})">
            <div class="container-fluid page-header-inner py-5" >
                <div class="container text-center">
                    <h1 class="display-3 text-white mb-3 animated slideInDown">Proyectos</h1>

                </div>
            </div>
        </div>
    </main>
    <article>


        <div class="container" id="container-proyecto-detalle">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-left ">
                    <li class="breadcrumb-item"><a href="{{route('/')}}"><strong>Inicio</strong></a></li>
                    <li class="breadcrumb-item"><a href="{{route('competencias-genericas')}}"><strong>Competencias
                                genéricas</strong></a>
                    <li class="breadcrumb-item text-black active" aria-current="page"><a
                            href="{{route('carreras')}}"><strong>Proyectos</strong></a>
                    </li>
                    <li class="breadcrumb-item text-black active" aria-current="page"><strong>{{
                            $proyecto->nombre }}</strong>
                    </li>
                </ol>
            </nav>
            {{-- <div class="boxed"> --}}
                <div class="row">
                    <div class="col-md-8">
                        <div class="content blog-list">
                            <div class="blog-wrapper clearfix">
                                <div class="blog-meta">
                                    <h3><a title="">{{ $proyecto->nombre }}</a></h3>
                                    <ul class="list-inline">
                                        <li>Periodo: {{ $proyecto->Periodo->año }}</li>
                                        <li><a>{{ $proyecto->Carrera->nombre }}</a></li>
                                    </ul>
                                </div><!-- end blog-meta -->

                                <div class="blog-media" width="1280" height="1024">
                                    <a title=""><img class="img-proyecto" width="1280" height="1024"
                                            src="/img/admin/proyecto/{{ $proyecto->imagen }}"></a>
                                </div><!-- end media -->

                                <div class="blog-descripcion">
                                    <h3 class="lead" class="underline">Descripción general del proyecto</h3>
                                    <div class="mt-4 flex flex-col gap-4">
                                        <?php $parrafos = explode("\n", $proyecto->descripcion) ?>
                                        @foreach($parrafos as $parrafo)
                                        @if(trim($parrafo) != "")
                                        <p class="font-cuerpo font-normal text-negro text-lg">{{$parrafo}}</p>
                                        @endif
                                        @endforeach
                                    </div>
                                </div><!-- end desc -->

                                <div class="blog-desc-objetivo">
                                    <h3 class="lead" class="underline">Objetivo de Desarrollo Sostenible</h3>
                                    <p>
                                    <div class="media-body">
                                        @forelse ($proyecto->Ods as $ods)
                                        <p class="font-cuerpo font-normal text-negro text-lg">{{ $ods->nombre; }}
                                        </p>
                                        @empty
                                        @endforelse
                                    </div>
                                    </p>
                                </div><!-- end desc -->

                            </div><!-- end blog -->
                        </div><!-- end content -->
                    </div><!-- end col -->

                    <div class="sidebar col-md-4">

                        <div class="widget clearfix">
                            <h3 class="widget-title">#Tags</h3>
                            <br>
                            <div class="tags-widget">
                                <ul class="list-inline">
                                    @forelse ($proyecto->Tag as $tag)
                                    <li><a href="{{route('tags',$tag->id)}}">{{ $tag->nombre; }}</a></li>
                                    @empty
                                    @endforelse
                                </ul>
                            </div><!-- end list-widget -->
                        </div><!-- end widget -->

                        <div class="widget clearfix">
                            <h3 class="widget-title">Socio comunitario</h3>
                            <div class="post-widget">
                                <div class="media">
                                    {{-- <img src="upload/blog_small_01.jpg" alt=""
                                        class="img-responsive alignleft img-rounded"> --}}
                                    <div class="media-body">
                                        <h5 class="mt-0"><a>{{ $proyecto->Socio_comunitario->nombre }}</a></h5>
                                    </div>
                                </div>
                            </div><!-- end post-widget -->
                        </div><!-- end widget -->
                        {{-- para imagenes --}}
                        {{-- <div class="widget clearfix">
                            <div class="banner-widget">
                                <img src="upload/banner.jpeg" alt="" class="img-responsive img-rounded">
                            </div>
                        </div> --}}

                        <div class="widget clearfix">
                            <h3 class="widget-title">Alumnos del proyecto</h3>
                            <div class="post-widget">
                                <div class="media">
                                    {{-- <img src="upload/blog_small_01.jpg" alt=""
                                        class="img-responsive alignleft img-rounded"> --}}
                                    <div class="media-body">
                                        @forelse ($proyecto->Alumno as $alumno)
                                        <p class="mt-0"><a>{{ $alumno->nombre; }}</a></p>
                                        @empty
                                        @endforelse
                                    </div>
                                </div>

                            </div><!-- end post-widget -->
                        </div><!-- end widget -->

                    </div><!-- end sidebar -->
                </div><!-- end row -->
                {{--
            </div><!-- end boxed --> --}}
        </div><!-- end container -->
        </div>

    </article>

</body>
<footer>
    @include('layouts.footer')
</footer>


</html>