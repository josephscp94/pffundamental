<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- Libraries Stylesheet -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap1.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/home_carrusel.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
</head>

<!-- Carousel Start -->
<div class="container-fluid header-carousel px-0 mb-5">
    <div class="container">
    <div id="header-carousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            
                <?php 
                foreach($home_carrusel as $carrusel => $slide) {
                    $active = ($carrusel === 0) ? "active" : "";
                    
                    echo '<div class="carousel-item '.$active.'">
                        <img class="w-100" src="/img/admin/home_carrusel/'.$slide["imagen"].'" alt="Image">
                        <div class="carousel-caption ">
                            <div class="container">
                                <div class="row justify-content-start">
                                    <div class="col-lg-7 text-start">
                                        <h1 class="display-1 text-white animated slideInRight mb-3">'.$slide["descripcion"].'
                                        </h1>';
                                        
                    if (!empty($slide["link"])) {
                        echo '  <a href="'.$slide["link"].'" class="myButton">Ver más</a>';
                      
                    }
                    
                    echo '</div>
                                </div>
                                    </div>
                                        </div>
                        <div class="progress-bar__container">
                            <div class="progress-bar">
                                <span class="progress-bar__text">Uploaded Successfully!</span>
                            </div>
                        </div>
                    </div>';
                    
                }
                ?>

            </div>

            <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#header-carousel" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>



    </div>
</div>
<!-- Carousel End -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

</html>