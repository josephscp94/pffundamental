<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Programa de Formación Fundamental | Inicio</title>
  <link rel="icon" href="/svg/Logo_blanco-2.svg">
  <link href="css/bootstrap1.min.css" rel="stylesheet">
  <link href="{{ asset('css/home.css') }}" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <!-- Bootstrap core CSS -->


</head>
<header>
  @include('layouts.head')
</header>
{{-- <div id="preloader-active">
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="preloader-inner position-relative">
      <div class="preloader-circle"></div>
      <div class="preloader-img pere-text">
        <img src="/svg/utalca.svg" alt="">
      </div>
    </div>
  </div>
</div> --}}

{{-- @section('scrollup')

@endsection --}}
@include('home_carrusel')
<br>

@include('home_noticias')

@include('testimonio')


<footer>
  @include('layouts.footer')
</footer>

</html>