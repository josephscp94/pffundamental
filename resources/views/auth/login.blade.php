<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login-administrador</title>
    <link rel="icon" href="/svg/Logo_blanco-2.svg">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="css/login.css">
</head>

<body>
    @section('content')
    @if ($errors->has('login_error'))
    <div class="alert alert-danger">
        {{ $errors->first('login_error') }}
    </div>
    @endif

    <div class="login-area">
        <div class="bg-image">
            <div class="login-signup">
                <div class="container">
                    <div class="login-header">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="login-logo">
                                    <img src="/img/logo_utalca_oscuro.png" alt="PFF" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="login-details">
                                    <ul class="nav nav-tabs navbar-right">

                                        <li class="active"><a data-toggle="tab" href="#login">Login</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        {{-- <div id="register" class="tab-pane">
                            <div class="login-inner">
                                <div class="title">
                                    <h1>Bienvenido<span>!</span></span></h1>
                                </div>
                                <div class="login-form">
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf >

                                        <div class="form-details">
                                            <label class="name">
                                                <input type="text" name="name" placeholder="Full Name" id="name">
                                            </label>
                                            <label class="email">
                                                <input type="email" name="email" placeholder="correo" id="email">
                                            </label>
                                            <label class="password">
                                                <input type="password" name="password" placeholder="Password"
                                                    id="password">
                                            </label>
                                            <label class="pass">
                                                <input type="password" name="password" placeholder="Confirm Password"
                                                    id="password">
                                            </label>
                                        </div>
                                        <button type="submit" class="form-btn" onsubmit="">Sent</button>
                                    </form>
                                </div>
                            </div>
                        </div> --}}
                        <div id="login" class="tab-pane fade in active">
                            <div class="login-inner">
                                <div class="title">
                                    <h1>Bienvenido <span>!</span></h1>
                                </div>
                                <div class="login-form">
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-details">
                                            <label class="email">
                                                <input type="email" name="email" placeholder="Ingresa tu correo "
                                                    id="email">
                                            </label>
                                            <label class="password">
                                                <input type="password" name="password"
                                                    placeholder="Ingresa tu contraseña" id="password">
                                            </label>
                                        </div>
                                        <button type="submit" class="form-btn" onsubmit="">Ingresar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
   

</body>

</html>