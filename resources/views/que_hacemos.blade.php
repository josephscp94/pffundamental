<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Programa de Formación Fundamental | Qué hacemos</title>
	<link rel="icon" href="/svg/Logo_blanco-2.svg">
	<link href="{{ asset('css/que_hacemos.css') }}" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

</head>

<body>
	{{-- <section>
		@yield('scrollup')
	</section> --}}
	<header>
		@include('layouts.head')

	</header>



	<main>
		<div class="container-fluid page-header mb-5 p-0"
			style=" background-image: url('/img/heading.png')  ">
			<div class="container-fluid page-header-inner py-5" >
				<div class="container text-center">
					<h1 class="display-3 text-white mb-3 animated slideInDown">Qué hacemos</h1>

				</div>
			</div>
		</div>
	</main>

	<body>
		<article>
			<div class="container-xxl service py-5">
				<div class="container" id="container-que-hacemos">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb justify-content-left">
							<li class="breadcrumb-item"><a href="{{ route('/') }}"><strong>Inicio</strong></a></li>
							<li class="breadcrumb-item text-black active" aria-current="page"><strong>Qué
									hacemos</strong></li>
						</ol>
					</nav>

					<div class="row g-4" data-wow-delay="0.3s">
						<div class="col-lg-4">
							<div class="nav w-100 nav-pills me-4">
								<button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4 active"
									data-bs-toggle="pill" data-bs-target="#tab-pane-1" type="button">
									<img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
									<h4 class="m-0">Compromiso institucional</h4>
								</button>
								<button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4"
									data-bs-toggle="pill" data-bs-target="#tab-pane-2" type="button">
									<img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
									<h4 class="m-0"> Docencia</h4>
								</button>
								<button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-0"
									data-bs-toggle="pill" data-bs-target="#tab-pane-3" type="button">
									<img src="/svg/arrow.svg" alt="home-image" width="30" style="margin-right: 10px">
									<h4 class="m-0">Vinculación con el medio</h4>
								</button>
							</div>
						</div>
						<div class="col-lg-8">
							<div class="tab-content w-100">
								<div class="tab-pane fade show active" id="tab-pane-1">
									<div class="row g-4">

										<div class="col-md-12">
											<h1 class="titulos">Compromiso institucional</h1>
											<?php $parrafos = explode("\n",  $que_hacemos[0]->compromiso_institucional) ?>
											@foreach($parrafos as $parrafo)
											@if(trim($parrafo) != "")
											<h5 class="parrafo">{{$parrafo}}</h5>
											@endif
											@endforeach

										</div>
										<div class="col-md-12" style="min-height: 350px;">
											<div class="position-relative h-100">
												<img class="position-absolute img-fluid w-100 h-100"
													src="/img/admin/que_hacemos/{{ $que_hacemos[0]->imagen_ci }}"
													style="object-fit: cover;" alt="">
											</div>
										</div>
									</div>
								</div>

								<div class="tab-pane fade" id="tab-pane-2">
									<div class="row g-4">

										<div class="col-md-12">
											<h1 class="titulos">Docencia</h1>
											<?php $parrafos = explode("\n",  $que_hacemos[0]->docencia) ?>
											@foreach($parrafos as $parrafo)
											@if(trim($parrafo) != "")
											<h5 class="parrafo">{{$parrafo}}</h5>
											@endif
											@endforeach

										</div>
										<div class="col-md-12" style="min-height: 350px;">
											<div class="position-relative h-100">
												<img class="position-absolute img-fluid w-100 h-100"
													src="/img/admin/que_hacemos/{{ $que_hacemos[0]->imagen_dc }}"
													style="object-fit: cover;" alt="">
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab-pane-3">
									<div class="row g-4">

										<div class="col-md-12">
											<h1 class="titulos">Vinculación con el medio</h1>
											<?php $parrafos = explode("\n",  $que_hacemos[0]->vinculacion_con_el_medio ) ?>
											@foreach($parrafos as $parrafo)
											@if(trim($parrafo) != "")
											<h5 class="parrafo">{{$parrafo}}</h5>
											@endif
											@endforeach
										</div>
										<div class="col-md-12" style="min-height: 350px;">
											<div class="position-relative h-100">
												<img class="position-absolute img-fluid w-100 h-100"
													src="/img/admin/que_hacemos/{{ $que_hacemos[0]->imagen_vcm }}"
													style="object-fit: cover;" alt="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</article>

		<!-- Service End -->

		<footer>
			@include('layouts.footer')
		</footer>

		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
		<script src="js/jquery.scrollUp.min.js"></script>

	</body>
</body>

</html>