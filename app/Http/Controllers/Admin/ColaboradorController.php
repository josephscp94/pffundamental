<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Colaborador;
use App\Models\Rol;
use App\Models\User;

class ColaboradorController extends Controller
{

    public function index(Request $request)
    {
        $colaboradores = Colaborador::orderBy('nombre', 'asc')->paginate(10);
        $colaboradores = Colaborador::all();
        if ($request) {
            $query = trim($request->get('search'));
            $colaboradores = Colaborador::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.colaborador.index', ['colaboradores' => $colaboradores, 'search' => $query]);
        }
        return view("admin.colaborador.index", compact("colaboradores"));
    }

    public function create()
    {
        $roles = Rol::all()->sortBy('nombre');
        $users = User::all();
        return view('admin.colaborador.create', compact('roles', 'users'));
    }

    public function store(Request $request)
    {

        $colaborador = new Colaborador();

        $colaborador->nombre         = request('nombre');
        $colaborador->correo         = request('correo');
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/colaborador', $file->getClientOriginalName());
            $colaborador->imagen = $file->getClientOriginalName();
        }
        $colaborador->save();
        $colaborador->Rol()->attach($request->input('roles'));
        $colaborador->Users()->attach($request->user()->id);
        return redirect()->route('colaborador.index');
    }

    public function edit($id)
    {

        $colaborador = Colaborador::findOrFail($id);
        $roles = Rol::all()->sortBy('nombre');
        return view('admin.colaborador.edit', compact('colaborador', 'roles'));
    }

    public function update(Request $request, $id)
    {

        $colaborador = Colaborador::findOrFail($id);
        $rol_id  = $request->get('rol_id');
        $colaborador->fill($request->all());

        $imagen_anterior = $colaborador->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/colaborador', $file->getClientOriginalName());
            $colaborador->imagen = $file->getClientOriginalName();
        }
        $colaborador->save();
        $colaborador->Rol()->sync($request->input('roles'));

        return redirect()->route('colaborador.index');
    }

    public function destroy($id)
    {
        $colaborador = colaborador::find($id);
        unlink(public_path('img/admin/colaborador/' . $colaborador->imagen));
        $colaborador->delete();
        return redirect('admin/colaborador');
    }
}
