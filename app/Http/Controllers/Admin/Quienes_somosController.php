<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quienes_somos;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class Quienes_somosController extends Controller
{

    public function index(Request $request)
    {
        $quienessomos = Quienes_somos::all();
        if ($request) {
            $query = trim($request->get('search'));
            $quienessomos = Quienes_somos::where('proposito', 'LIKE', '%' . $query . '%')
                ->orderBy('proposito', 'asc')
                ->paginate(10);
            return view('admin.quienes_somos.index', ['quienessomos' => $quienessomos, 'search' => $query]);
        }
    }

    public function create()
    {
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.quienes_somos.create', compact('user'));
    }

    public function store(Request $request)
    {

        $quienessomos = new Quienes_somos();
        $quienessomos->url      = request('url');
        $quienessomos->user_id = $request->user()->id;
        $quienessomos->save();
        return redirect()->route('quienes_somos.index');
    }

    public function edit($id)
    {
        $quienessomos = Quienes_somos::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.quienes_somos.edit', compact('quienessomos', 'user'));
    }

    public function update(Request $request, $id)
    {
        $quienessomos = Quienes_somos::findOrFail($id);
        $quienessomos->fill($request->all());
        $quienessomos->user_id = $request->user()->id;
        $quienessomos->save();
        return redirect()->route('quienes_somos.index');
    }

    public function destroy($id)
    {
        $quienessomos = Quienes_somos::find($id);
        $quienessomos->delete();
        return redirect('admin/quienes_somos');
    }
}
