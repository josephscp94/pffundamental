<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;


use App\Models\Proyecto;
use App\Models\Socio_comunitario;
use App\Models\Solicitudes;
use App\Models\Tag;
use App\Models\Ods;
use Illuminate\Http\Request;
use App\Models\User;

class ResumenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cons_proyectos = Proyecto::count();
        $cons_sc = Socio_comunitario::count();
        $cons_solicitudes = Solicitudes::count();
        $cons_tags = Tag::count();
        $cons_ods = Ods::count();

        return view('admin.resumen', compact('cons_proyectos', 'cons_sc', 'cons_solicitudes', 'cons_tags', 'cons_ods'));
    }
}
