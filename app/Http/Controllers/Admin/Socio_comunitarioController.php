<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Socio_comunitario;

class Socio_ComunitarioController extends Controller
{

    public function index(Request  $request)
    {
        $socios_comunitarios = Socio_Comunitario::orderBy('nombre', 'asc')->paginate(10);

        if ($request) {
            $query = trim($request->get('search'));
            $socios_comunitarios = Socio_comunitario::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.socio_comunitario.index', ['socios_comunitarios' => $socios_comunitarios, 'search' => $query]);
        }
    }

    public function create()
    {
        return view("admin.socio_comunitario.create");
    }

    public function store(Request $request)
    {

        $socio_comunitario = new Socio_comunitario();

        $socio_comunitario->nombre        = request('nombre');
        $socio_comunitario->descripcion        = request('descripcion');
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/socio_comunitario', $file->getClientOriginalName());
            $socio_comunitario->imagen = $file->getClientOriginalName();
        }
        $socio_comunitario->save();
        return redirect()->route('socio_comunitario.index');
    }

    public function edit($id)
    {
        $socio_comunitario = Socio_comunitario::findOrFail($id);
        return view('admin.socio_comunitario.edit', compact('socio_comunitario'));
    }

    public function update(Request $request, $id)
    {
        $socio_comunitario = Socio_comunitario::findOrFail($id);
        $socio_comunitario->fill($request->all());

        $imagen_anterior = $socio_comunitario->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/socio_comunitario', $file->getClientOriginalName());
            $socio_comunitario->imagen = $file->getClientOriginalName();
        }
        $socio_comunitario->save();
        return redirect()->route('socio_comunitario.index');
    }

    public function destroy($id)
    {
        $socio_comunitario = Socio_comunitario::find($id);
        unlink(public_path('img/admin/socio_comunitario/' . $socio_comunitario->imagen));
        $socio_comunitario->delete();
        return redirect('admin/socio_comunitario');
    }
}

