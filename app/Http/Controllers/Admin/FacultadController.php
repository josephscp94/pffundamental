<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Facultad;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class FacultadController extends Controller
{

    public function index()
    {
        $facultades = Facultad::all();
        return view("admin.facultad.index",compact("facultades"));
    }

    public function create()
    {
        return view("admin.facultad.create");
    }

    public function store(Request $request){

        $facultad = new Facultad();
            
        $facultad->nombre        = request('nombre');
        $facultad->slug          = Str::slug($request->get('nombre'));
        $facultad->save();
        return redirect()->route('facultad.index');
    }

    public function edit($id){
        $facultad = Facultad::findOrFail($id);
        return view('admin.facultad.edit',compact('facultad'));
    }

    public function update(Request $request, $id)
    {
        $registro = Facultad::findOrFail($id);
        $registro->fill($request->all());
        $registro->save();
        return redirect()->route('facultad.index');
    }

    public function destroy($id)
    {
        $facultad = Facultad::find($id);
        $facultad->delete();
        return redirect('admin/facultad');
    }
}
