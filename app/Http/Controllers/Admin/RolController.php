<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Area;
use Illuminate\Http\Request;
use App\Models\Rol;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class RolController extends Controller
{

    public function index(Request $request)
    {
        $roles = Rol::all();
        if ($request) {
            $query = trim($request->get('search'));
            $roles = Rol::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.rol.index', ['roles' => $roles, 'search' => $query]);
        }
        return view("admin.rol.index", compact("roles"));
    }

    public function create()
    {
        $areas = Area::all()->sortBy('nombre');
        return view("admin.rol.create", compact('areas'));
    }

    public function store(Request $request)
    {

        $rol = new Rol();
        $rol->area_id = ($request->input('area_id'));
        $rol->nombre        = request('nombre');
        $rol->save();
        return redirect()->route('rol.index');
    }

    public function edit($id)
    {
        $rol = Rol::findOrFail($id);
        $areas = Area::all()->sortBy('nombre');
        return view('admin.rol.edit', compact('rol', 'areas'));
    }

    public function update(Request $request, $id)
    {
        $registro = Rol::findOrFail($id);
        $registro->area_id = ($request->input('area_id'));
        $registro->fill($request->all());
        $registro->save();
        return redirect()->route('rol.index');
    }

    public function destroy($id)
    {
        $rol = Rol::find($id);
        $rol->delete();
        return redirect('admin/rol');
    }
}
