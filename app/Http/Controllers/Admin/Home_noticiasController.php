<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Home_noticias;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Nette\Utils\Image;

class Home_noticiasController extends Controller
{

    public function index(Request $request)
    {

        $noticias = Home_noticias::all();
        if ($request) {
            $query = trim($request->get('search'));
            $noticias = Home_noticias::where('titular', 'LIKE', '%' . $query . '%')
                ->orderBy('titular', 'asc')
                ->paginate(10);
            return view('admin.home_noticias.index', ['noticias' => $noticias, 'search' => $query]);
        }
    }

    public function create()
    {
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.home_noticias.create', compact('user'));
    }

    public function store(Request $request)
    {

        $noticias = new Home_noticias($request->all());

        $noticias->slug = Str::slug(($noticias->titular));
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/home_noticias', $file->getClientOriginalName());
            $noticias->imagen = $file->getClientOriginalName();
        }
        $noticias->estado = request('estado') ? 1 : 0;
        $noticias->user_id = $request->user()->id;
        $noticias->save();
        return redirect()->route('home_noticias.index');
    }

    public function edit($id)
    {
        $noticias = Home_noticias::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.home_noticias.edit', compact('noticias', 'user'));
    }

    public function update(Request $request, $id)
    {
        $noticias = Home_noticias::findOrFail($id);
        $noticias->fill($request->all());

        $imagen_proy_anterior = $noticias->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/home_noticias', $file->getClientOriginalName());
            $noticias->imagen = $file->getClientOriginalName();
        }

        $noticias->estado       = $request->get('estado') ? 1 : 0;
        $noticias->user_id = $request->user()->id;
        $noticias->save();
        return redirect()->route('home_noticias.index');
    }

    public function destroy($id)
    {
        $noticias = Home_noticias::find($id);
        unlink(public_path('img/admin/home_noticias/' . $noticias->imagen));
        $noticias->delete();
        return redirect('admin/home_noticias');
    }
}
