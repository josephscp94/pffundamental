<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class TagController extends Controller
{

    public function index()
    {
        $tags = Tag::all();
        return view("admin.tag.index", compact("tags"));
    }

    public function create()
    {
        return view("admin.tag.create");
    }

    public function store(Request $request)
    {

        $tag = new Tag();

        $tag->nombre        = request('nombre');
        $tag->slug          = Str::slug($request->get('nombre'));
        $tag->save();
        return redirect()->route('tag.index');
    }

    public function edit($id)
    {
        $tags = Tag::findOrFail($id);
        return view('admin.tag.edit', compact('tags'));
    }

    public function update(Request $request, $id)
    {
        $registro = Tag::findOrFail($id);
        $registro->fill($request->all());
        $registro->save();
        return redirect()->route('tag.index');
    }

    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        return redirect('admin/tag');
    }
}
