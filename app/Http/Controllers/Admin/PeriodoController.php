<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Periodo;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PeriodoController extends Controller
{

    public function index()
    {
        $periodos = Periodo::all();
        return view("admin.periodo.index", compact("periodos"));
    }

    public function create()
    {
        return view("admin.periodo.create");
    }

    public function store()
    {
        $año = request('año');

        // // Verificar si los datos ya existen en la base de datos
        $periodoExistente = Periodo::where('año', $año)->first();

        if ($periodoExistente) {
            //     Los datos ya existen, maneja el error o redirige de vuelta con un mensaje de error
            return redirect()->back()->withErrors('El año ya existe en la base de datos');
        }

        // Los datos no existen, procede a almacenarlos
        $periodo = new Periodo();
        $periodo->año = $año;
        $periodo->save();

        return redirect()->route('periodo.index');
    }

    public function edit($id)
    {
        $periodos = Periodo::findOrFail($id);
        return view('admin.periodo.edit', compact('periodos'));
    }

    public function update(Request $request, $id)
    {
        $periodo = Periodo::findOrFail($id);
        $año = $request->input('año');

        // Verificar si los datos ya existen en la base de datos
        $periodoExistente = Periodo::where('año', $año)->where('id', '!=', $id)->first();

        if ($periodoExistente) {
            // Los datos ya existen, maneja el error o redirige de vuelta con un mensaje de error
            return redirect()->back()->withErrors('El año ya existe en la base de datos');
        }

        // Los datos no existen, procede a actualizarlos
        $periodo->fill($request->all());
        $periodo->save();

        return redirect()->route('periodo.index');
    }
}
