<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alumno;
use App\Models\Proyecto;
use App\Models\Socio_comunitario;
use App\Models\User;
use App\Models\Tag;
use App\Models\Ods;
use App\Models\Carrera;
use App\Models\Periodo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ProyectoController extends Controller
{
    // Otros métodos...

    public function generateUniqueSlug($name, $socioComunitarioId)
    {
        $slug = Str::slug($name);
        $count = Proyecto::where('nombre', $name)
                         ->where('socio_comunitario_id', $socioComunitarioId)
                         ->count();
    
        Log::debug("Nombre: {$name}, Socio Comunitario ID: {$socioComunitarioId}, Count: {$count}");
    
        return $count ? "{$slug}-{$count}" : $slug;
    }
   public function index(Request  $request)
    {
        $proyectos = Proyecto::all();
        $socio_comunitarios = Socio_comunitario::all();
        $tags = Tag::all();
        $carreras = Carrera::all();
        $odss = Ods::all();
        $alumnos = Alumno::all();
        $periodos = Periodo::all();
        if ($request) {
            $query = trim($request->get('search'));
            $proyectos = Proyecto::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.proyecto.index', ['proyectos' => $proyectos, 'search' => $query]);
        }
        return view("admin.proyecto.index", compact('socio_comunitarios', 'tags', 'carreras', 'odss', 'alumnos', 'periodos'));
    }

    public function create()
    {
        $socio_comunitarios = Socio_comunitario::all()->sortBy('nombre');
        $users = User::all();
        $tags = Tag::all()->sortBy('nombre');
        $carreras = Carrera::all()->sortBy('nombre');
        $odss = Ods::all()->sortBy('nombre');
        $periodos = Periodo::all()->sortBy('año');
        return view('admin.proyecto.create', compact('socio_comunitarios', 'users', 'tags', 'carreras', 'odss', 'periodos'));
    }
    public function store(Request $request)
    {
        // Validación y lógica previa...

        // Generar un slug único
        $slug = $this->generateUniqueSlug($request->nombre, $request->socio_comunitario_id);
        Log::debug("Slug generado: {$slug}"); // Agregar un registro de depuración

        $proyecto = new Proyecto();
        $proyecto->carrera_id = ($request->input('carrera_id'));
        $proyecto->periodo_id = ($request->input('periodo_id'));
        $proyecto->socio_comunitario_id = ($request->input('socio_comunitario_id'));

        $proyecto->nombre         = request('nombre');
        $proyecto->slug           = $slug; // Usar el slug generado
        $proyecto->descripcion    = request('descripcion');

       if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/proyecto', $file->getClientOriginalName());
            $proyecto->imagen = $file->getClientOriginalName();
        }
        $proyecto->save();
  $proyecto_id = $proyecto->id;
     Log::debug("Proyecto guardado con ID: {$proyecto->id}"); // Agregar un registro de depuración
        $proyecto->Users()->attach($request->user()->id);
        $proyecto->Tag()->attach($request->input('tags'));
        $proyecto->Ods()->attach($request->input('odss'));

        $alumnos  = request('alumnos');
        foreach ($alumnos as $alumno) {
            if ($alumno != NULL) {
                $var = new Alumno();
                $var->proyecto_id = $proyecto_id;
                $var->nombre = $alumno;
                $var->save();
            }
        }
        return redirect()->route('proyecto.index');
    }

    public function edit($id)
    {

        $proyecto = Proyecto::findOrFail($id);
        $alumnos = Alumno::where('proyecto_id', $id)->orderBy('nombre', 'asc')->get();
        $socio_comunitarios = Socio_comunitario::all()->sortBy('nombre');
        $tags = Tag::all()->sortBy('nombre');
        $carreras = Carrera::all()->sortBy('nombre');
        $odss = Ods::all()->sortBy('nombre');
        $periodos = Periodo::all()->sortBy('nombre');
        return view('admin.proyecto.edit', compact('proyecto', 'socio_comunitarios', 'tags', 'carreras', 'odss', 'periodos', 'alumnos'));
    }

       
    public function update(Request $request, $id)
    {
        $proyecto = Proyecto::findOrFail($id);
        // Validación y lógica previa...

        // Generar un slug único
        $slug = $this->generateUniqueSlug($request->nombre, $request->socio_comunitario_id);
        Log::debug("Slug generado: {$slug}"); // Agregar un registro de depuración

        $proyecto->carrera_id = ($request->input('carrera_id'));
        $proyecto->periodo_id = ($request->input('periodo_id'));
        $proyecto->socio_comunitario_id = ($request->input('socio_comunitario_id'));
        $proyecto->nombre         = request('nombre');
        $proyecto->slug           = $slug; // Usar el slug generado
        $proyecto->descripcion    = request('descripcion');

     $imagen_anterior = $proyecto->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/proyecto', $file->getClientOriginalName());
            $proyecto->imagen = $file->getClientOriginalName();
        }
        $proyecto->save();
        $proyecto->Tag()->sync($request->input('tags'));
        $proyecto->Ods()->sync($request->input('odss'));

        Alumno::where('proyecto_id', $id)->delete();
        $alumnos  = request('alumnos');
        foreach ($alumnos as $alumno) {
            if ($alumno != NULL) {
                $var = new Alumno();
                $var->proyecto_id = $id;
                $var->nombre = $alumno;
                $var->save();
            }
        }


        return redirect()->route('proyecto.index');
    }

public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        unlink(public_path('img/admin/proyecto/' . $proyecto->imagen));
        $proyecto->delete();
        return redirect('admin/proyecto');
    }
}
