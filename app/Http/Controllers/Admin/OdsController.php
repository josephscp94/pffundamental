<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ods;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class OdsController extends Controller
{

    public function index(Request $request)
    {
        $odss = Ods::orderBy('nombre', 'asc')->paginate(10);
        if ($request) {
            $query = trim($request->get('search'));
            $odss = Ods::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.ods.index', ['odss' => $odss, 'search' => $query]);
        }
        return view("admin.ods.index", compact("odss"));
    }

    public function create()
    {
        return view("admin.ods.create");
    }

    public function store(Request $request)
    {

        $ods = new Ods();

        $ods->nombre        = request('nombre');
        $ods->slug          = Str::slug($request->get('nombre'));
        $ods->save();
        return redirect()->route('ods.index');
    }

    public function edit($id)
    {
        $ods = Ods::findOrFail($id);
        return view('admin.ods.edit', compact('ods'));
    }

    public function update(Request $request, $id)
    {
        $registro = Ods::findOrFail($id);
        $registro->fill($request->all());
        $registro->save();
        return redirect()->route('ods.index');
    }

    public function destroy($id)
    {
        $ods = Ods::find($id);
        $ods->delete();
        return redirect('admin/ods');
    }
}
