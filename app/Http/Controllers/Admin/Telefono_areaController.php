<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Telefono_area;
use Illuminate\Http\Request;
use App\Models\User;

class Telefono_areaController extends Controller
{

    // public function index()
    // {
    //     $telefonos = Telefono_area::all();
    //     return view('admin.telefono_area.index', compact('telefonos'));
    // }

    public function create()
    {
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.telefono_area.create', compact('user'));
    }

    public function store(Request $request)
    {
        $telefonos = new Telefono_area($request->all());
        $telefonos->user_id = $request->user()->id;
        $telefonos->save();
        return redirect()->route('contacto.index');
    }

    public function edit($id)
    {
        $telefono = Telefono_area::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.telefono_area.edit', compact('telefono', 'user'));
    }

    public function update(Request $request, $id)
    {
        $telefono = Telefono_area::findOrFail($id);
        $telefono->fill($request->all());
        $telefono->user_id = $request->user()->id;
        $telefono->save();
        return redirect()->route('contacto.index');
    }

    public function destroy($id)
    {
        $telefono = Telefono_area::find($id);
        $telefono->delete();
        return redirect('admin/contacto');
    }
}
