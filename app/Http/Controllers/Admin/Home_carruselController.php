<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Home_carrusel;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Nette\Utils\Image;

class Home_carruselController extends Controller
{

    public function index(Request  $request)
    {
        $carrusel = Home_carrusel::all();
        if ($request) {
            $query = trim($request->get('search'));
            $carrusel = Home_carrusel::where('descripcion', 'LIKE', '%' . $query . '%')
                ->orderBy('descripcion', 'asc')
                ->paginate(10);
            return view('admin.home_carrusel.index', ['carrusel' => $carrusel, 'search' => $query]);
        }
    }

    public function create()
    {
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.home_carrusel.create', compact('user'));
    }

    public function store(Request $request)
    {
        $carrusel = new Home_carrusel($request->all());
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/home_carrusel', $file->getClientOriginalName());
            $carrusel->imagen = $file->getClientOriginalName();
        }
        $carrusel->estado       = $request->get('estado') ? 1 : 0;
        $carrusel->user_id = $request->user()->id;
        $carrusel->save();
        return redirect()->route('home_carrusel.index');
    }

    public function edit($id)
    {
        $carrusel = Home_carrusel::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.home_carrusel.edit', compact('carrusel', 'user'));
    }

    public function update(Request $request, $id)
    {
        $carrusel = Home_carrusel::findOrFail($id);
        $carrusel->fill($request->all());

        $imagen_proy_anterior = $carrusel->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/home_carrusel', $file->getClientOriginalName());
            $carrusel->imagen = $file->getClientOriginalName();
        }
        $carrusel->estado       = request('estado') ? 1 : 0;
        $carrusel->user_id = $request->user()->id;
        $carrusel->save();
        return redirect()->route('home_carrusel.index');
    }

    public function destroy($id)
    {
        $carrusel = Home_carrusel::find($id);
        unlink(public_path('img/admin/home_carrusel/' . $carrusel->imagen));
        $carrusel->delete();
        return redirect('admin/home_carrusel');
    }
}
