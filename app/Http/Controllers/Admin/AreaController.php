<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Area;

class AreaController extends Controller
{

    public function index()
    {
        $areas = Area::all();
        return view("admin.area.index", compact("areas"));
    }

    public function create()
    {
        return view("admin.area.create");
    }

    public function store(Request $request)
    {
        $area = new area();
        $area->nombre        = request('nombre');
        $area->save();
        return redirect()->route('area.index');
    }

    public function edit($id)
    {
        $areas = Area::findOrFail($id);
        return view('admin.area.edit', compact('areas'));
    }

    public function update(Request $request, $id)
    {
        $registro = Area::findOrFail($id);
        $registro->fill($request->all());
        $registro->save();
        return redirect()->route('area.index');
    }

    public function destroy($id)
    {
        $area = Area::find($id);
        $area->delete();
        return redirect('admin/area');
    }
}
