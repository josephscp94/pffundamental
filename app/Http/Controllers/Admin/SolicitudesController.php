<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Solicitudes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class SolicitudesController extends Controller
{

    public function index(Request  $request)
    {
        $solicitudes = Solicitudes::all();
        if ($request) {
            $query = trim($request->get('search'));
            $solicitudes = Solicitudes::where('nombre_institucion', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre_institucion', 'asc')
                ->paginate(10);
            return view('admin.solicitudes.index', ['solicitudes' => $solicitudes, 'search' => $query]);
        }
        return view("admin.solicitudes.index", compact("solicitudes"));
    }

    // public function store(Request $request)
    // {

    //     $solicitud = new Solicitudes();
    //     $solicitud->nombre_institucion       = request('nombre_institucion');
    //     $solicitud->descripcion        = request('descripcion');
    //     $solicitud->nombre_persona        = request('nombre_persona');
    //     $solicitud->correo        = request('correo');
    //     $solicitud->telefono        = request('telefono');
    //     $solicitud->save();
    //     return redirect()->route('socios-comunitarios');
    // }

    // public function edit($id)
    // {
    //     $tags = Tag::findOrFail($id);
    //     return view('admin.tag.edit', compact('tags'));
    // }

    // public function update(Request $request, $id)
    // {
    //     $registro = Tag::findOrFail($id);
    //     $registro->fill($request->all());
    //     $registro->save();
    //     return redirect()->route('tag.index');
    // }

    public function destroy($id)
    {
        $solicitud = Solicitudes::find($id);
        $solicitud->delete();
        return redirect('admin/solicitudes');
    }
}
