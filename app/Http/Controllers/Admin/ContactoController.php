<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Contacto;
use App\Models\Telefono_area;
use Illuminate\Http\Request;
use App\Models\User;

class ContactoController extends Controller
{

    public function index(Request $request)
    {
        $contacto = Contacto::all();
        $telefonos = Telefono_area::all();
        return view('admin.contacto.index', compact('contacto', 'telefonos'));
    }

    public function edit($id)
    {
        $contacto = Contacto::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.contacto.edit', compact('contacto', 'user'));
    }

    public function update(Request $request, $id)
    {
        $contacto = Contacto::findOrFail($id);
        $contacto->fill($request->all());

        $contacto->user_id = $request->user()->id;
        $contacto->save();
        return redirect()->route('contacto.index');
    }
}
