<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Competencia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CompetenciaController extends Controller
{

    public function index()
    {
        $competencia = Competencia::all();
        return view("admin.competencia.index", compact("competencia"));
    }

    public function create()
    {
        return view("admin.competencia.create");
    }

    public function store(Request $request)
    {

        $competencia = new Competencia();

        $competencia->nombre        = request('nombre');
        $competencia->descripcion        = request('descripcion');
        $competencia->modulo        = request('modulo');
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/competencia_transversal', $file->getClientOriginalName());
            $competencia->imagen = $file->getClientOriginalName();
        }
        $competencia->save();
        $competencia->Users()->attach($request->user()->id);
        return redirect()->route('competencia.index');
    }

    public function edit($id)
    {
        $competencia = Competencia::findOrFail($id);
        return view('admin.competencia.edit', compact('competencia'));
    }

    public function update(Request $request, $id)
    {
        $competenciaes = Competencia::findOrFail($id);
        $competenciaes->fill($request->all());
        $imagen_anterior = $competenciaes->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/competencia_transversal', $file->getClientOriginalName());
            $competenciaes->imagen = $file->getClientOriginalName();
        }
        $competenciaes->save();
        return redirect()->route('competencia.index');
    }

    public function destroy($id)
    {
        $competencia = Competencia::find($id);
        $competencia->delete();
        return redirect('admin/competencia');
    }
}
