<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\UserEditFormRequest;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.edit',['user' => $user ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(),['email' =>['required', 'email','max:225','unique:users,email,' . $id]]);
        $user = User::findOrFail($id);

        $user->name = $request->get('name');
        $user->email = $request->get('email'); 
    

        $pass = $request->get('password');
        if($pass !=null){
            $user->password = bcrypt($request->get('password'));
        }else{
            unset($user->password);
        }

        $user->update();

        return redirect('/admin/rol');
    }

    public function destroy($id)
    {
        $usuario = User::findOrFail($id);
        $usuario->delete();
        return redirect('/admin/rol');
    }
}
