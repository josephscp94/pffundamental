<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carrera;
use App\Models\Facultad;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CarreraController extends Controller
{

    public function index(Request  $request)
    {
        $carreras = Carrera::orderBy('nombre', 'asc')->paginate(8);
        $facultades = Facultad::all();
        if ($request) {
            $query = trim($request->get('search'));
            $carreras = Carrera::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.carrera.index', ['carreras' => $carreras, 'search' => $query]);
        }
        return view("admin.carrera.index", compact("carreras", 'facultades'));
    }

    public function create()
    {
        $facultades = Facultad::all()->sortBy('nombre');
        return view('admin.carrera.create', compact('facultades'));
    }

    public function store(Request $request)
    {

        $carrera = new Carrera();
        $carrera->facultad_id = ($request->input('facultad_id'));

        $carrera->nombre         = request('nombre');
        $carrera->slug           = Str::slug($request->get('nombre'));
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/carrera', $file->getClientOriginalName());
            $carrera->imagen = $file->getClientOriginalName();
        }
        $carrera->save();

        return redirect()->route('carrera.index');
    }

    public function edit($id)
    {

        $carrera = Carrera::findOrFail($id);
        $facultades = Facultad::all()->sortBy('nombre');
        return view('admin.carrera.edit', compact('carrera', 'facultades'));
    }

    public function update(Request $request, $id)
    {

        $carrera = Carrera::findOrFail($id);
        $carrera->facultad_id = ($request->input('facultad_id'));
        $carrera->fill($request->all());

        $imagen_anterior = $carrera->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/carrera', $file->getClientOriginalName());
            $carrera->imagen = $file->getClientOriginalName();
        }
        $carrera->save();

        return redirect()->route('carrera.index');
    }

    public function destroy($id)
    {
        $carrera = Carrera::find($id);
        unlink(public_path('img/admin/carrera/' . $carrera->imagen));
        $carrera->delete();
        return redirect('admin/carrera');
    }
}
