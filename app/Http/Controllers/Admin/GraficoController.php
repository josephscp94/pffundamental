<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Carrera;
use App\Models\Periodo;
use App\Models\Socio_comunitario;
use App\Models\Tag;
use App\Models\Ods;

class GraficoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function grafico_carreras()
    {
        $cons_carreras = Carrera::withCount('proyecto')->get();
        $labels = $cons_carreras->sortBy('nombre')->pluck('nombre');
        $data = $cons_carreras->sortBy('nombre')->pluck('proyecto_count');

        return view('admin.grafico_carreras', compact('labels', 'data'));
    }

    public function grafico_socios()
    {
        $cons_socios = Socio_comunitario::withCount('proyecto')->get();
        $labels = $cons_socios->sortBy('nombre')->pluck('nombre');
        $data = $cons_socios->sortBy('nombre')->pluck('proyecto_count');

        return view('admin.grafico_socios', compact('labels', 'data'));
    }

    public function grafico_periodos()
    {
        $cons_periodo = Periodo::withCount('proyecto')->get();
        $labels = $cons_periodo->sortBy('año')->pluck('año');
        $data = $cons_periodo->sortBy('año')->pluck('proyecto_count');

        return view('admin.grafico_periodos', compact('labels', 'data'));
    }

    public function grafico_tags()
    {
        $cons_periodo = Tag::withCount('proyecto')->get();
        $labels = $cons_periodo->sortBy('nombre')->pluck('nombre');
        $data = $cons_periodo->sortBy('nombre')->pluck('proyecto_count');

        return view('admin.grafico_tags', compact('labels', 'data'));
    }

    public function grafico_ods()
    {
        $cons_ods = Ods::withCount('proyecto')->get();
        $labels = $cons_ods->sortBy('nombre')->pluck('nombre');
        $data = $cons_ods->sortBy('nombre')->pluck('proyecto_count');

        return view('admin.grafico_ods', compact('labels', 'data'));
    }
}
