<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Que_hacemos;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class Que_hacemosController extends Controller
{

    public function index(Request $request)
    {
        $quehacemos = Que_hacemos::all();
        return view('admin.que_hacemos.index', compact('quehacemos'));
    }

    public function create()
    {


        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.que_hacemos.create', compact('user'));
    }

    public function store(Request $request)
    {

        $quehacemos = new Que_hacemos();

        $quehacemos->slug          = Str::slug(('que_hacemos'));
        $quehacemos->descripcion_de_proyecto        = request('descripcion_de_proyecto');
        if ($request->hasFile('imagen_proy')) {
            $file = $request->imagen_proy;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_proy = $file->getClientOriginalName();
        }

        $quehacemos->compromiso_institucional        = request('compromiso_institucional');
        if ($request->hasFile('imagen_ci')) {
            $file = $request->imagen_ci;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_ci = $file->getClientOriginalName();
        }

        $quehacemos->docencia      = request('docencia');
        if ($request->hasFile('imagen_dc')) {
            $file = $request->imagen_dc;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_dc = $file->getClientOriginalName();
        }


        $quehacemos->vinculacion_con_el_medio      = request('vinculacion_con_el_medio');
        if ($request->hasFile('imagen_vcm')) {
            $file = $request->imagen_vcm;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_vcm = $file->getClientOriginalName();
        }
        $quehacemos->estado       = request('estado');
        $quehacemos->user_id = $request->user()->id;
        $quehacemos->save();
        return redirect()->route('que_hacemos.index');
    }

    public function show(Que_hacemos $quehacemos)
    {
        //
    }

    public function edit($id)
    {
        $quehacemos = Que_hacemos::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.que_hacemos.edit', compact('quehacemos', 'user'));
    }

    public function update(Request $request, $id)
    {
        $quehacemos = Que_hacemos::findOrFail($id);
        $quehacemos->fill($request->all());

        if ($request->hasFile('imagen_ci')) {
            $file = $request->imagen_ci;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_ci = $file->getClientOriginalName();
        }

        if ($request->hasFile('imagen_dc')) {
            $file = $request->imagen_dc;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_dc = $file->getClientOriginalName();
        }

        if ($request->hasFile('imagen_vcm')) {
            $file = $request->imagen_vcm;
            $file->move(public_path() . '/img/admin/que_hacemos', $file->getClientOriginalName());
            $quehacemos->imagen_vcm = $file->getClientOriginalName();
        }

        $quehacemos->user_id = $request->user()->id;
        $quehacemos->save();
        return redirect()->route('que_hacemos.index');
    }

    public function destroy($id)
    {
        $quehacemos = Que_hacemos::find($id);
        unlink(public_path('img/admin/que_hacemos/' . $quehacemos->imagen_proy));
        unlink(public_path('img/admin/que_hacemos/' . $quehacemos->imagen_ci));
        unlink(public_path('img/admin/que_hacemos/' . $quehacemos->imagen_dc));
        unlink(public_path('img/admin/que_hacemos/' . $quehacemos->imagen_vcm));
        $quehacemos->delete();
        return redirect('admin/que_hacemos');
    }
}
