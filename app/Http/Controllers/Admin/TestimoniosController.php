<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Home_noticias;
use App\Models\Testimonios;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Nette\Utils\Image;

class TestimoniosController extends Controller
{

    public function index(Request $request)
    {
        $testimonios = Testimonios::all();
        if ($request) {
            $query = trim($request->get('search'));
            $testimonios = Testimonios::where('nombre', 'LIKE', '%' . $query . '%')
                ->orderBy('nombre', 'asc')
                ->paginate(10);
            return view('admin.testimonios.index', ['testimonios' => $testimonios, 'search' => $query]);
        }
    }



    public function create()
    {
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.testimonios.create', compact('user'));
    }

    public function store(Request $request)
    {

        $testimonios = new Testimonios($request->all());

        $testimonios->slug          = Str::slug(('testimonios'));
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/testimonios', $file->getClientOriginalName());
            $testimonios->imagen = $file->getClientOriginalName();
        }
        $testimonios->estado       = $request->get('estado') ? 1 : 0;

        $testimonios->user_id = $request->user()->id;
        $testimonios->save();
        return redirect()->route('testimonios.index');
    }

    public function show(Testimonios $testimonios)
    {
        //
    }

    public function edit($id)
    {
        $testimonios = Testimonios::findOrFail($id);
        $user = User::orderBy('id', 'desc')->pluck('name', 'id');
        return view('admin.testimonios.edit', compact('testimonios', 'user'));
    }

    public function update(Request $request, $id)
    {
        $testimonios = Testimonios::findOrFail($id);
        $testimonios->fill($request->all());

        $imagen_proy_anterior = $testimonios->imagen;
        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/img/admin/testimonios', $file->getClientOriginalName());
            $testimonios->imagen = $file->getClientOriginalName();
        }


        $testimonios->estado       = $request->get('estado') ? 1 : 0;

        $testimonios->user_id = $request->user()->id;
        $testimonios->save();
        return redirect()->route('testimonios.index');
    }

    public function destroy($id)
    {
        $testimonios = Testimonios::find($id);
        unlink(public_path('img/admin/testimonios/' . $testimonios->imagen));
        $testimonios->delete();
        return redirect('admin/testimonios');
    }
}
