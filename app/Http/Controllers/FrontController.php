<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Colaborador;
use App\Models\Competencia;
use App\Models\Home_carrusel;
use App\Models\Home_noticias;
use App\Models\Quienes_somos;
use App\Models\Que_hacemos;
use App\Models\Socio_comunitario;
use App\Models\Facultad;
use App\Models\Carrera;
use App\Models\Tag;
use App\Models\Ods;
use App\Models\Proyecto;
use App\Models\Periodo;
use App\Models\Testimonios;
use App\Models\Solicitudes;
use App\Models\Contacto;
use App\Models\Telefono_area;
use Illuminate\Http\Request;
use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Php;

class FrontController extends Controller
{

    public function home()
    {
        $home_carrusel = Home_carrusel::where('estado', '1')->orderBy('updated_at', 'desc')->get();
        $home_noticias = Home_noticias::where('estado', '1')->orderBy('updated_at', 'desc')->get();
        $testimonios = Testimonios::where('estado', '1')->orderBy('updated_at', 'desc')->get();
        $footer = Contacto::all();

        return view('home', compact('home_carrusel', 'home_noticias', 'testimonios', 'footer'));
    }

    public function noticias($slug)
    {
        $home_noticias = Home_noticias::where('slug', $slug)->firstOrFail();
        $otras_noticias = Home_noticias::where('estado', 1)
            ->orderBy('updated_at', 'desc')
            ->whereNotIn('id', [$home_noticias->id])
            ->take(3)
            ->get();

        $footer = Contacto::all();

        return view('noticias', compact('home_noticias', 'otras_noticias', 'footer'));
    }


    public function blognoticias()
    {
        $home_noticias = Home_noticias::where('estado', '1')->orderBy('titular', 'asc')->paginate(8);
        $footer = Contacto::all();
        return view('blognoticias', compact('home_noticias', 'footer'));
    }

    public function competencias()
    {
        $competencias = Competencia::all();
        $footer = Contacto::all();
        return view('competencias', compact('competencias', 'footer'));
    }

    public function carreras()
    {
        $carreras = Carrera::orderBy('nombre', 'asc')->get();
        $facultades = Facultad::orderBy('nombre', 'asc')->get();
        $footer = Contacto::all();
        return view('carreras', compact('carreras', 'facultades', 'footer'));
    }

    public function que_hacemos()
    {
        $que_hacemos = Que_hacemos::all();
        $footer = Contacto::all();
        return view('que_hacemos', compact('que_hacemos', 'footer'));
    }
    public function contacto()
    {
        $footer = Contacto::all();
        $telefonos = Telefono_area::all();
        return view('contacto', compact('footer', 'telefonos'));
    }
    public function socio_comunitarios()
    {
        $socios_comunitarios = Socio_comunitario::orderBy('nombre')->paginate(6);
        $facultades = Facultad::all();
        $footer = Contacto::all();
        return view('socio_comunitarios', compact('socios_comunitarios', 'facultades', 'footer'));
    }

    public function home_carrusel()
    {
        $home_carrusel = Home_carrusel::where('estado', '1')->orderBy('updated_at', 'desc')->get();
        $footer = Contacto::all();
        return view('home_carrusel', compact('home_carrusel', 'footer'));
    }

    public function proyecto_detalle($slug)
    {
        $proyecto = Proyecto::where('slug', $slug)->firstOrFail();
        $socios_comunitarios = Socio_comunitario::all();
        $periodo = Periodo::all();
        $carrera = Carrera::all();
        $footer = Contacto::all();
        return view('proyecto_detalle', compact('proyecto', 'periodo', 'carrera', 'footer'));
    }

    public function proyectos(Request $request, $id)
    {
        $proyectos = Proyecto::where('carrera_id', $id)->orderBy('periodo_id', 'desc')->orderBy('nombre', 'asc');
        $query = Proyecto::where('carrera_id', $id)->orderBy('periodo_id', 'desc')->orderBy('nombre', 'asc');


        if ($request->input('ods_id')) {
            $odsId = $request->input('ods_id');
            $query = Proyecto::where('carrera_id', $id)->whereHas('ods', function ($query) use ($odsId) {
                $query->where('ods.id', $odsId);
            });
        }
        if ($request->input('periodo_id')) {
            $query->where('carrera_id', $id)->where('periodo_id', $request->input('periodo_id'));
        }
        if ($request->input('socio_comunitario_id')) {
            $query->where('carrera_id', $id)->where('socio_comunitario_id', $request->input('socio_comunitario_id'));
        }

        if ($request->input('buscador')) {
            $buscador = trim($request->get('buscador'));
            $query = Proyecto::where('carrera_id', $id)->where('nombre', 'LIKE', '%' . $buscador . '%');
        }

        $proyectos = $query->paginate(12);
        $idsSociosComunitarios = $proyectos->pluck('socio_comunitario_id')->toArray();
        $sociosComunitarios = Socio_comunitario::whereIn('id', $idsSociosComunitarios)->get();
        $socios = $sociosComunitarios->sortBy('nombre');

        $ids_periodo = $proyectos->pluck('periodo_id')->toArray();
        $periodos_p = Periodo::whereIn('id', $ids_periodo)->get();
        $periodos = $periodos_p->sortBy('nombre');

        $odss = Ods::all()->sortBy('nombre');

        $carrera = Carrera::where('id', $id)->firstOrFail();
        $footer = Contacto::all();

        return view('proyectos', compact('socios', 'odss', 'proyectos', 'carrera', 'periodos', 'footer'));
    }

    public function formulario(Request $request)
    {
        $solicitud = new Solicitudes();
        $solicitud->nombre_institucion       = request('nombre_institucion');
        $solicitud->descripcion        = request('descripcion');
        $solicitud->nombre_persona        = request('nombre_persona');
        $solicitud->correo        = request('correo');
        $solicitud->telefono        = request('telefono');
        $solicitud->save();

        return redirect('/socios-comunitarios');
    }

    public function tags($id)
    {
        $proyectos = Proyecto::whereHas('tag', function ($query) use ($id) {
            $query->where('tags.id', $id);
        })->paginate(12);
        $footer = Contacto::all();
        $tag = Tag::where('id', $id)->firstOrFail();
        return view('tags', compact('proyectos', 'footer', 'tag'));
    }

    public function quienes_somos()
    {
        $areas = Area::all();
        // $colaboradores = Colaborador::orderBy('nombre', 'asc')->paginate(12);
        $boton1 = Colaborador::whereHas('rol', function ($query1) {
            $query1->where('area_id', '1');
        })->orderBy('nombre', 'asc')->paginate(12);
        $boton2 = Colaborador::whereHas('rol', function ($query2) {
            $query2->where('area_id', '2');
        })->orderBy('nombre', 'asc')->paginate(12);
        $boton3 = Colaborador::whereHas('rol', function ($query3) {
            $query3->where('area_id', '3');
        })->orderBy('nombre', 'asc')->paginate(12);

        $quienes_somos = Quienes_somos::all();
        $footer = Contacto::all();
        return view('quienes_somos', compact('areas', 'boton1', 'boton2', 'boton3', 'quienes_somos', 'footer'));
    }
}
