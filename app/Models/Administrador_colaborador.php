<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Administrador_colaborador extends Pivot
{
    use HasFactory;
    protected $fillable = [
        'colaborador_id',
        'user_id'
    ];
}
