<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nombre',
        'proyecto_id'
    ];

    public function Proyecto()
    {
        return $this->belongsTo(Proyecto::class, 'proyecto_id');
    }
}
