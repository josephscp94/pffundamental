<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable
{
    
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    public function Colaboradors():BelongsToMany{
        return $this->belongsToMany(Colaborador::class,'colaborador')->using(Colaborador::class)->withTimestamps();
    }
    public function Modulos():BelongsToMany{
        return $this->belongsToMany(Modulo::class,'modulo')->using(Modulo::class)->withTimestamps();
    }
    public function Proyectos():BelongsToMany{
        return $this->belongsToMany(Proyecto::class,'proyecto')->using(Proyecto::class)->withTimestamps();
    }
 

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
