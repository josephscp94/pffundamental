<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Ods extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nombre',
        'slug',
    ];
    public function Proyecto():BelongsToMany{
        return $this->belongsToMany(Proyecto::class,'proyecto_ods')->using(Proyecto_ods::class)->withTimestamps();
    }
}
