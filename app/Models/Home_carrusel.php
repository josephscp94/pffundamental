<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Home_carrusel extends Model
{
    use HasFactory;
    protected $fillable = [
        'descripcion',
        'imagen',
        'estado',
        'link',
        'user_id'
    ];

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user')->using(User::class)->withTimestamps();
    }
}
