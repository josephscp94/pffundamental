<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Que_hacemos extends Model
{
    use HasFactory;
    protected $fillable = [
        'compromiso_institucional',
        'imagen_ci',
        'docencia',
        'imagen_dc',
        'vinculacion_con_el_medio',
        'imagen_vcm',
        'user_id',


    ];

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user')->using(User::class)->withTimestamps();
    }
}
