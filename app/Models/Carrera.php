<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Carrera extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nombre',
        'slug',
        'imagen',
        'facultad_id'
    ];

    public function Proyecto(): HasMany
    {
        return $this->hasMany(Proyecto::class);
    }
    public function Facultad()
    {
        return $this->belongsTo(Facultad::class, 'facultad_id');
    }
}
