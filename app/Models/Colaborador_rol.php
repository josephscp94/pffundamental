<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Colaborador_rol extends Pivot
{
    use HasFactory;
    protected $fillable = [
        'colaborador_id',
        'rol_id'
    ];
}
