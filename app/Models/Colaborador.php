<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Colaborador extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nombre',
        'correo',
        'imagen',
    ];
    public function Rol(): BelongsToMany
    {
        return $this->belongsToMany(Rol::class, 'colaborador_rols')
            ->withPivot(['rol_id'])
            ->using(Colaborador_rol::class)
            ->withTimestamps();
    }

    public function Users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'administrador_colaboradors')->using(Administrador_colaborador::class)->withTimestamps();
    }
}
