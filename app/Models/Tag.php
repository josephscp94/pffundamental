<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'slug',
        'nombre',
    ];
    public function Proyecto(): BelongsToMany
    {
        return $this->belongsToMany(Proyecto::class, 'proyecto_tags')->using(Proyecto_tag::class)->withTimestamps();
    }
}
