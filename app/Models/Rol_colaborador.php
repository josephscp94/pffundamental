<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol_colaborador extends Model
{
    use HasFactory;
    protected $fillable = [
        'colaborador_id',
        'rol_id'
    ];

    public function Colaborador(){
        return $this->belongsTo("App\Colaborador");
    }
    public function Rol(){
        return $this->belongsTo("App\Rol");
    }
}
