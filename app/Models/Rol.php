<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Rol extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nombre',
        'area_id'
    ];
    public function Colaborador(): BelongsToMany
    {
        return $this->belongsToMany(Colaborador::class, 'colaborador_rols')->using(Colaborador_rol::class)->withTimestamps();
    }

    public function Area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }
}
