<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Area extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nombre'
    ];

    public function Rol(): HasMany
    {
        return $this->hasMany(Rol::class, 'foreign_key');
    }
}
