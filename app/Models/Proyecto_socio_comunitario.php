<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Proyecto_socio_comunitario extends Pivot
{
    use HasFactory;
    protected $fillable = [
        'proyecto_id',
        'socio_comunitario_id'
    ];
}
