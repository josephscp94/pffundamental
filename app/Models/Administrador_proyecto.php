<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Administrador_proyecto extends Pivot
{
    use HasFactory;
    protected $fillable = [
        'proyecto_id',
        'user_id'
    ];

}
