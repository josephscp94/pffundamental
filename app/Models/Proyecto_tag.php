<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Proyecto_tag extends Pivot
{
    use HasFactory;
    protected $fillable = [
        'proyecto_id',
        'tag_id'
    ];

}
