<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Proyecto extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'slug',
        'nombre',
        'descripcion',
        'imagen',
        'carrera_id',
        'periodo_id',
        'socio_comunitario_id'
    ];

    public function Tag(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'proyecto_tags')->using(Proyecto_tag::class)->withTimestamps();
    }

    public function Ods(): BelongsToMany
    {
        return $this->belongsToMany(Ods::class, 'proyecto_ods')->using(Proyecto_ods::class)->withTimestamps();
    }

    public function Users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'administrador_proyectos')->using(Administrador_proyecto::class)->withTimestamps();
    }

    public function Carrera()
    {
        return $this->belongsTo(Carrera::class, 'carrera_id');
    }

    public function Alumno(): HasMany
    {
        return $this->HasMany(Alumno::class, 'proyecto_id');
    }
    public function Socio_comunitario()
    {
        return $this->belongsTo(Socio_comunitario::class, 'socio_comunitario_id');
    }
    public function Periodo()
    {
        return $this->belongsTo(Periodo::class, 'periodo_id');
    }
}
