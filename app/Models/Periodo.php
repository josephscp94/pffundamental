<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Periodo extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'año',
    ];

    public function Proyecto(): HasMany
    {
        return $this->hasMany(Proyecto::class);
    }
}
