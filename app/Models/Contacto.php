<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contacto extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'correo',
        'telefono',
        'instagram',
        'url_universidad',
        'url_vicerrectoria',
        'descripcion',
        'user_id'
    ];

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user')->using(User::class)->withTimestamps();
    }
}
