<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitudes extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre_institucion',
        'descripcion',
        'nombre_persona',
        'correo',
        'telefono',
        'estado'
    ];
}
